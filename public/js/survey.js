
$('.datepicker').datepicker();
var questionCount = 0;
var choiceCount = 0;

$('#addQuestionBt').on('click',function(){
    type = $('#typeSelector').val();
    question = $('#questionField').val();

    console.log(type);
    console.log(question);
    questionCount++;

    el = '<div class="form-group clearfix ui-state-default" data-id="'+questionCount+'">';

    if(type=='text')
    {
        el += '<label>Text Question </label>';
    } else if (type=='textarea')
    {
        el += '<label>Textarea Question </label>';
    } else if (type=='radio')
    {
        el += '<label>Multiple choices with single answer</label>';
    } else if (type=='checkbox')
    {
        el += '<label>Multiple choices with multiple answers</label>';
    } else if (type=='date')
    {
        el += '<label>Date Question </label>';
    } else if (type=='rate')
    {
        el += '<label>Rate Question </label>';
    }

    el += '<input type="text" class="form-control" required="required" name="question['+questionCount+'][question]" style="margin-bottom: 10px;" placeholder="Add question here...">';
    el += '<span class="delete-question"><span class="glyphicon glyphicon-trash" aria-hidden="true"></span></span><span class="move-question"><span class="glyphicon glyphicon-move" aria-hidden="true"></span></span>';

    if (type=='radio'|| type=='checkbox')
    {
        el += '<div class="row"><div class="field-group col-md-9"><label>Choices</label>';
        el += '&nbsp;[ Display side by side <input name="question['+questionCount+'][is_float]" value="1" type="checkbox"> ]';
        el += '<br><div class="choice"><input type="text" name="question['+questionCount+'][choices]['+choiceCount+'][choice]"></div><br><button type="button" class="addQBT">+ Add a choice</button><br>';
        choiceCount++;
    }
    else if (type=='rate')
    {
        el += '<div class="row"><div class="field-group col-md-9"><label>Items</label>';
        el += '<br><div class="choice"><input type="text" name="question['+questionCount+'][choices]['+choiceCount+'][choice]">&nbsp;<select class="rateSelect" name="question['+questionCount+'][choices]['+choiceCount+'][rate]"></select></div><br><button type="button" class="addRBT">+ Add an item</button><br>';
        choiceCount++;
    }
    else
    {
        el += '<div class="row"><div class="field-group col-md-9"><br><br>';
    }

    el += '<input type="hidden" name="question['+questionCount+'][type]" value="'+type+'">';

    el += '<br></div><div class="col-md-3"><label class="pull-right answer-required">Answer required <input name="question['+questionCount+'][is_required]" value="1" type="checkbox" checked></label></div>';

    el += '</div>';

    el += '<input type="text" class="count" readonly="readonly" name="question['+questionCount+'][order]" value="'+questionCount+'">';

    el += '</div>';

    $('#surveyElements').append(el);
    addQBTClickEvent();
    addRBTClickEvent();
    addDeleteQuestionClickEvent();
    updateSortable();
    updateRateSelect();
});

function updateRateSelect(){
    choices = [5,10];

    $('.rateSelect').each(function(e,i){
        item = $(this);

        if(item.find('option').length < 1){
            for(x=0;x<choices.length;x++)
                $('<option/>').attr('value', choices[x]).text('1 to '+choices[x]).appendTo(item).trigger('change');
        }

    });

}

function addQBTClickEvent(){
    $('.addQBT').unbind('click');
    $('.addQBT').on('click',function(){
        holder = $(this).closest('.form-group');
        id = holder.attr('data-id');

        el = '<div class="choice">';
        el += '<input type="text" name="question['+id+'][choices]['+choiceCount+'][choice]"><span class="glyphicon glyphicon-remove-sign delete-choice" aria-hidden="true"></span><br>';
        el += '</div>';

        holder.find('.field-group .choice').last().after(el);
        addDeleteChoiceClickEvent();
        choiceCount++;
    });
}

function addRBTClickEvent(){
    $('.addRBT').unbind('click');
    $('.addRBT').on('click',function(){
        holder = $(this).closest('.form-group');
        id = holder.attr('data-id');

        el = '<div class="choice">';
        el += '<input type="text" name="question['+id+'][choices]['+choiceCount+'][choice]">&nbsp;<select class="rateSelect" name="question['+id+'][choices]['+choiceCount+'][rate]"></select><span class="glyphicon glyphicon-remove-sign delete-choice" aria-hidden="true"></span><br>';
        el += '</div>';

        holder.find('.field-group .choice').last().after(el);

        updateRateSelect();
        addDeleteChoiceClickEvent();
        choiceCount++;
    });
}

function addDeleteChoiceClickEvent(){
    $('.delete-choice').unbind('click');
    $('.delete-choice').on('click',function(){
        holder = $(this).closest('.choice').remove();
    });
}

function addDeleteQuestionClickEvent(){
    console.log('adding');
    $('#surveyElements .delete-question').unbind('click');
    $('#surveyElements .delete-question').on('click',function(){

        console.log('deleting');
        console.log($(this).closest('.form-group'));
        $(this).closest('.form-group').remove();
    });
}

function updateSortable(){
    setTimeout(function(){
        $( "#surveyElements" ).sortable( "refresh" );
    },300);
}

$( "#surveyElements" ).sortable({
    tolerance: 'pointer',
    stop : function( event, ui ){
        updateOrder();
    }
});

function updateOrder(){
    order = 1;
    $('#surveyElements .form-group').each(function(e,i){
        $(i).find('.count').val(order);
        order++;
    });
}

//        $('#submitForm').on('click',function(){
//            $('#surveyElements').clone().appendTo('#surveyForm');
//            $('#surveyForm').submit();
//        });