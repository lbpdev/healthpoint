@extends('emails.main')

@section('email_body')
    <br>
    <br>
    <h1>New survey entry for {{ $data['survey'] }}</h1>
    <br>

    @foreach($data['questions'] as $index=>$item)
        <div class="form-group clearfix ui-state-default">
            <label>{{$index+1}}. {{ $item['question'] }}</label>
            <br>
            <p>
                @foreach($item['data'] as $index=>$data)
                     &nbsp; &nbsp; {{ $data['value'] }} {{ $index+1 < count($item['data']) ? ',' : '' }}
                @endforeach
            </p>
        </div>
    @endforeach

@endsection