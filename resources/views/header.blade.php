
<!doctype html>
<html class="no-js" lang="">
<head>
    <meta charset="utf-8">
    <meta http-equiv="x-ua-compatible" content="ie=edge">

    @inject('optionFetcher','App\Services\OptionFetcher')

    <?php
        $title = $optionFetcher->getBySlug('site-title');
    ?>

    <title>{{ $title ? strip_tags($title) : 'Healthpoint - Official Website'  }}</title>
    <meta name="description" content="">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="SHORTCUT ICON" href="{{ asset('public/favicon.ico') }}">
    <!-- Place favicon.ico in the root directory -->

    <link href="{{ asset('public/css/bootstrap.css') }}" rel="stylesheet" type="text/css">
    <link href="{{ asset('public/css/bootstrap-select.css') }}" rel="stylesheet" type="text/css">

    <link href="{{ asset('public/css/genestrap.css') }}" rel="stylesheet" type="text/css">
    <link href="{{ asset('public/css/main.css') }}" rel="stylesheet" type="text/css">
    <link href="{{ asset('public/css/responsive.css') }}" rel="stylesheet" type="text/css">

    <link href="{{ asset('public/fonts/stylesheet.css') }}" rel="stylesheet" type="text/css">
    <link href="{{ asset('public/css/overrides.css') }}" rel="stylesheet" type="text/css">

    <link rel="stylesheet" href="{{ asset('public/css/normalize.css') }}">
    <script src="{{ asset('public/js/modernizr-2.8.3.min.js') }}"></script>

    <style>
        #html5-watermark {
            display: none !important;
        }
        .patient-portal {
            position: absolute;
            margin-top: 12px;
            right: 152px;
            top: 0px;
            /* border: 1px solid #ccc; */
            border-radius: 7px;
            background-color: #e4e1df;
            padding: 3px 10px;
            color: #6b554e;
            text-transform: capitalize;
            z-index: 111;
        }
        .navbar-toggle {
            margin-top: 14px !important;
            margin-bottom: 8px !important;
        }
    </style>

    @yield('custom-styles')
    @yield('custom-styles-alt')

    @yield('metas')
</head>
<body>
<!--[if lt IE 8]>
<p class="browserupgrade">You are using an <strong>outdated</strong> browser. Please <a href="http://browsehappy.com/">upgrade your browser</a> to improve your experience.</p>
<![endif]-->

<div class="col-md-12" id="top-pad"></div>
<div class="container">
<header class="col-md-12 padding-t-0">

    <a class="patient-portal" target="_blank" href="http://Healthpoint.iqhealth.com">Patient Portal</a>
    <div class="row padding-t-0">
        <div class="col-md-2 col-sm-12 col-xs-12 logo-wrap">
            <div class="row padding-t-0">
                <a href="{{ url('/') }}"><img src="{{ asset('public/images/logos/Healthpoint-Logo-Small.png') }}" title="healthpoint logo" width="90%" id="top-logo"></a>
            </div>
        </div>
        <div class="col-md-8 col-xs-12 col-sm-12 nav-row">
            <div class="row padding-t-0">

                <nav class="navbar navbar-default" id="main-nav">

                    <div class="container-fluid">
                        <!-- Brand and toggle get grouped for better mobile display -->
                        <div class="navbar-header">
                            <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1" aria-expanded="false">
                                <span class="sr-only">Toggle navigation</span>
                                <span class="icon-bar"></span>
                                <span class="icon-bar"></span>
                                <span class="icon-bar"></span>
                            </button>
                        </div>

                        <!-- Collect the nav links, forms, and other content for toggling -->
                        <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
                            <ul class="nav navbar-nav">
                                <li class="dropdown active">
                                    <a class="dropdown-toggle {{ str_contains(Request::path(), 'about-us') ? 'active' : '' }}" href="{{ url('about-us') }}">About Us</a>
                                    <ul class="dropdown-menu">
                                        <li>
                                            <a href="{{ url('about-us') }}">Who We Are</a>
                                            <a href="{{ url('about-us') }}?tab=management">Executive Management</a>
                                            <a href="{{ url('about-us') }}?tab=healthcare-network">Mubadala Healthcare Network</a>
                                            <a href="{{ url('about-us') }}?tab=accreditation">Awards and Accreditations</a>
                                        </li>
                                    </ul>
                                </li>

                                <li class="dropdown">
                                    <a href="{{ url('find-a-doctor') }}" class=" {{ str_contains(Request::path(), 'find-a-doctor') || str_contains(Request::path(), 'clinical-services') ? 'active' : '' }} dropdown-toggle" >Clinical Services & Departments</a>
                                    <ul class="dropdown-menu">
                                        <li>
                                            <a href="{{ url('find-a-doctor') }}">Find a Physician</a>
                                            <a href="{{ url('clinical-services') }}">Clinical Services</a>
                                        </li>
                                    </ul>
                                </li>
                                <li class="dropdown">
                                    <a class="dropdown-toggle {{ str_contains(Request::path(), 'patients/') ? 'active' : '' }}" href="{{ url('patients/our-services') }}">Patients & Visitors</a>
                                    <ul class="dropdown-menu">
                                        <li>
                                            <a href="{{ url('patients/our-services') }}">Our Services</a>
                                            <a href="{{ url('patients/insurance') }}">Insurance</a>
                                            <a href="{{ url('patients/at-healthpoint') }}">Innovations At Healthpoint</a>
                                            <a href="{{ route('media.patient-newsletters') }}">Patient Newsletter</a>
                                            {{--<a href="{{ route('surveys.index') }}">Surveys</a>--}}
                                            <a href="{{ route('surveys.index') }}">Patient Surveys</a>
                                            <a target="_blank" href="http://Healthpoint.iqhealth.com">Patient Portal</a>
                                        </li>
                                    </ul>
                                </li>
                                <li><a href="{{ url('health-tips') }}" class="{{ str_contains(Request::path(), 'health-tips') ? 'active' : '' }}">Health Tips</a></li>
                                <li class="dropdown">
                                    <a class="dropdown-toggle {{ str_contains(Request::path(), 'work-with-us') ? 'active' : '' }}" href="{{ url('work-with-us') }}">Work With Us</a>
                                    <ul class="dropdown-menu">
                                        <li>
                                            <a href="{{ url('work-with-us') }}">Working for Healthpoint</a>
                                            <a href="{{ url('work-with-us/emiratization') }}">Emiratization</a>
                                            <a href="{{ url('work-with-us/careers') }}">Careers</a>
                                        </li>
                                    </ul>
                                </li>
                                <li class="dropdown">
                                    <a class="dropdown-toggle {{ str_contains(Request::path(), 'media/') ? 'active' : '' }}" href="{{ url('media/news') }}">Media</a>
                                    <ul class="dropdown-menu">
                                        <li>
                                            <a href="{{ url('media/news') }}">News</a>
                                            <a href="{{ url('media/seminars-and-workshops') }}">Seminars and Workshops</a>
                                            {{--<a href="{{ url('media/hospital-campaigns') }}">Hospital Campaigns</a>--}}
                                            <a href="{{ url('media/galleries') }}">Galleries</a>
                                        </li>
                                    </ul>
                                </li>
                                <li class="dropdown">
                                    <a class="dropdown-toggle {{ str_contains(Request::path(), 'events') ? 'active' : '' }}" href="{{ url('events/upcoming') }}">Events</a>
                                </li>
                            </ul>
                        </div><!-- /.navbar-collapse -->
                    </div><!-- /.container-fluid -->
                </nav>
            </div>
        </div>
        <div class="col-md-2 col-xs-12 col-sm-12">
            <div class="row padding-t-0">
                <div class="text-right">
                    <nav id="socials" class="clearfix">
                        <ul class="socials">
                            <li><a href="https://www.facebook.com/HealthpointUAE" target="_blank" class="social-icon fb"></a></li>
                            <li><a href="https://twitter.com/HealthpointUAE" target="_blank"  class="social-icon tw"></a></li>
                            <li><a href="https://www.instagram.com/HealthpointUAE" target="_blank"  class="social-icon in"></a></li>
                            <li><a href="http://www.linkedin.com/company/HealthpointUAE" target="_blank"  class="social-icon lk"></a></li>
                            <li><a href="http://www.youtube.com/user/HealthpointUAE" target="_blank"  class="social-icon yt"></a></li>
                        </ul>
                    </nav>
                    {!! Form::open(['route'=>'search','id'=>'top-search']) !!}
                        <input type="text" placeholder="" name="keyword" value="{{ isset($keyword) ? $keyword : '' }}">
                        <input type="submit" value="">
                    {!! Form::close() !!}
                </div>
            </div>
        </div>
    </div>
</header>