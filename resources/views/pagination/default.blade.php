@if ($paginator->lastPage() > 1)

    <?php
        $filters = '';

        if(isset($_GET['name']))
            $filters .= '&name='.$_GET['name'];
        if(isset($_GET['gender']))
            $filters .= '&gender='.$_GET['gender'];
        if(isset($_GET['specialty']))
            $filters .= '&specialty='.$_GET['specialty'];
    ?>
    <ul class="pagination">
        <li class="{{ ($paginator->currentPage() == 1) ? ' disabled' : '' }}">
            <a href="{{ $paginator->url(1).$filters }}">Previous</a>
        </li>
        @for ($i = 1; $i <= $paginator->lastPage(); $i++)
            <li class="{{ ($paginator->currentPage() == $i) ? ' active' : '' }}">
                <a href="{{ $paginator->url($i).$filters }}">{{ $i }}</a>
            </li>
        @endfor
        <li class="{{ ($paginator->currentPage() == $paginator->lastPage()) ? ' disabled' : '' }}">
            <a href="{{ $paginator->url($paginator->currentPage()+1).$filters }}" >Next</a>
        </li>
    </ul>
@endif