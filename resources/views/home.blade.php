@include('header')
    <div class="pull-left home">
        <div class="row">
            <div class="col-md-9 main-content">
                @include('home.banner-box')
                <div class="row pull-left margin-t-0">
                    <div class="col-md-4 clearfix info">
                        @include('home.hospital-info')
                    </div>
                    <div class="col-md-8 gray-border-left clearfix clinical-services">
                        <div class="col-md-12 yellowish-bg margin-b-10">
                            @include('home.clinical-services')
                        </div>
                        <div class="col-md-12 clearfix pull-left">
                            @include('home.sponsors')
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-md-3 sidebar">
                @include('templates.sidebar.help')
                @include('templates.sidebar.tweets')
                @include('templates.sidebar.news')
            </div>
        </div>
    </div>

@section('custom-js')
    <script>
        $('select[name=specialty]').prepend($("<option></option>")
                        .attr("value",'any')
                        .text('Department'));
        $('select[name=specialty]').selectpicker('refresh').val('any').trigger('change');
    </script>
@endsection

@include('footer')
