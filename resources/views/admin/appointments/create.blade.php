@extends('admin.master')

@section('content')
        <!-- Content Header (Page header) -->
<section class="content-header">
    <h1>
        Appointment Requests
        <small>Control panel</small>
    </h1>
</section>

<!-- Main content -->
<section class="content">
    <div class="box box-info">
        <div class="box-header">
            <div class="col-md-12">
            @if(Session::has('success'))
                <div class="alert alert-success">
                    {{ Session::get('success') }}
                </div>
            @endif

            <h3 class="box-title">{{ isset($data) ? 'Edit' : 'Add' }} Appointment Request</h3>

            <!-- /. tools -->
            </div>
        </div>
        <!-- /.box-header -->

        <div class="row">
            <div class="col-md-12">
                <div class="box-body pad">

                    {!! Form::open(['class'=>'appointment','route'=>'admin.appointments.store']) !!}
                    <hr>

                    <h1>Requester Information</h1>

                    <div class="row">
                        <div class="col-md-3 col-sm-3 col-xs-12 clearfix">
                            <div class="padding-t-0 ">
                                <p class="text-left">Who is this appointment for?</p>
                            </div>
                        </div>
                        <div class="col-md-9">
                            <div class="">
                                <input type="radio" id="r1" checked name="Who is this appointment for" value="Self"/>
                                <label for="r1">Self</label>
                                <br>
                                <input type="radio" id="r2" name="Who is this appointment for" value="Other"/>
                                <label for="r2">Other</label>
                            </div>
                        </div>
                    </div>
                    <hr>


                    <h1>Patient Information</h1>
                    <p>Please provide patient information as it appears on legal documents.</p>

                    <div class="row">
                        <div class="col-md-3 col-sm-3 col-xs-12 clearfix">
                            <div class="padding-t-0">
                                <p class="">Have you previously received care at Healthpoint?</p>
                            </div>
                        </div>
                        <div class="col-md-9">
                            <div class="">
                                <input type="radio" id="r3" checked name="Have you previously received care at Healthpoint" value="Yes"/>
                                <label for="r3">Yes</label>
                                <br>
                                <input type="radio" id="r4" name="Have you previously received care at Healthpoint" value="No"/>
                                <label for="r4">No</label>
                                <br>
                                <input type="radio" id="r5" name="Have you previously received care at Healthpoint" value="Do not know"/>
                                <label for="r5">Don't know</label>
                            </div>
                        </div>
                    </div>

                    <div class="row">
                        <div class="col-md-3 col-sm-3 col-xs-12 clearfix">
                            <div class="padding-t-0 ">
                                <p class=" label">First Name:</p>
                            </div>
                        </div>
                        <div class="col-md-9">
                            <div class="">
                                <input type="text" required value="GEne" name="First Name"/>
                            </div>
                        </div>
                    </div>

                    <div class="row">
                        <div class="col-md-3 col-sm-3 col-xs-12 clearfix">
                            <div class="padding-t-0 ">
                                <p class=" label">Surname:</p>
                            </div>
                        </div>
                        <div class="col-md-9">
                            <div class="">
                                <input type="text" required  value="Ellorin" name="Surname"/>
                            </div>
                        </div>
                    </div>

                    <div class="row">
                        <div class="col-md-3 col-sm-3 col-xs-12 clearfix">
                            <div class="padding-t-0 ">
                                <p class=" label">Address:</p>
                            </div>
                        </div>
                        <div class="col-md-9">
                            <div class="">
                                <input type="text" required  value="ASdasdasd" name="Address"/>
                            </div>
                        </div>
                    </div>

                    <div class="row">
                        <div class="col-md-3 col-sm-3 col-xs-12 clearfix">
                            <div class="padding-t-0 ">
                                <p class=" label">City:</p>
                            </div>
                        </div>
                        <div class="col-md-9">
                            <div class="">
                                <input type="text" required  value="City" name="City"/>
                            </div>
                        </div>
                    </div>

                    <div class="row">
                        <div class="col-md-3 col-sm-3 col-xs-12 clearfix">
                            <div class="padding-t-0 ">
                                <p class=" label">Emirate:</p>
                            </div>
                        </div>
                        <div class="col-md-9">
                            <div class="">
                                <input type="text" required  value="Dubai" name="Emirate"/>
                            </div>
                        </div>
                    </div>

                    <div class="row">
                        <div class="col-md-3 col-sm-3 col-xs-12 clearfix">
                            <div class="padding-t-0 ">
                                <p class=" label">P.O Box:</p>
                            </div>
                        </div>
                        <div class="col-md-9">
                            <div class="">
                                <input type="text"  value="POTA BAOX" name="P.O Box"/>
                            </div>
                        </div>
                    </div>

                    <div class="row">
                        <div class="col-md-3 col-sm-3 col-xs-12 clearfix">
                            <div class="padding-t-0 ">
                                <p class=" label">Primary phone:</p>
                            </div>
                        </div>
                        <div class="col-md-9">
                            <div class="">
                                <input type="text" required  value="0564043349" name="Primary phone" class="margin-b-10"/>
                                <br>
                                <input type="radio" id="r3" checked name="Primary phone type" value="Home"/>
                                <label for="r3">Home</label>
                                <br>
                                <input type="radio" id="r4" name="Primary phone type" value="Mobile"/>
                                <label for="r4">Mobile</label>
                                <br>
                                <input type="radio" id="r5" name="Primary phone type" value="Office"/>
                                <label for="r5">Office</label>
                            </div>
                        </div>
                    </div>

                    <div class="row">
                        <div class="col-md-3 col-sm-3 col-xs-12 clearfix">
                            <div class="padding-t-0 ">
                                <p class=" label">Secondary phone:</p>
                            </div>
                        </div>
                        <div class="col-md-9">
                            <div class="">
                                <input type="text" name="Secondary phone"  value="123123123123123" class="margin-b-10"/>
                                <br>
                                <input type="radio" id="r3" checked name="Secondary phone type" value="Home"/>
                                <label for="r3">Home</label>
                                <br>
                                <input type="radio" id="r4" name="Secondary phone type" value="Mobile"/>
                                <label for="r4">Mobile</label>
                                <br>
                                <input type="radio" id="r5" name="Secondary phone type" value="Office"/>
                                <label for="r5">Office</label>
                            </div>
                        </div>
                    </div>


                    <div class="row">
                        <div class="col-md-3 col-sm-3 col-xs-12 clearfix">
                            <div class="padding-t-0 ">
                                <p class=" label">Email:</p>
                            </div>
                        </div>
                        <div class="col-md-9">
                            <div class="">
                                <input type="text" required  value="elloringene@gmail.com" name="Email"/>
                            </div>
                        </div>
                    </div>

                    <div class="row">
                        <div class="col-md-3 col-sm-3 col-xs-12 clearfix">
                            <div class="padding-t-0 ">
                                <p class=" label">Gender:</p>
                            </div>
                        </div>
                        <div class="col-md-9">
                            <div class="">
                                <input type="radio" id="r3" checked name="Gender" value="Male"/>
                                <label for="r3">Male</label>
                                <br>
                                <input type="radio" id="r4"  name="Gender" value="Female"/>
                                <label for="r4">Female</label>
                            </div>
                        </div>
                    </div>

                    <div class="row">
                        <div class="col-md-3 col-sm-3 col-xs-12 clearfix">
                            <div class="padding-t-0 ">
                                <p class=" label">Date of Birth:</p>
                            </div>
                        </div>
                        <div class="col-md-9">
                            <div class="date-input">
                                <select class="selectpicker pull-left" name="Date of Birth[month]">
                                    <option>1</option>
                                    <option>2</option>
                                    <option>3</option>
                                </select>
                                <select class="selectpicker pull-left" name="Date of Birth[day]">
                                    <option>1</option>
                                    <option>2</option>
                                    <option>3</option>
                                </select>
                                <select class="selectpicker pull-left" name="Date of Birth[year]">
                                    <option>1991</option>
                                </select>
                            </div>
                        </div>
                    </div>

                    <div class="row">
                        <div class="col-md-3 col-sm-3 col-xs-12 clearfix">
                            <div class="padding-t-0 ">
                                <p class=" label">Parent name/Guardian name:</p>
                            </div>
                        </div>
                        <div class="col-md-9">
                            <div class="">
                                <input type="text" required  value="Nico" name="Parent name/Guardian name"/>
                                <p class="font-8">The name of a parent is required if <br>the patient is under the age of 16.</p>
                            </div>
                        </div>
                    </div>


                    <div class="row">
                        <div class="col-md-3 col-sm-3 col-xs-12 clearfix">
                            <div class="padding-t-0 ">
                                <p class=" label">Does the patient need <br>an interpreter?</p>
                            </div>
                        </div>
                        <div class="col-md-9">
                            <div class=" ">
                                <input type="radio" id="r3" checked name="Does the patient need an interpreter" value="No"/>
                                <label for="r3">No</label>
                                <br>
                                <input type="radio" id="r3"  name="Does the patient need an interpreter" value="Male"/>
                                <label for="r3">Male</label>
                                <br>
                                <input type="radio" id="r4"  name="Does the patient need an interpreter" value="Female"/>
                                <label for="r4">Female</label>
                            </div>
                        </div>
                    </div>

                    <hr>


                    <h1>Patient Insurance Information</h1>

                    <div class="row">
                        <div class="col-md-3 col-sm-3 col-xs-12 clearfix">
                            <div class="padding-t-0 ">
                                <p class="">Does the patient have
                                    health insurance?</p>
                            </div>
                        </div>
                        <div class="col-md-9">
                            <div class="">
                                <input type="radio" id="r1" checked name="Does the patient have health insurance" value="Yes"/>
                                <label for="r1">Yes</label>
                                <br>
                                <input type="radio" id="r2" name="Does the patient have health insurance" value="No"/>
                                <label for="r2">No</label>
                            </div>
                        </div>
                    </div>

                    <hr>

                    <h1>Medical Concern</h1>

                    <div class="row">
                        <div class="col-md-3 col-sm-3 col-xs-12 clearfix">
                            <div class="padding-t-0 ">
                                <p class="">What is the primary medical
                                    problem or diagnosis for the
                                    appointment request?</p>
                            </div>
                        </div>
                        <div class="col-md-9">
                            <div class="">
                                <textarea id="r1" rows="5" required  name="What is the primary medical problem or diagnosis for the appointment request">Problem</textarea>
                            </div>
                        </div>
                    </div>

                    <div class="row">
                        <div class="col-md-3 col-sm-3 col-xs-12 clearfix">
                            <div class="padding-t-0 ">
                                <p class="">How long has the patient had this problem?</p>
                            </div>
                        </div>
                        <div class="col-md-9">
                            <div class="date-input">
                                <select class="selectpicker" name="How long has the patient had this problem[month]">
                                    <option>1</option>
                                    <option>2</option>
                                    <option>3</option>
                                </select>
                                <select class="selectpicker" name="How long has the patient had this problem[year]">
                                    <option>1991</option>
                                </select>
                            </div>
                        </div>
                    </div>

                    <div class="row">
                        <div class="col-md-3 col-sm-3  col-xs-12 clearfix">
                            <div class="padding-t-0 ">
                                <p class="">Are there additional medical
                                    problems the patient needs
                                    assessed during this visit?</p>
                            </div>
                        </div>
                        <div class="col-md-9">
                            <div class="">
                                <textarea id="r1" rows="5" required  name="Are there additional medical problems the patient needs assessed during this visit"></textarea>
                                <p class="font-8">You have 300 characters remaining out of 300 total. (optional)</p>
                            </div>
                        </div>
                    </div>

                    <hr>
                    <p class="brown-text standard-bold font-13">Important: After submission, please do not leave this form until you see the confirmation message.</p>

                    <div class="row">
                        <div class="col-md-9 col-md-offset-3">
                            <div class="padding-t-10">
                                <input type="submit" value="Send Request">
                            </div>
                        </div>
                    </div>

                    </form>
                </div>
            </div>
        </div>
    </div>
</section>
<!-- /.content -->

@endsection