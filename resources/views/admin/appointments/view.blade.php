@extends('admin.master')

@section('content')
        <!-- Content Header (Page header) -->
<section class="content-header">
    <h1>
        Appointment Requests
        <small>Control panel</small>
    </h1>
</section>

<!-- Main content -->
<section class="content">
    <div class="box box-info">
        <div class="box-header">
            <div class="col-md-12">
                <h3 class="box-title">Appointment Request from {{ $data->name }}</h3>
                <a class="pull-right" href="{{ route('admin.appointments.index') }}">Back</a>

                <!-- /. tools -->
            </div>
        </div>
        <!-- /.box-header -->

        <div class="row">
            <div class="col-md-12">
                <div class="box-body pad">

                    <div class="col-md-12">
                        @foreach($data->info as $item)
                            <b>{{ $item->key }}: </b><br>
                            <p>{{ $item->value }}</p>
                        @endforeach
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
<!-- /.content -->

@endsection