@extends('admin.master')

@section('content')
        <!-- Content Header (Page header) -->
<section class="content-header">
    <h1>
        Career
        <small>Control panel</small>
    </h1>
</section>

<!-- Main content -->
<section class="content">
    <div class="box box-info">
        <div class="box-header">
            <div class="col-md-12">
            @if(Session::has('success'))
                <div class="alert alert-success">
                    {{ Session::get('success') }}
                </div>
            @endif

            <h3 class="box-title">{{ isset($data) ? 'Edit' : 'Add' }} Career</h3>

            <!-- /. tools -->
            </div>
        </div>
        <!-- /.box-header -->

        <div class="row">
            <div class="col-md-12">
                <div class="box-body pad">
                    {!! Form::open(['route'=> isset($data) ? 'admin.careers.update' : 'admin.careers.store','files'=>true]) !!}

                        @if(isset($data))
                            <input type="hidden" name="id" value="{{ $data->id }}">
                        @endif

                        <div class="col-md-6">
                            <div class="form-group">
                                <label>Name</label>
                                <input type="text" name="name" class="form-control" value="{{ isset($data) ? $data->name : '' }}">
                            </div>
                        </div>

                        <div class="col-md-12">
                            <div class="form-group">
                                <label>Description (Optional)</label>
                                <textarea class="editor" name="description" rows="10" cols="80">{{ isset($data) ? $data->description : '' }}</textarea>
                            </div>
                        </div>

                        <div class="col-md-12">
                            <div class="form-group">
                                <input type="submit" class="form-control">
                            </div>
                        </div>
                    {!! Form::close() !!}
                </div>
            </div>
        </div>
    </div>
</section>
<!-- /.content -->

@endsection