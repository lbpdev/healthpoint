@extends('admin.master')

@section('custom-css')
    <link rel="stylesheet" href="{{ asset('public/admin') }}/plugins/datatables/dataTables.bootstrap.css">
    @endsection

    @section('content')
            <!-- Content Header (Page header) -->
    <section class="content-header">
        <h1>
            Files
            <small>Control panel</small>
        </h1>
    </section>

    <!-- Main content -->
    <section class="content">

        <div class="box">
            <div class="box-header"></div>
            <div class="box-body">

                <div class="col-md-4 col-md-offset-4">
                    <h4>Upload a new file:</h4>

                    {!! Form::open(['route'=> 'admin.uploads.store','files'=>true]) !!}
                        <div class="form-group">
                            <label>File Name</label>
                            <input type="text" name="name" required class="form-control" value="">
                        </div>

                        <div class="form-group">

                            <label>Select File</label>
                            <input type="file" class="form-control"  value="" name="file">
                        </div>

                        <div class="form-group">
                            <input type="submit" name="Upload" class="form-control">
                        </div>

                    {!! Form::close() !!}
                </div>
                @if(Session::has('success'))
                    <div class="col-md-12 alert alert-success">
                        {{ Session::get('success') }}
                    </div>
                @endif

                <div class="col-md-12">
                    <div class="row">
                        <h4>Files</h4>
                    </div>
                </div>
                <table width="100%" class="table table-bordered table-hover">
                    <thead>
                    <tr>
                        <th>Name</th>
                        <th>Source</th>
                        <th>Date Added</th>
                    </tr>
                    </thead>
                    <tbody>
                    @foreach($data as $item)
                        <tr>
                            <td>{{ $item->name }}</td>
                            <td><a target="_blank" href="{{ asset('public/'.$item->src) }}">{{ asset('public/'.$item->src) }}</a></td>
                            <td>{{ $item->created_at->format('M d Y H:m') }}</td>
                            <td>
                                <a href="{{ route('admin.uploads.delete',$item->id) }}" onclick="return window.confirm('Are you sure you want to delete this?');">Delete</a>
                            </td>
                        </tr>
                    @endforeach
                    </tbody>
                    <tfoot>
                    <tr>
                        <th>Name</th>
                        <th>Source</th>
                        <th>Date Added</th>
                    </tr>
                    </tfoot>
                </table>
            </div>
        </div>

    </section>
    <!-- /.content -->

@endsection

@section('custom-js')
    <script src="{{ asset('public/admin') }}/plugins/datatables/jquery.dataTables.min.js"></script>
    <script src="{{ asset('public/admin') }}/plugins/datatables/dataTables.bootstrap.min.js"></script>
    <script>
        $('table').DataTable({
            "order": [[ 2, "desc" ]],
            "pageLength": 14,
            "paging": true,
            "lengthChange": false,
            "searching": false,
            "ordering": true,
            "info": true,
            "autoWidth": false
        });
    </script>
@endsection