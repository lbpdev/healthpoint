@extends('admin.master')

@section('content')
        <!-- Content Header (Page header) -->
<section class="content-header">
    <h1>
        Management
        <small>Control panel</small>
    </h1>
</section>

<!-- Main content -->
<section class="content">
    <div class="box box-info">
        <div class="box-header">
            <div class="col-md-12">
            @if(Session::has('success'))
                <div class="alert alert-success">
                    {{ Session::get('success') }}
                </div>
            @endif

            <h3 class="box-title">{{ isset($data) ? 'Edit' : 'Add' }} Management User</h3>

            <!-- /. tools -->
            </div>
        </div>
        <!-- /.box-header -->

        <div class="row">
            <div class="col-md-12">
                <div class="box-body pad">
                    {!! Form::open(['route'=> isset($data) ? 'admin.management.update' : 'admin.management.store','files'=>true]) !!}

                    @if(isset($data))
                        <div class="col-md-12">
                            <img src="{{ $data->thumbnail }}" height="100">
                            <br>
                            Remove Thumbnail &nbsp;<input type="checkbox" name="remove_thumb">
                            <br>
                            <br>
                        </div>
                    @endif

                        @if(isset($data))
                            <input type="hidden" name="id" value="{{ $data->id }}">
                        @endif
                        <div class="col-md-6">
                            <div class="form-group">
                                <label>Name</label>
                                <input type="text" name="name" class="form-control" value="{{ isset($data) ? $data->name : '' }}">
                            </div>
                            <div class="form-group">
                                <label>Position</label>
                                <input type="text" name="position" class="form-control" value="{{ isset($data) ? $data->position : '' }}">
                            </div>
                            <div class="form-group">

                                <label>Photo</label>
                                <input type="file" class="form-control"  value="" name="thumbnail">
                            </div>
                        </div>

                        <div class="col-md-12">
                            <div class="form-group">
                                <label>Description (Optional)</label>
                                <textarea class="editor" name="description" rows="10" cols="80">{{ isset($data) ? $data->description : '' }}</textarea>
                            </div>

                        </div>

                        <div class="col-md-12">
                            <div class="form-group">
                                <input type="submit" class="form-control">
                            </div>
                        </div>
                    {!! Form::close() !!}
                </div>
            </div>
        </div>
    </div>
</section>
<!-- /.content -->

@endsection