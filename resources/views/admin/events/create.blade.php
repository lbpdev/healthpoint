@extends('admin.master')

@section('content')
        <!-- Content Header (Page header) -->
<section class="content-header">
    <h1>
        Events
        <small>Control panel</small>
        <a class="pull-right" style="font-size: 16px;" href="{{ route('admin.events.create') }}">+ Add Event</a>
    </h1>
</section>

<!-- Main content -->
<section class="content">
    <div class="box box-info">
        <div class="box-header">
            <div class="col-md-12">
            @if(Session::has('success'))
                <div class="alert alert-success">
                    {{ Session::get('success') }}
                </div>
            @endif

            <h3 class="box-title">{{ isset($data) ? 'Edit' : 'Add' }} Event</h3>

            <!-- /. tools -->
            </div>
        </div>
        <!-- /.box-header -->

        <div class="row">
            <div class="col-md-12">
                <div class="box-body pad">
                    {!! Form::open(['route'=> isset($data) ? 'admin.events.update' : 'admin.events.store','files'=>true]) !!}

                    @if(isset($data))
                        <div class="col-md-12">
                            <img src="{{ $data->thumbnail }}" height="100">
                            <br>
                            Remove Thumbnail &nbsp;<input type="checkbox" name="remove_thumb">
                            <br>
                            <br>
                        </div>
                    @endif

                        @if(isset($data))
                            <input type="hidden" name="id" value="{{ $data->id }}">
                        @endif
                        <div class="col-md-6">
                            <div class="form-group">
                                <label>Title</label>
                                <input type="text" name="title" required class="form-control" value="{{ isset($data) ? $data->title : '' }}">
                            </div>
                            <div class="form-group">
                                <label>Sub-title (Optional)</label>
                                <input type="text" name="subtitle" value="{{ isset($data) ? $data->subtitle : '' }}" class="form-control">
                            </div>
                            <div class="form-group">
                                <label>Date</label>
                                <input type="text"  name="date" value="{{ isset($data) ? $data->date : '' }}" class="datepicker form-control">
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="form-group">
                                <label>Category</label>

                                <?php $categories = [
                                    'seminar' => 'Seminar',
                                    'campaign' => 'Campaign',
                                    'event' => 'Event',
                                ] ?>
                                {!! Form::select('category',$categories, isset($data) ? $data->category : 0 ,['class'=>'form-control']) !!}
                            </div>
                            <div class="form-group">

                                <label>Thumbnail</label>
                                <input type="file" class="form-control"  value="" name="thumbnail">
                            </div>
                            <div class="form-group">
                                <label>Time</label>
                                <input type="text" name="time"  value="{{ isset($data) ? $data->time : '' }}" class="form-control">
                            </div>
                        </div>

                        <div class="col-md-12">
                            <div class="form-group">
                                <label>Location</label>
                                <input type="text" name="location"  value="{{ isset($data) ? $data->location : '' }}" class="form-control">
                            </div>
                            <div class="form-group">
                                <label>Content</label>
                                <textarea class="editor" name="content" rows="10" cols="80">{{ isset($data) ? $data->content : '' }}</textarea>
                            </div>
                        </div>

                        <div class="col-md-12">
                            <div class="form-group">
                                <label>External Link (Optional)</label>
                                <input type="text" class="form-control"  value="{{ isset($data) ? $data->external_link : '' }}" name="external_link">
                            </div>
                        </div>

                        <div class="col-md-12">
                            <div class="form-group">
                                <input type="submit" class="form-control">
                            </div>
                        </div>
                    {!! Form::close() !!}
                </div>
            </div>
        </div>
    </div>
</section>
<!-- /.content -->

@endsection

@section('custom-js')
    <script>
        $('.datepicker').datepicker();
    </script>
@endsection