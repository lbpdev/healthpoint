@extends('admin.master')

@section('custom-css')
    <link rel="stylesheet" href="{{ asset('public/admin') }}/plugins/datatables/dataTables.bootstrap.css">
@endsection

@section('content')
        <!-- Content Header (Page header) -->
<section class="content-header">
    <h1>
        Videos
        <small>Control panel</small>
    </h1>
</section>

<!-- Main content -->
<section class="content">

    <div class="box">
        <div class="box-header"></div>
        <div class="box-body">
            <a href="{{ route('admin.videos.create') }}">+ Add Videos</a>
            @if(Session::has('success'))
                <div class="col-md-12 alert alert-success">
                    {{ Session::get('success') }}
                </div>
            @endif
            <table width="100%" class="table table-bordered table-hover">
                <thead>
                <tr>
                    <th>Name</th>
                    <th>Description</th>
                    <th>Source</th>
                    <th>Category</th>
                    <th>Date Added</th>
                </tr>
                </thead>
                <tbody>
                    @foreach($data as $item)
                        <tr>
                            <td>{{ $item->name }}</td>
                            <td>{!!  \Illuminate\Support\Str::limit(strip_tags($item->description),80) !!}</td>
                            <td>{{ $item->source }}</td>
                            <td>{{ $item->category }}</td>
                            <td>{{ $item->created_at->format('M d Y H:m') }}</td>
                            <td>
                                <a href="{{ route('admin.videos.edit',$item->id) }}">Edit</a>
                                |
                                <a href="{{ route('admin.videos.delete',$item->id) }}" onclick="return window.confirm('Are you sure you want to delete this?');">Delete</a>
                            </td>
                        </tr>
                    @endforeach
                </tbody>
                <tfoot>
                <tr>
                    <th>Name</th>
                    <th>Description</th>
                    <th>Source</th>
                    <th>Category</th>
                    <th>Date Added</th>
                </tr>
                </tfoot>
            </table>
        </div>
    </div>

</section>
<!-- /.content -->

@endsection

@section('custom-js')
    <script src="{{ asset('public/admin') }}/plugins/datatables/jquery.dataTables.min.js"></script>
    <script src="{{ asset('public/admin') }}/plugins/datatables/dataTables.bootstrap.min.js"></script>
    <script>
        $('table').DataTable({
            "order": [[ 2, "desc" ]],
            "pageLength": 14,
            "paging": true,
            "lengthChange": false,
            "searching": false,
            "ordering": true,
            "info": true,
            "autoWidth": false
        });
    </script>
@endsection