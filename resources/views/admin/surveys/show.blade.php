@extends('admin.master')
<style>
    #surveyElements {
        position: relative;
        float: left;
        width: 100%;
    }
    #surveyElements .form-group {
        border: 3px solid #3694f7;
        padding: 5px 20px;
        width: 100%;
        background-color: #fff;
        position: relative;
        float: left;
    }
    #surveyElements .count, #surveyElements .delete-question, #surveyElements .move-question {
        border: 0;
        width: auto;
        background-color: #3694f7;
        width: 30px;
        height: 30px;
        text-align: center;
        padding-left: 5px;
        color: #fff;
        position: absolute;
        top: -3px;
        left: -32px;
        border-top-left-radius: 50%;
        border-bottom-left-radius: 50%;
        font-size: 16px;
    }

    #surveyElements .glyphicon {
        line-height: 30px;
        cursor: pointer;
    }

    #surveyElements .delete-question {
        top: 32px;
        left: -32px;
    }

    #surveyElements .form-group .delete-choice {
        margin-left: 5px;
        color: #fb4c4c;
    }

    #surveyElements .move-question {
        top: 67px;
        left: -32px;
    }

    .field-group input {
        margin-bottom: 5px;
        padding: 0px 10px;
    }

    .answer-required {
        border: 1px solid #ccc;
        padding: 5px 10px;
        font-weight: normal;
        margin-top: 10px;
    }

    button {
        border: 1px solid #ccc;
        background-color: #3694f7;
        color: #fff !important;
    }

    #surveyElements {
        background-color: #ebebeb;
        padding: 20px;
    }

    .addQBT {
        margin-top: 10px;
    }
</style>
@section('content')
<!-- Content Header (Page header) -->
<section class="content-header">
    <h1>
        Edit Survey
        <a style="font-size:16px; background-color: #1e9e33; color: #fff;padding: 5px 10px" class="pull-right" href="{{ route('admin.surveys.toggle-publish',$survey->id) }}">{{ $survey->is_active ? 'Unpublish' : 'Publish'}}</a>
        <a target="_blank" style="font-size:16px; margin-right:20px;background-color: #cc7620; color: #fff;padding: 5px 10px" class="pull-right" href="{{ route('admin.surveys.preview',$survey->id) }}">Preview</a>
        <a style="font-size:16px; margin-right:20px; background-color: #9e160a; color: #fff;padding: 5px 10px" class="pull-right" href="{{ route('admin.surveys.delete',$survey->id) }}" onclick="return window.confirm('Are you sure you want to delete this?');">Delete</a>
    </h1>
</section>

<!-- Main content -->
<section class="content">
    <div class="box box-info">
        <div class="box-header">
            <div class="col-md-12">
                @if(Session::has('message'))
                    <div class="alert alert-success">
                        {{ Session::get('message') }}
                    </div>
                @endif
                <!-- /. tools -->
            </div>
        </div>
        <!-- /.box-header -->

        <div class="row">
            <div class="col-md-12">
                <div class="box-body pad">
                    {!! Form::open(['route'=> 'admin.surveys.update','files'=>true,'id'=>'surveyForm']) !!}
                    {!! Form::hidden('id',$survey->id) !!}
                    <div class="col-md-6">
                        <div class="form-group">
                            <label>Title</label>
                            <input type="text" name="survey[title]" required class="form-control" value="{{ $survey->title }}">
                        </div>
                    </div>

                    <div class="col-md-12">
                        <div class="form-group">
                            <label>Description</label>
                            <textarea class="editor" name="survey[description]" rows="10" cols="80">{{ $survey->description }}</textarea>
                        </div>
                    </div>

                    <div class="col-md-12">
                        <label>Short Thumbnail ( 320x240 )</label><br>
                        <img src="{{ $survey->thumbnail }}" height="100">
                        <input type="file" class="form-control"  value="" name="thumbnail">
                        <br>
                        <br>
                        <label>Long Thumbnail ( 720x270 )</label><br>
                        <img src="{{ $survey->photo }}" height="100">
                        <br>
                        <br>
                        <input type="file" class="form-control"  value="" name="photo">

                        <hr>

                        <div class="col-md-12">
                            <div class="row">
                                Remove Thumbnails &nbsp;<input type="checkbox" name="remove_thumb">
                            </div>
                        </div>
                    </div>
                    <div class="col-md-12">
                        <hr>
                        <h3>Questions</h3>
                        <div id="surveyElements">
                            @foreach($survey->questions as $index=>$question)
                                <?php
                                $el = '';
                                $is_float = $question->is_float ? 'checked' : '';

                                if($question->type){

                                    $el = '<div class="form-group clearfix ui-state-default" data-id="'.$question->id.'">';

                                    if($question->type->slug=='text')
                                    {
                                        $el .='<label>Text Field Question </label>';
                                    } else if ($question->type->slug=='textarea')
                                    {
                                        $el .='<label>Textarea Question </label>';
                                    } else if ($question->type->slug=='radio')
                                    {
                                        $el .='<label>Multiple Choices with single answers</label>';
                                    } else if ($question->type->slug=='checkbox')
                                    {
                                        $el .='<label>Multiple Choices with multiple answers</label>';
                                    } else if ($question->type->slug=='date')
                                    {
                                        $el .='<label>Date Question </label>';
                                    }

                                    $el .='<input type="text" class="form-control" name="question['.$question->id.'][question]" style="margin-bottom: 10px;" value="'.$question->question.'" placeholder="Add question here...">';
                                    $el .='<span class="delete-question"><span class="glyphicon glyphicon-trash" aria-hidden="true"></span></span><span class="move-question"><span class="glyphicon glyphicon-move" aria-hidden="true"></span></span>';

                                    if ($question->type->slug=='radio'|| $question->type->slug=='checkbox')
                                    {
                                        $el .='<div class="row"><div class="field-group col-md-6"><label>Choices</label>';

                                        $el .= ' [ Display side by side <input name="question['.$question->id.'][is_float]" value="1" type="checkbox" '.$is_float.'> ]';
                                        $el .='<br>';

                                        $choiceCount = 1;
                                        foreach($question->choices as $question->idIn=>$choice){
                                            $el .= '<div class="choice"><input type="text" name="question['.$question->id.'][choices]['.$choiceCount.'][choice]" value="'.$choice->value.'">';

                                            if($choiceCount>1)
                                                $el .= '<span class="glyphicon glyphicon-remove-sign delete-choice" aria-hidden="true"></span>';

                                            $choiceCount++;

                                            $el .= '</div>';
                                        }

                                        $el .='<button type="button" class="addQBT">+ Add a choice</button><br>';
                                    }
                                    else if ($question->type->slug=='rate')
                                    {
                                        $el .='<div class="row"><div class="field-group col-md-6"><label>Items</label>';

                                        $el .='<br>';

                                        $choiceCount = 1;
                                        foreach($question->choices as $question->idIn=>$choice){
                                            $el .= '<div class="choice"><input type="text" name="question['.$question->id.'][choices]['.$choiceCount.'][choice]" value="'.$choice->value.'">';
                                            $el .= '<select class="rateSelect" name="question['.$question->id.'][choices]['.$choiceCount.'][rate]">';
                                            $el .= '<option value="5" '.( $choice->ratability == 5 ? 'selected' : '' ).'>1 to 5</option>';
                                            $el .= '<option value="10" '.( $choice->ratability == 10 ? 'selected' : '' ).'>1 to 10</option>';
                                            $el .= '</select>';

                                            if($choiceCount>1)
                                                $el .= '<span class="glyphicon glyphicon-remove-sign delete-choice" aria-hidden="true"></span>';

                                            $choiceCount++;

                                            $el .= '</div>';
                                        }

                                        $el .='<button type="button" class="addRBT">+ Add a item</button><br>';
                                    }
                                    else
                                    {
                                        $el .='<div class="row"><div class="field-group col-md-6"><br><br>';
                                    }

                                    $el .='<input type="hidden" name="question['.$question->id.'][type]" value="'.$question->type->slug.'">';

                                    $el .='<br></div><div class="col-md-6"><label class="pull-right answer-required">Answer required <input name="question['.$question->id.'][is_required]" value="1" type="checkbox" '.( $question->is_required ? 'checked' : '').'></label></div>';

                                    $el .='</div>';

                                    $el .='<input type="text" class="count" readonly="readonly" name="question['.$question->id.'][order]" value="'.$question->order.'">';

                                    $el .='</div>';

                                    echo $el;
                                }
                                ?>
                            @endforeach
                        </div>
                        <hr>
                        <label>Type</label>
                        {!! Form::select('survey[type]',$data['types'],$survey->survey_question_type_id,['class'=>'selectpicker form-control','id'=>'typeSelector']) !!}
                        <br>
                        <button id="addQuestionBt" type="button" class="pull-left">Add a question</button>
                        <button id="submitForm" class="pull-right">Save Survey</button>
                    </div>

                    {!! Form::close() !!}

                </div>
            </div>
        </div>
    </div>
</section>
<!-- /.content -->

@endsection

@section('custom-js')
    <script src="{{ asset('public/js/survey.js') }}"></script>
    <script>
        addQBTClickEvent();
        addRBTClickEvent();
        addDeleteQuestionClickEvent();
        updateSortable();
    </script>
@endsection