@extends('admin.master')

<link rel="stylesheet" href="{{ asset('public/css/main.css') }}">
<link rel="stylesheet" href="{{ asset('public/css/genestrap.css') }}">
<style>
    #surveyElements {
        position: relative;
        float: left;
        width: 100%;
    }
    #surveyElements .form-group {
        border: 3px solid #3694f7;
        padding: 5px 20px;
        width: 100%;
        background-color: #fff;
        position: relative;
        float: left;
    }
    #surveyElements .count, #surveyElements .delete-question, #surveyElements .move-question {
        border: 0;
        width: auto;
        background-color: #3694f7;
        width: 30px;
        height: 30px;
        text-align: center;
        padding-left: 5px;
        color: #fff;
        position: absolute;
        top: -3px;
        left: -32px;
        border-top-left-radius: 50%;
        border-bottom-left-radius: 50%;
        font-size: 16px;
    }

    #surveyElements .glyphicon {
        line-height: 30px;
        cursor: pointer;
    }

    #surveyElements .delete-question {
        top: 32px;
        left: -32px;
    }

    #surveyElements .form-group .delete-choice {
        margin-left: 5px;
        color: #fb4c4c;
    }
    #surveyElements .move-question {
        top: 67px;
        left: -32px;
    }

    .field-group input {
        margin-bottom: 5px;
        padding: 0px 10px;
    }

    .answer-required {
        border: 1px solid #ccc;
        padding: 5px 10px;
        font-weight: normal;
        margin-top: 10px;
    }

    button,input[type="submit"] {
        border: 1px solid #ccc;
        background-color: #3694f7;
        color: #fff !important;
    }

    #surveyElements {
        background-color: #ebebeb;
        padding: 20px;
    }
    input[type=text], select, textarea {
        background-color: #e4e1df;
        border: 0;
        padding: 8px 12px;
        font-size: 13px;
        min-width: 200px;
    }
    input[type=text], select, textarea {
        background-color: #e4e1df;
        border: 0;
        padding: 8px 12px;
        font-size: 13px;
        resize: none;
        min-width: 300px;
    }
</style>
@section('content')
        <!-- Content Header (Page header) -->
<section class="content-header">
    <h1>
        Preview Survey
    </h1>
</section>

<!-- Main content -->
<section class="content">
    <div class="box box-info">
        <!-- /.box-header -->

        <div class="row">
            <div class="col-md-12">
                <div class="box-body pad">
                    @if($data)
                        <h1 class="panel-title">{{ $data->title }}</h1>

                        @if($data->photo || $data->thumbnailLong)
                            <img src="{{ $data->photo ? $data->photo : $data->thumbnailLong }}" width="100%" class="margin-b-10">
                        @endif

                        {!! str_replace("&nbsp;"," ",$data->description) !!}
                        <input type="hidden" name="id" value="{{ $data->id }}">
                        @foreach($data->questions as $index=>$question)
                            <?php $qID = $data->questions[$index]->id; ?>
                            <div class="form-group clearfix ui-state-default" data-id="{{ $question->id }}">
                                <label>{{ $question->question }}</label>
                                <br>
                                @if($question->type->slug=='radio' || $question->type->slug=='checkbox')
                                    @foreach($question->choices as $index=>$choice)
                                        @if($question->is_float)
                                            <div class="pull-left margin-r-20">
                                                <input id="{{$choice->id}}"  {{ $index==0 ? 'checked' : '' }} type="{{ $question->type->slug }}" name="questions[{{$qID}}][]" value="{{$choice->value}}">
                                                <label for="{{$choice->id}}">{{$choice->value}}</label>
                                            </div>
                                        @else
                                            <input id="{{$choice->id}}"  {{ $index==0 ? 'checked' : '' }} type="{{ $question->type->slug }}" name="questions[{{$qID}}][]" value="{{$choice->value}}">
                                            <label for="{{$choice->id}}">{{$choice->value}}</label>
                                            <br>
                                        @endif
                                    @endforeach
                                @elseif($question->type->slug=='rate')
                                    @foreach($question->choices as $cIndex=>$choice)
                                        <div class="choice clearfix" style="width: 100%">
                                            <input type="hidden" name="questions[{{ $question->id }}][choices][{{$choice->id}}][choice_id]" value="{{$choice->id}}">
                                            <div class="pull-left margin-r-20">
                                                {{ $choice->value  }}
                                            </div>
                                            <div class="pull-left margin-r-20">
                                                @for($x=0;$x<$choice->ratability;$x++)
                                                    <input id="{{$choice->id}}{{$x}}" {{ $x==0 ? 'checked' : '' }} type="radio" name="questions[{{ $question->id }}][choices][{{$choice->id}}][rate]" value="{{$x+1}}">
                                                    <label for="{{$choice->id}}{{$x}}" class="margin-r-10">{{$x+1}}</label>
                                                @endfor
                                            </div>
                                        </div>
                                    @endforeach
                                @elseif($question->type->slug=='textarea')
                                    <textarea name="questions[{{$qID}}]" {{ $question->is_required ? 'required="required"' : '' }}></textarea>
                                @elseif($question->type->slug=='date')
                                    <input type="text" class="datepicker" name="questions[{{$qID}}]" {{ $question->is_required ? 'required="required"' : '' }}>
                                @else
                                    <input type="text" name="questions[{{$qID}}]" {{ $question->is_required ? 'required="required"' : '' }}>
                                @endif
                            </div>

                        @endforeach
                    @else
                        <h1 class="alert-danger alert">Article not found.</h1>
                    @endif

                </div>
            </div>
        </div>
    </div>
</section>
<!-- /.content -->

@endsection