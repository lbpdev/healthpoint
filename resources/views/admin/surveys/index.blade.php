@extends('admin.master')

@section('custom-css')
    <link rel="stylesheet" href="{{ asset('public/admin') }}/plugins/datatables/dataTables.bootstrap.css">
    @endsection

    @section('content')
            <!-- Content Header (Page header) -->
    <section class="content-header">
        <h1>
            Surveys
            <small>Control panel</small>
        </h1>
    </section>

    <!-- Main content -->
    <section class="content">
        <div class="box">
            <div class="box-header"></div>
            <div class="box-body">
                <a href="{{ route('admin.surveys.create') }}">+ Add Survey</a>
                @if(Session::has('message'))
                    <div class="col-md-12 alert alert-success">
                        {{ Session::get('message') }}
                    </div>
                @endif
                <table width="100%" class="table table-bordered table-hover">
                    <thead>
                    <tr>
                        <th>Title</th>
                        <th>Date Created</th>
                        <th>Entries</th>
                        <th>Actions</th>
                    </tr>
                    </thead>
                    <tbody>
                    @if(count($data))
                        @foreach($data as $item)
                            <tr>
                                <td><a href="{{ route('admin.surveys.show',$item->id) }}">{{ $item->title }}</a></td>
                                <td>{{ $item->created_at->format('M d Y') }}</td>
                                <td>
                                    {{ count($item->entries) }}
                                    @if(count($item->entries) > 0)
                                        ( <a href="{{ route('admin.surveys.entries',$item->id) }}">View All</a> | <a href="{{ route('admin.surveys.excel',$item->id) }}">Export</a> )
                                    @endif
                                </td>
                                <td>
                                    <a target="_blank" href="{{ route('admin.surveys.preview',$item->id) }}">Preview</a> |
                                    <a href="{{ route('admin.surveys.toggle-publish',$item->id) }}">{{ $item->is_active ? 'Unpublish' : 'Publish'}}</a> |
                                    <a href="{{ route('admin.surveys.delete',$item->id) }}" onclick="return window.confirm('Are you sure you want to delete this?');">Delete</a>
                                </td>
                            </tr>
                        @endforeach
                    @endif
                    </tbody>
                    <tfoot>
                    <tr>
                        <th>Title</th>
                        <th>Date Created</th>
                        <th>Entries</th>
                        <th>Actions</th>
                    </tr>
                    </tfoot>
                </table>
            </div>
        </div>
    </section>
    <!-- /.content -->

@endsection

@section('custom-js')
    <script src="{{ asset('public/admin') }}/plugins/datatables/jquery.dataTables.min.js"></script>
    <script src="{{ asset('public/admin') }}/plugins/datatables/dataTables.bootstrap.min.js"></script>
    <script>
        $('table').DataTable({
            "order": [[ 1, "desc" ]],
            "pageLength": 14,
            "paging": true,
            "lengthChange": false,
            "searching": true,
            "ordering": true,
            "info": true,
            "autoWidth": false
        });
    </script>
@endsection