@extends('admin.master')
<style>
    #surveyElements {
        position: relative;
        float: left;
        width: 100%;
    }
    #surveyElements .form-group {
        border: 3px solid #3694f7;
        padding: 5px 20px;
        width: 100%;
        background-color: #fff;
        position: relative;
        float: left;
    }
    #surveyElements .count, #surveyElements .delete-question, #surveyElements .move-question {
        border: 0;
        width: auto;
        background-color: #3694f7;
        width: 30px;
        height: 30px;
        text-align: center;
        padding-left: 5px;
        color: #fff;
        position: absolute;
        top: -3px;
        left: -32px;
        border-top-left-radius: 50%;
        border-bottom-left-radius: 50%;
        font-size: 16px;
    }

    #surveyElements .glyphicon {
        line-height: 30px;
        cursor: pointer;
    }

    #surveyElements .delete-question {
        top: 32px;
        left: -32px;
    }

    #surveyElements .form-group .delete-choice {
        margin-left: 5px;
        color: #fb4c4c;
    }
    #surveyElements .move-question {
        top: 67px;
        left: -32px;
    }

    .field-group input {
        margin-bottom: 5px;
        padding: 0px 10px;
    }

    .answer-required {
        border: 1px solid #ccc;
        padding: 5px 10px;
        font-weight: normal;
        margin-top: 10px;
    }

    button,input[type="submit"] {
        border: 1px solid #ccc;
        background-color: #3694f7;
        color: #fff !important;
    }

    #surveyElements {
        background-color: #ebebeb;
        padding: 20px;
    }
</style>
@section('content')
        <!-- Content Header (Page header) -->
<section class="content-header">
    <h1>
        Events
        <small>Control panel</small>
        <a class="pull-right" style="font-size: 16px;" href="{{ route('admin.events.create') }}">+ Add Event</a>
    </h1>
</section>

<!-- Main content -->
<section class="content">
    <div class="box box-info">
        <div class="box-header">
            <div class="col-md-12">
                @if(Session::has('success'))
                    <div class="alert alert-success">
                        {{ Session::get('success') }}
                    </div>
                @endif

                <h3 class="box-title">Create Survey</h3>

                <!-- /. tools -->
            </div>
        </div>
        <!-- /.box-header -->

        <div class="row">
            <div class="col-md-12">
                <div class="box-body pad">
                    {!! Form::open(['route'=> 'admin.surveys.store','files'=>true,'id'=>'surveyForm']) !!}

                    <div class="col-md-6">
                        <div class="form-group">
                            <label>Title</label>
                            <input type="text" name="survey[title]" required class="form-control" value="">
                        </div>
                        <div class="form-group">
                            {{--<label>Survey Duration</label><br>--}}
                            {{--<div class="row">--}}
                                {{--<div class="col-md-6">--}}
                                    {{--From <input type="text"  name="survey[date_start]" value="" class="datepicker form-control">--}}
                                {{--</div>--}}
                                {{--<div class="col-md-6">--}}
                                    {{--To <input type="text"  name="survey[date_end]" value="" class="datepicker form-control">--}}
                                {{--</div>--}}
                            {{--</div>--}}
                        </div>
                    </div>

                    <div class="col-md-12">
                        <div class="form-group">
                            <label>Description</label>
                            <textarea class="editor" name="survey[description]" rows="10" cols="80"></textarea>
                        </div>
                    </div>

                    <div class="col-md-12">
                        <label>Short Thumbnail ( 320x240 )</label><br>
                        <input type="file" class="form-control"  value="" name="thumbnail">
                        <br>
                        <label>Long Thumbnail ( 720x270 )</label><br>
                        <input type="file" class="form-control"  value="" name="photo">
                    </div>

                    <div class="col-md-12">
                        <hr>
                        <h3>Questions</h3>
                        <div id="surveyElements">
                        </div>
                        <hr>
                        <label>Type</label>
                        {!! Form::select('survey[type]',$data['types'],0,['class'=>'selectpicker form-control','id'=>'typeSelector']) !!}
                        <br>
                        <button id="addQuestionBt" class="pull-left" type="button">Add a question</button>
                        <input type="submit" class="pull-right" value="Save Survey">
                    </div>

                    {!! Form::close() !!}



                </div>
            </div>
        </div>
    </div>
</section>
<!-- /.content -->

@endsection

@section('custom-js')
    <script src="{{ asset('public/js/survey.js') }}"></script>
@endsection