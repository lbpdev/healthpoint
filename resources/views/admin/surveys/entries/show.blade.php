@extends('admin.master')
<style>
    #surveyElements {
        position: relative;
        float: left;
        width: 100%;
    }
    #surveyElements .form-group {
        border: 3px solid #3694f7;
        padding: 5px 20px;
        width: 100%;
        background-color: #fff;
        position: relative;
        float: left;
    }
    #surveyElements .count, #surveyElements .delete-question, #surveyElements .move-question {
        border: 0;
        width: auto;
        background-color: #3694f7;
        width: 30px;
        height: 30px;
        text-align: center;
        padding-left: 5px;
        color: #fff;
        position: absolute;
        top: -3px;
        left: -32px;
        border-top-left-radius: 50%;
        border-bottom-left-radius: 50%;
        font-size: 16px;
    }

    #surveyElements .glyphicon {
        line-height: 30px;
        cursor: pointer;
    }

    #surveyElements .delete-question {
        top: 32px;
        left: -32px;
    }

    #surveyElements .form-group .delete-choice {
        margin-left: 5px;
        color: #fb4c4c;
    }
    #surveyElements .move-question {
        top: 67px;
        left: -32px;
    }

    .field-group input {
        margin-bottom: 5px;
        padding: 0px 10px;
    }

    .answer-required {
        border: 1px solid #ccc;
        padding: 5px 10px;
        font-weight: normal;
        margin-top: 10px;
    }

    button {
        border: 1px solid #ccc;
        background-color: #3694f7;
        color: #fff !important;
    }

    #surveyElements {
        background-color: #ebebeb;
        padding: 20px;
    }
</style>
@section('content')
        <!-- Content Header (Page header) -->
<section class="content-header">
    <h1>
        {{ $survey->title }} Entry
        <small>Control panel</small>
        <a class="pull-right" style="font-size: 16px;" href="{{ route('admin.surveys.entries',$survey->id) }}">+ Back</a>
    </h1>
</section>

<!-- Main content -->
<section class="content">
    <div class="box box-info">
        <div class="box-header">
            <div class="col-md-12">
                @if(Session::has('success'))
                    <div class="alert alert-success">
                        {{ Session::get('success') }}
                    </div>
                @endif

                <h3 class="box-title">Survey Entry</h3>

                <!-- /. tools -->
            </div>
        </div>
        <!-- /.box-header -->

        <div class="row">
            <div class="col-md-12">
                <div class="box-body pad">
                    <div class="col-md-12">
                        @foreach($data as $index=>$item)
                            <div class="form-group clearfix ui-state-default">
                                <label>{{ $item['question'] }}</label>
                                <br>
                                <p>
                                    @if(isset($item['choices']))
                                        @foreach($item['choices'] as $index=>$data)
                                            {{ $data['choice'] }} : {{ $data['rate'] }} <br>
                                        @endforeach
                                    @else
                                        @foreach($item['data'] as $index=>$data)
                                            {{ $data->value }} {{ $index+1 < count($item['data']) ? ', ' : '' }}
                                        @endforeach
                                    @endif
                                </p>
                            </div>
                        @endforeach
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
<!-- /.content -->

@endsection

@section('custom-js')
@endsection

