@extends('admin.master')

@section('content')
        <!-- Content Header (Page header) -->
<section class="content-header">
    <h1>
        Website Settings
        <small>Control panel</small>
    </h1>
</section>

<!-- Main content -->
<section class="content">
    <div class="box box-info">
        <div class="box-header">
            <div class="col-md-12">
                @if(Session::has('success'))
                    <div class="alert alert-success">
                        {{ Session::get('success') }}
                    </div>
                @endif

                <h3 class="box-title">Settings</h3>

                <!-- /. tools -->
            </div>
        </div>
        <!-- /.box-header -->

        @inject('optionFetcher','App\Services\OptionFetcher')

        <?php
            $options = $optionFetcher->getAll();

        ?>
        <div class="row">
            <div class="col-md-12">
                <div class="box-body pad">
                    {!! Form::open(['route'=> 'admin.options.update','files'=>true]) !!}

                    <div class="col-md-12">
                        <div class="form-group">
                            <label>Site title</label>
                            <input type="text" name="site-title" required class="form-control" value="{!! isset($options['site-title']) ? $options['site-title']->value: '' !!}">
                        </div>
                    </div>
                    <div class="col-md-6">
                        <div class="form-group">
                            <label>Clinic Hours:</label>
                            <textarea class="editor" name="clinic-hours" rows="10" cols="80">{!! isset($options['clinic-hours']) ? $options['clinic-hours']->value : '' !!}</textarea>
                        </div>
                    </div>
                    <div class="col-md-6">
                        <div class="form-group">
                            <label>Location:</label>
                            <textarea class="editor" name="location" rows="10" cols="80">{!! isset($options['location']) ? $options['location']->value : '' !!}</textarea>
                        </div>
                    </div>
                    <div class="col-md-6">
                        <div class="form-group">
                            <label>Contact:</label>
                            <textarea class="editor" name="contact" rows="10" cols="80">{!! isset($options['contact']) ? $options['contact']->value : '' !!}</textarea>
                        </div>
                    </div>
                    <div class="col-md-6">
                        <div class="form-group">
                            <label>Media Contact:</label>
                            <textarea class="editor" name="media-contact" rows="10" cols="80">{!! isset($options['media-contact']) ? $options['media-contact']->value : '' !!}</textarea>
                        </div>
                    </div>
                    <div class="col-md-12">
                        <div class="form-group">
                            <label>Who we are:</label>
                            <textarea class="editor" name="who-we-are" rows="10" cols="80">{!! isset($options['who-we-are']) ? $options['who-we-are']->value : '' !!}</textarea>
                        </div>
                    </div>
                    <div class="col-md-12">
                        <div class="form-group">
                            <label>Mission:</label>
                            <textarea class="editor" name="mission" rows="10" cols="80">{!! isset($options['mission']) ? $options['mission']->value : '' !!}</textarea>
                        </div>
                    </div>
                    <div class="col-md-12">
                        <div class="form-group">
                            <label>Vision:</label>
                            <textarea class="editor" name="vision" rows="10" cols="80">{!! isset($options['vision']) ? $options['vision']->value : '' !!}</textarea>
                        </div>
                    </div>
                    <div class="col-md-12">
                        <div class="form-group">
                            <label>Working for us:</label>
                            <textarea class="editor" name="working-for-us" rows="10" cols="80">{!! isset($options['working-for-us']) ? $options['working-for-us']->value : '' !!}</textarea>
                        </div>
                    </div>

                    <div class="col-md-12">
                        <div class="form-group">
                            <input type="submit" class="form-control">
                        </div>
                    </div>
                    {!! Form::close() !!}
                </div>
            </div>
        </div>
    </div>
</section>
<!-- /.content -->

@endsection

@section('custom-js')
    <script>
        $('.datepicker').datepicker();
    </script>
@endsection