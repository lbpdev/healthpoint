@extends('admin.master')

@section('content')
        <!-- Content Header (Page header) -->
<section class="content-header">
    <h1>
        About Us
        <small>Control panel</small>
    </h1>
</section>

<!-- Main content -->
<section class="content">
    <div class="box box-info">
        <div class="box-header">
            <div class="col-md-12">
                @if(Session::has('success'))
                    <div class="alert alert-success">
                        {{ Session::get('success') }}
                    </div>
                @endif

                <h3 class="box-title">Settings</h3>

                <!-- /. tools -->
            </div>
        </div>
        <!-- /.box-header -->

        @inject('optionFetcher','App\Services\OptionFetcher')

        <?php
            $options = $optionFetcher->getAll();
        ?>
        <div class="row">
            <div class="col-md-12">
                <div class="box-body pad">
                    {!! Form::open(['route'=> 'admin.options.update','files'=>true]) !!}

                    <div class="col-md-4">
                        <div class="form-group">
                            <label>Who we are:</label>
                            <textarea class="editor" name="who-we-are" rows="3" cols="80">{!! isset($options['who-we-are']) ? $options['who-we-are']->value : '' !!}</textarea>
                        </div>
                    </div>
                    <div class="col-md-4">
                        <div class="form-group">
                            <label>Mission:</label>
                            <textarea class="editor" name="mission" rows="3" cols="80">{!! isset($options['mission']) ? $options['mission']->value : '' !!}</textarea>
                        </div>
                    </div>
                    <div class="col-md-4">
                        <div class="form-group">
                            <label>Vision:</label>
                            <textarea class="editor" name="vision" rows="3" cols="80">{!! isset($options['vision']) ? $options['vision']->value : '' !!}</textarea>
                        </div>
                    </div>
                    <hr>

                    <div class="col-md-12">
                        <h3>About Us</h3>
                        <div class="form-group">
                            <label>Text:</label>
                            <textarea class="editor" name="about-us" rows="3" cols="80">{!! isset($options['about-us']) ? $options['about-us']->value : '' !!}</textarea>
                        </div>
                    </div>


                    <div class="col-md-12">
                        <h3>Mubadala Development Company</h3>
                        <div class="form-group">
                            <label>Video:</label>
                            <input type="text" name="mubadala-development-company-video" value="{!! isset($options['mubadala-development-company-video']) ? $options['mubadala-development-company-video']->value : '' !!}" class="form-control">
                            <label>Text:</label>
                            <textarea class="editor" name="mubadala-development-company" rows="3" cols="80">{!! isset($options['mubadala-development-company']) ? $options['mubadala-development-company']->value : '' !!}</textarea>
                        </div>
                    </div>

                    <div class="col-md-12">
                        <h3>Healthpoint Executive Management Team</h3>
                        <div class="form-group"><label>Text:</label>
                            <textarea class="editor" name="executive-management" rows="3" cols="80">{!! isset($options['executive-management']) ? $options['executive-management']->value : '' !!}</textarea>
                        </div>
                    </div>

                    <div class="col-md-12">
                        <h3>Mubadala Healthcare Portfolio</h3>


                        <h4>Catalyst For The Economic Diversification Of The Emirate Of Abu Dhabi</h4>
                        <label>Video:</label>
                        <input type="text" name="mubadala-portfolio-catalyst-video" value="{!! isset($options['mubadala-portfolio-catalyst-video']) ? $options['mubadala-portfolio-catalyst-video']->value : '' !!}" class="form-control">

                        <div class="form-group"><label>Text:</label>
                            <textarea class="editor" name="mubadala-portfolio-catalyst" rows="3" cols="80">{!! isset($options['mubadala-portfolio-catalyst']) ? $options['mubadala-portfolio-catalyst']->value : '' !!}</textarea>
                        </div>

                        <div class="form-group">
                        <h4>Cleveland Clinic Abu Dhabi</h4>
                            <textarea class="editor" name="mubadala-portfolio-cleveland" rows="3" cols="80">{!! isset($options['mubadala-portfolio-cleveland']) ? $options['mubadala-portfolio-cleveland']->value : '' !!}</textarea>
                        </div>

                        <div class="form-group">
                        <h4>Imperial College London Diabetes Centre (ICLDC)</h4>
                            <textarea class="editor" name="mubadala-portfolio-imperial" rows="3" cols="80">{!! isset($options['mubadala-portfolio-imperial']) ? $options['mubadala-portfolio-imperial']->value : '' !!}</textarea>
                        </div>
                    </div>

                    <div class="col-md-12">
                        <div class="form-group">
                            <input type="submit" class="form-control">
                        </div>
                    </div>
                    {!! Form::close() !!}
                </div>
            </div>
        </div>
    </div>
</section>
<!-- /.content -->

@endsection

@section('custom-js')
    <script>
        $('.datepicker').datepicker();
    </script>
@endsection