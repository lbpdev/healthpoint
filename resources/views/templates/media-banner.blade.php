@inject('newsFetcher','App\Services\ArticleFetcher')

<?php
    $latestNews = $newsFetcher->getLatestNews();
?>

@if($latestNews)
    <div class="banner-box news working-for-us" style="background-image: url('{{ $latestNews->thumbnailLong }}')">

        <div class="col-md-4 col-lg-offset-8">
            <div class="panel text-center margin-t-5">
                <a href="{{ route('media.news.single',$latestNews->slug) }}">
                    <h1>{{ $latestNews->title }}</h1>
                    <p>{{ $latestNews->created_at->format('M d Y') }}</p>
                </a>
            </div>
        </div>
    </div>
@endif