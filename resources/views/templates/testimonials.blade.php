<div class="col-md-12 pull-left">
    <div class="row padding-t-0">
        <h1>Testimonials</h1>
        <div class="row padding-t-0">

            @inject('vidFetcher', 'App\Services\VideoFetcher')

            <?php $videos = $vidFetcher->getByCategory('testimonial',4); ?>

            <ul class="float-list">

                @forelse($videos as $video)
                    <li class="col-md-3 col-sm-3">
                        <div class="videoWrapper">
                            <video width="100%" poster="{{ $video->thumbnail }} ">
                                <source src="{{ asset('public').'/'.$video->source }}" type="video/mp4">
                                Your browser does not support the video tag.
                            </video>
                            <span class="bt"></span>
                            <a href="{{ asset('public').'/'.$video->source }}" class="html5lightbox cover" data-width="780" data-height="480" title="{{ $video->name }}"></a>
                        </div>
                    </li>
                @empty
                @endforelse
                {{--<li class="col-md-3 col-sm-3">--}}
                    {{--<div class="videoWrapper">--}}
                        {{--<video width="100%" onclick="this.play()">--}}
                            {{--<source src="{{ asset('public/videos/sample.mp4') }}" type="video/mp4">--}}
                            {{--Your browser does not support the video tag.--}}
                        {{--</video>--}}
                        {{--<div class="cover">&nbsp;</div>--}}
                    {{--</div>--}}
                {{--</li>--}}
            </ul>
        </div>
    </div>
</div>