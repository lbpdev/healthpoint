<div class="widget">
    <h1 class="panel-title">More Health Tips</h1>
    <div  class="padding-l-0 padding-r-0">

        @inject('articleFetcher', 'App\Services\ArticleFetcher')

        <?php $articles = $articleFetcher->getByCategory(4,6); ?>
        <?php $current_article_id = isset($article) ? $article->id : 0; ?>

        <ul class="post-list clearfix">
            @forelse($articles as $article)

                @if($current_article_id)
                    @if($current_article_id != $article->id)
                    <li class="col-md-12 col-sm-6 col-xs-6">
                        <div class="col-md-3 col-sm-5 col-xs-4 padding-l-0">
                            <div class="row text-left">
                                <a href="{{ route('blog.single',$article->slug) }}"><img src="{{ $article->thumbnailSquare }}" width="100%"></a>
                            </div>
                        </div>
                        <div class="col-md-9 col-sm-7 col-xs-8 padding-r-0 padding-l-25">
                            <div class="row">
                                <a href="{{ route('blog.single',$article->slug) }}"><h1>{{ $article->title }}</h1></a>
                            </div>
                        </div>
                    </li>
                    @endif
                @endif
            @empty
            @endforelse
        </ul>
    </div>
</div>