<div class="clearfix widget">
    <h1 class="panel-title">Health News</h1>
    <div class="col-md-12">
        <div class="row padding-t-0">
            <ul class="article-list">
                @inject('articleFetcher', 'App\Services\ArticleFetcher')

                <?php $articles = $articleFetcher->getByCategory(5,3); ?>

                @forelse($articles as $article)
                    <li class="col-md-12 col-sm-6 col-xs-6 col-xxs-12">
                        <div class="row">
                                <div class="col-md-5 col-sm-5 col-xs-5 padding-l-0">
                                    <a href="{{ route('media.news.single',$article->slug) }}">
                                        <img src="{{ $article->thumbnailSquare }}" width="95%">
                                    </a>
                                </div>
                                <div class="col-md-7 col-sm-7 col-xs-7">
                                    <div class="row">
                                        <a href="{{ route('media.news.single',$article->slug) }}"><h1>{{ $article->title }}</h1></a>
                                    </div>
                                </div>
                            </a>
                        </div>
                    </li>
                @empty
                    <li class="col-md-12 col-sm-6 col-xs-6 col-xxs-12">
                        <div class="row">
                            None
                        </div>
                    </li>
                @endforelse

                {{--<li class="col-md-12 col-sm-6 col-xs-6">--}}
                    {{--<div class="row">--}}
                            {{--<div class="col-md-5 col-sm-5 col-xs-5 padding-l-0">--}}
                                {{--<a href="#">--}}
                                    {{--<img src="{{ asset('public/images/placeholders/square-sm.jpg') }}" width="95%">--}}
                                {{--</a>--}}
                            {{--</div>--}}
                            {{--<div class="col-md-7 col-sm-7 col-xs-7 padding-r-0">--}}
                                {{--<div class="row">--}}
                                    {{--<a href="#"><h1>Simusam fugia nimusEhent</h1></a>--}}
                                    {{--<p>As aut lab incid excea di necerit fugiant officabo...</p>--}}
                                {{--</div>--}}
                            {{--</div>--}}
                        {{--</a>--}}
                    {{--</div>--}}
                {{--</li>--}}
            </ul>
        </div>
    </div>
</div>


