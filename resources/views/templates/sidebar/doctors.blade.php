<div class="widget margin-t-20">
    <ul class="doctor-list">
        @inject('physFetcher', 'App\Services\PhysicianFetcher')

        <?php
        $physicians = [];
        $head = null;
        $subhead = null;

        if(isset($department)){

            $head = $department->head;
            $subhead = $department->subhead;

            if(count($department->physicians)){

                $physicians = [];

                foreach ($department->physicians as $data){

                    if($head){
                        if($data->id != $head->id)
                            $physicians[] = $data;
                    } else {
                        $physicians[] = $data;
                    }

//                        if(count($physicians)>4){
//                            break;
//                        }
                }
            }
        }
        //            else
        //                $physicians = $physFetcher->getLimit(3);

        ?>

        @if($head)
            <li class="col-md-12 col-sm-4 col-xs-6 padding-r-0">
                <div class="row">
                    <div class="col-md-3 col-sm-3 col-xs-12">
                        <div class="row">
                            <a href="{{ route('physician.single',$head->slug) }}"><img src="{{ $head->thumbnail }}" width="100%"></a>
                        </div>
                    </div>
                    <div class="col-md-8 col-sm-8 col-xs-12">
                        <h1><a href="{{ route('physician.single',$head->slug) }}">{{ $head->name }}</a></h1>
                        {!! $head->specialty  !!} - Head of Department
                        @if(isset($department))
                            @if($department->id==17 && $head->id == 9)
                                (Orthopedics)
                            @endif
                        @endif
                    </div>
                </div>
            </li>
        @endif


        @if($subhead)
            <li class="col-md-12 col-sm-4 col-xs-6 padding-r-0">
                <div class="row">
                    <div class="col-md-3 col-sm-3 col-xs-12">
                        <div class="row">
                            <a href="{{ route('physician.single',$subhead->slug) }}"><img src="{{ $subhead->thumbnail }}" width="100%"></a>
                        </div>
                    </div>
                    <div class="col-md-8 col-sm-8 col-xs-12">
                        <h1><a href="{{ route('physician.single',$subhead->slug) }}">{{ $subhead->name }}</a></h1>
                        {!! $subhead->specialty  !!}
                    </div>
                </div>
            </li>
        @endif


        @forelse($physicians as $physician)
            <li class="col-md-12 col-sm-4 col-xs-6 padding-r-0">
                <div class="row">
                    <div class="col-md-3 col-sm-3 col-xs-12">
                        <div class="row">
                            <a href="{{ route('physician.single',$physician->slug) }}"><img src="{{ $physician->thumbnail }}" width="100%"></a>
                        </div>
                    </div>
                    <div class="col-md-8 col-sm-8 col-xs-12">
                        <h1><a href="{{ route('physician.single',$physician->slug) }}">{{ $physician->name }}</a></h1>
                        {!! $physician->specialty  !!}
                        @if(isset($department))
                            @if($department->id==17 && $physician->id == 9)
                                - Head of Department (Orthopedics)
                            @elseif($department->id==17 && $physician->id == 44)
                                - Head of Department (Spine)
                            @endif
                        @endif

                    </div>
                </div>
            </li>
        @empty
        @endforelse
    </ul>
</div>