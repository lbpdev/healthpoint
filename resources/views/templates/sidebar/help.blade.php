<div class="help-box clearfix brownish-bg">
    <h1>Let Us Help You</h1>
    <ul id="help-icon-list">
        <li><a href="mailto:info@healthpoint.ae"><span class="icon-text phone">Contact Us</span></a></li>
        <li><a href="{{ url('request-appointment') }}"><span class="icon-text calendar">Make an appointment</span></a> </li>
        <li><a href="{{ url('find-a-doctor') }}"><span class="icon-text doctor">Find a physician</span></a> </li>
        <li><a href="{{ url('clinical-services') }}"><span class="icon-text search">Find a service</span></a> </li>
        <li><a href="{{ url('patients/insurance') }}"><span class="icon-text insurance">Insurance information</span></a> </li>
    </ul>
</div>