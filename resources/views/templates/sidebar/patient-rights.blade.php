<div class="clearfix widget">
    <h1 class="panel-title">Patient Rights</h1>


    @inject('optionFetcher','App\Services\OptionFetcher')

    <div class="col-md-12">
        <div class="row padding-t-0">
            <div class="panel-group" id="accordion">
                <div class="panel panel-default">
                    <div class="panel-heading">
                        <h4 class="panel-title">
                            <a data-toggle="collapse" data-parent="#accordion" href="#collapse1" class="collapsed">
                                Healthpoint patients and their families have the right:</a>
                        </h4>
                    </div>
                    <div id="collapse1" class="panel-collapse collapse">
                        <div class="panel-body can-have-lists">
                            {!! str_replace("&nbsp;"," ",$optionFetcher->getBySlug('patient-rights')) !!}
                        </div>
                    </div>
                </div>
                <div class="panel panel-default">
                    <div class="panel-heading">
                        <h4 class="panel-title">
                            <a data-toggle="collapse" data-parent="#accordion" href="#collapse2">
                                Healthpoint patients and their families are responsible to:</a>
                        </h4>
                    </div>
                    <div id="collapse2" class="panel-collapse collapse in">
                        <div class="panel-body can-have-lists">
                            {!! str_replace("&nbsp;"," ",$optionFetcher->getBySlug('patient-responsibilities')) !!}
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>