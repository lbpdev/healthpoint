<div class="col-md-12 clearfix">
    <div class="row clearfix">
        <h1 class="panel-title padding-b-5">Share on</h1>
        <div  class="padding-l-0 padding-r-0">
            <ul class="share-list pull-left width-full margin-b-0">
                <li><a class="st_facebook_large share-icon-text fb relative" href="#">Facebook</a></li>
                <li><a class="st_twitter_large share-icon-text tw relative" href="#">Twitter</a></li>
                <li><a class="st_linkedin_large share-icon-text lk relative" href="#">LinkedIn</a></li>
                <li><a href="mailto:?subject=I wanted you to see this site&amp;body=Check out this site {{ \Illuminate\Support\Facades\Request::fullUrl() }}" class="st_email_large share-icon-text em relative">E-mail</a></li>
            </ul>
        </div>
    </div>
</div>