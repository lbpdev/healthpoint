<div class="widget">
    <h1 class="gray-header"><img src="{{ asset('public/images/icons/social/Tweets-Icon.png') }}" title="Twitter Icon" height="25"> Tweets</h1>
    <div  class="yellowish-bg padding-l-0 padding-r-0">

        @inject('tweetFetcher', 'App\Services\TweetFetcher')

        <?php $tweets = $tweetFetcher->getTweets(); ?>

        <ul class="tweet-list clearfix">

            @foreach($tweets as $tweet)

                <?php
                if (!preg_match('/\p{Arabic}/u', $tweet->text)) // '/[^a-z\d]/i' should also work.
                {
                ?>
                    <li class="col-md-12 col-sm-6 col-xs-6 col-xxs-12">
                        <div class="col-md-3 col-sm-3 col-xs-3 padding-l-0">
                            <a href="https://twitter.com/HealthpointUAE">
                                <img src="{{ asset('public/images/healthpoint-twitter.jpg') }}" width="100%">
                            </a>
                        </div>
                        <div class="col-md-9 col-sm-9 col-xs-9">
                            <div class="row">
                                <a target="_blank" href="https://twitter.com/HealthpointUAE"><h1>Healthpoint UAE</h1></a>
                                <a target="_blank" href="https://twitter.com/HealthpointUAE"><span>‏@HealthpointUAE</span></a>
                                <p>{!! $tweet->text !!}</p>
                            </div>
                        </div>
                    </li>
                <?php
                }
                ?>
            @endforeach
        </ul>
    </div>
</div>