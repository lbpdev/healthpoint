<div class="col-md-12 pull-left">
    <div class="row">
        <h1 class="panel-title">Partners</h1>
        <p>Round the clock and/or as needed analgesics (either orally or ​intravenously)
            Patient-controlled analgesia, in which the patient gives himself the analgesics whenever demanded just by pressing a button
            Continuous local anesthesia infiltration via catheter directly into the surgical site or in the epidural site</p>
    </div>

    <div class="row page padding-t-0">
        <div class="row page">

            @inject('articleFetcher', 'App\Services\ArticleFetcher')

            <?php $articles = $articleFetcher->getByCategory(7,5); ?>


            <ul class="float-list">

                @forelse($articles as $article)

                <li class="col-md-4 col-sm-6 col-xs-12 gray-border-right">
                    <a href="{{ route('page.single',$article->slug) }}">
                        <img src="{{ $article->thumbnail }}" width="100%">
                        <h1 class="margin-t-8">{{ $article->title }}</h1>
                    </a>
                    <p>{!! \Illuminate\Support\Str::limit(strip_tags($article->content),140) !!}</p>
                </li>

                @empty
                    <li class="col-md-4">None</li>
                @endforelse
            </ul>
        </div>
    </div>
</div>