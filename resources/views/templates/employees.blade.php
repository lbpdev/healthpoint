
@inject('physFetcher', 'App\Services\PhysicianFetcher')

<?php $physicians = $physFetcher->getLimit(4); ?>
<?php $featured = $physFetcher->getFeatured(); ?>

<div class="row pull-left">
    <div class="col-md-4">
        <h1>Featured Employee Profile</h1>
        <div class="featured-user">
            <a href="{{ route('physician.single',$featured->slug) }}">
                <img src="{{ $featured->thumbnailFull }}" width="100%">
                <h1>{{ $featured->name }}</h1>
            </a>
            <p>{{ $featured->position }}</p>
        </div>
    </div>
    <div class="col-md-8 col-xs-top-20">
        <h1>Meet Our Physicians</h1>

        <ul class="doctor-list col-xs-right-n12 col-xs-left-n12">
                <div class="clearfix">
                    @forelse($physicians as $physician)
                        <li class="col-md-6 col-sm-6  col-xs-6 padding-r-0">
                            <div class="row">
                                <div class="col-md-3 col-sm-3 col-xs-12">
                                    <div class="row">
                                        <a href="{{ route('physician.single',$physician->slug) }}"><img src="{{ $physician->thumbnail }}" width="100%"></a>
                                    </div>
                                </div>
                                <div class="col-md-8 col-sm-8 col-xs-12 gray-border-right">
                                    <h1 class="margin-b-0"><a href="{{ route('physician.single',$physician->slug) }}">{{ $physician->name }}</a></h1>
                                    {!! $physician->position !!}
                                </div>
                            </div>
                        </li>
                    @empty
                    @endforelse
                    {{--<li class="col-md-6 col-sm-6 col-xs-6">--}}
                        {{--<div class="row">--}}
                            {{--<div class="col-md-3 col-sm-3 col-xs-12">--}}
                                {{--<div class="row">--}}
                                    {{--<a href="{{ url('/find-a-doctor/doctor') }}"><img src="{{ asset('public/images/placeholders/rectangle-y-sm.jpg') }}" width="100%"></a>--}}
                                {{--</div>--}}
                            {{--</div>--}}
                            {{--<div class="col-md-9 col-sm-9 col-xs-12">--}}
                                {{--<h1 class="margin-b-0"><a href="{{ url('/find-a-doctor/doctor') }}">Suhail Mahmood Al Ansari</a></h1>--}}
                                {{--<p>Executive Director,</p>--}}
                                {{--<p>Mubadala Healthcare;</p>--}}
                                {{--<p>Chairman, Healthpoint</p>--}}
                            {{--</div>--}}
                        {{--</div>--}}
                    {{--</li>--}}
                </div>
        </ul>
    </div>
</div>