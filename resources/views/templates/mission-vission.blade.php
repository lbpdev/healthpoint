@inject('optionFetcher','App\Services\OptionFetcher')

<?php
$who = $optionFetcher->getBySlug('who-we-are');
$mission = $optionFetcher->getBySlug('mission');
$vision = $optionFetcher->getBySlug('vision');
?>

<div class="banner-box about tabbed">
    <div class="col-md-12 col-sm-12 col-xs-12 margin-b-20">
        <div class="row tabs padding-t-0">
            <ul class="float-list">
                <li class="col-md-3 col-sm-3 col-xs-3 active"><div class="row padding-t-0"><a href="#">Who We Are</a> </div></li>
                <li class="col-md-3 col-sm-3 col-xs-3"><div class="row padding-t-0"><a href="#">Mission</a> </div></li>
                <li class="col-md-3 col-sm-3 col-xs-3"><div class="row padding-t-0"><a href="#">Vision</a> </div></li>
                <li class="col-md-3 col-sm-3 col-xs-3"><div class="row padding-t-0"><a href="#">Values</a> </div></li>
            </ul>
        </div>
    </div>
    <div class="col-md-4 col-md-offset-8 tab-contents active">
        <div class="panel text-center margin-t-5">
            <h1>Who We Are</h1>
            {!! $who !!}
        </div>
    </div>
    <div class="col-md-4 col-md-offset-8 tab-contents">
        <div class="panel text-center margin-t-5">
            <h1>Mission</h1>
            {!! $mission !!}
        </div>
    </div>
    <div class="col-md-4 col-md-offset-8 tab-contents">
        <div class="panel text-center margin-t-5">
            <h1>Vision</h1>
            {!! $vision !!}
        </div>
    </div>
    <div class="col-md-7 col-md-offset-5 tab-contents">
        <div class="panel text-center margin-t-5">
            <h1>Values</h1>
            <img src="{{ asset('public/images/values.png') }}" height="200">
        </div>
    </div>
</div>