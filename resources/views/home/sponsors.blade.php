<div class="row">

    @inject('insFetcher','App\Services\InsuranceFetcher')

    <?php $insurances = $insFetcher->getAll(); ?>

    <h1 class="panel-title">Insurance Partners</h1>

    @inject('optionFetcher','App\Services\OptionFetcher')
    {!! $optionFetcher->getBySlug('insurance-partners') !!}

    <ul class="float-list sponsor-list">

        {{--<li><img src="{{ asset('public/images/placeholders/sponsors/ADNIC-01 Grey.jpg') }}"> </li>--}}
        {{--<li><img src="{{ asset('public/images/placeholders/sponsors/Almadallah_logo-01Grey.jpg') }}"> </li>--}}
        {{--<li><img src="{{ asset('public/images/placeholders/sponsors/DAMAN logo-01 Grey.jpg') }}"> </li>--}}
        {{--<li><img src="{{ asset('public/images/placeholders/sponsors/NAS Grey.png') }}"> </li>--}}
        {{--<li><img src="{{ asset('public/images/placeholders/sponsors/Puba Grey.png') }}"> </li>--}}
        {{--<li><img src="{{ asset('public/images/placeholders/sponsors/saicoHealth Grey.jpg') }}"> </li>--}}
        {{--<li><img src="{{ asset('public/images/placeholders/sponsors/thiqalogo_Grey.jpg') }}"> </li>--}}
        {{--<li><img src="{{ asset('public/images/placeholders/sponsors/Wapmed-Logo-Grey.jpg') }}"> </li>--}}

        @foreach($insurances as $insurance)
            <li><img src="{{ $insurance->thumbnail }}"> </li>
        @endforeach

    </ul>
</div>