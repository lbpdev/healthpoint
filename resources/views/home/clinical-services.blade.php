<h1 class="panel-title">Clinical Services <br>& Departments</h1>
<div class="col-md-6 col-sm-6 col-xs-6 padding-0">
    @inject('deptFetcher','App\Services\DepartmentFetcher')

    <?php $departments = $deptFetcher->getAllArray(); ?>

    <ul class="">
        <?php $rowCount = 0; ?>
        @if(count($departments))
            @foreach($departments as $department)
                @if($rowCount<12)
                    <li><a href="{{ route('department.single',$department['slug']) }}">{{$department['name']}}</a></li>
                @endif
                <?php $rowCount++; ?>
            @endforeach
        @else
            <li><a href="#">None</a></li>
        @endif
    </ul>
</div>
<div class="col-md-6 col-sm-6 col-xs-6 padding-0">
    <ul class="">
        @if(count($departments)>12)
            <?php $departments = array_slice($departments, 12); ?>
            @foreach($departments as $department)
                <li><a href="{{ route('department.single',$department['slug']) }}">{{$department['name']}}</a></li>
            @endforeach
        @endif
    </ul>
</div>
