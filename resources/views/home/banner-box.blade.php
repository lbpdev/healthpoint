@inject('departmentFetcher','App\Services\DepartmentFetcher')
@inject('optionFetcher','App\Services\OptionFetcher')
<?php
    $departments = $departmentFetcher->getList();
?>

<div class="banner-box" style="background-image: url({{ $optionFetcher->getBySlug('home-banner-image') ? asset('public'.$optionFetcher->getBySlug('home-banner-image')) : asset('public/images/Home-Screen-Image.png') }}) ">
    <div class="col-md-4 col-sm-4 col-xs-6">
        <div class="panel text-center">
            <img src="{{ asset('public/images/icons/doctor-white.png') }}" height="40">
            <h1>Find a Physician</h1>
            {!! Form::open(['route'=>'physician.index','method'=>'get']) !!}
                <input type="text" placeholder="Search for a Physician" name="name" class="form-control icon-input search margin-b-0">
                <hr>
                <div class="form-group">
                    {!! Form::select('specialty',$departments,null,['class'=>'selectpicker form-control']) !!}
                </div>
                <div class="">
                    <select class="selectpicker form-control" name="gender">
                        <option value="any">Gender</option>
                        <option value="M">Male</option>
                        <option value="F">Female</option>
                    </select>
                </div>
                <input type="submit" class="no-bg white" value="Search">
            {!! Form::close() !!}
        </div>
    </div>
    <div class="col-md-4 col-sm-4 col-xs-6">
        <div class="panel text-center">
            <img src="{{ asset('public/images/icons/search-white.png') }}" height="40">
            <h1>What Service</h1>

            {!! Form::open(['route'=>'search.service']) !!}
                <div class="form-group">
                    <input type="text" placeholder="Search for a Service" name="keyword" class="icon-input search form-control">
                </div>
                <p class="text-left">Enter a keyword, disease/condition or department name. </p>
            {!! Form::close() !!}
        </div>
    </div>
</div>