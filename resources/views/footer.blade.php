<script src="{{ asset('public/js/jquery-3.1.0.min.js') }}"></script>
<script src="{{ asset('public/js/bootstrap.min.js') }}"></script>
<script src="{{ asset('public/js/bootstrap-select.min.js') }}"></script>
<script>
    $(document).ready(function(){
        $('.videoWrapper .cover').click(function(){
            var parent = $(this).closest('.videoWrapper');

            var video = $(parent).find('video');

            if(video[0].paused){
                video[0].play();
                $(this).addClass('playing');
            } else {
                video[0].pause();
                $(this).removeClass('playing');
            }

            $(video).unbind('stop pause ended');
            $(video).bind('ended', function(e) {
                console.log('stopped');
                $(this).closest('.videoWrapper').find('.cover').removeClass('playing');
            });
        });
        $('.can-have-lists').find('ul').addClass('standard-list');
        $('.can-have-lists').find('ol li').css('color','#4b3328');
    });
</script>

<script type="text/javascript">var switchTo5x=true;</script>
<script type="text/javascript" src="https://ws.sharethis.com/button/buttons.js"></script>
<script type="text/javascript">stLight.options({publisher: "a3905ba3-c27f-4f64-b6e8-9e0f543ac3dc", doNotHash: false, doNotCopy: false, hashAddressBar: false});</script>

@yield('custom-js')

</div>
<footer class="row" id="site-footer">
    <div class="container">
        <div class="pull-right">
            © 2016 Healthpoint. All Rights Reserved
        </div>
    </div>
</footer>
</body>
</html>

