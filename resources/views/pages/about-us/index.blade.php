@inject('optionFetcher','App\Services\OptionFetcher')

@include('header')
<div class="pull-left" style="width:100%;">
    <div class="row">
        <div class="col-md-9 main-content page">
            @include('templates.mission-vission')
            <div class="tabbed pull-left" style="width:100%;">
                <div class="col-md-12">
                    <div class="row">
                        <ul class="border-line-list col-4 clearfix tabs">
                            <li class="col-md-3 col-xs-12 {{ isset($_GET['tab']) ? '' : 'active' }}"><a href="#">About us</a></li>
                            <li class="col-md-3 col-xs-12 {{ isset($_GET['tab']) ? ( $_GET['tab'] == 'management' ? 'active' : '' ) : '' }}"><a href="#">Executive management</a></li>
                            <li class="col-md-3 col-xs-12 {{ isset($_GET['tab']) ? ( $_GET['tab'] == 'healthcare-network' ? 'active' : '' ) : '' }}"><a href="#">Mubadala Healthcare Network</a></li>
                              <li class="col-md-3 col-xs-12 {{ isset($_GET['tab']) ? ( $_GET['tab'] == 'accreditation' ? 'active' : '' ) : '' }}"><a href="#">Awards and Accreditations</a></li>
                        </ul>
                    </div>
                </div>
                @include('pages.about-us._mubadala')
                @include('pages.about-us._management')
                @include('pages.about-us._portfolio')
                @include('pages.about-us._accreditation')
                @include('pages.about-us._assets')

            </div>
        </div>
        <div class="col-md-3 sidebar">
            @include('templates.sidebar.help')
            @include('templates.sidebar.tweets')
        </div>
    </div>
</div>

@section('custom-js')
    <script src="{{ asset('public/js/html5lightbox.js') }}"></script>
    <script type="text/javascript">
        var html5lightbox_options = {
            watermark: " ",
            watermarklink: " "
        };
    </script>
    <script>

        var animationFlag = false;

        $(document).ready(function(){
            $('.tabs li').on('click',function(e){

                e.preventDefault();
                parent = $(this).closest('.tabbed');

                if(animationFlag==false){
                    animationFlag = true;

                    index = $(this).closest('.tabbed').find('.tabs li').index(this);
                    container = $(this).closest('.tabbed').find('.tab-contents').eq(index);

                    $(this).closest('.tabbed').find('.tabs li').removeClass('active');
                    $(this).addClass('active');

                    $(this).closest('.tabbed').find('.tab-contents').fadeOut(500);

                    $(container).delay(400).fadeIn(500, function(){
                        console.log('show');

                        $(this).closest('.tabbed').find('.tab-contents').removeClass('active');
                        $(container).find('.tab-contents').eq(index).addClass('active');

                        animationFlag = false;
                    });
                }

            });
        });
    </script>
@endsection
@include('footer')
