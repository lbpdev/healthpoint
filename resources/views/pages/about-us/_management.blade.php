@inject('manFetcher','App\Services\ManagementFetcher')

<?php
    $management = $manFetcher->getAll();
?>
<div class="col-md-12 tab-contents {{ isset($_GET['tab']) ? ( $_GET['tab'] == 'management' ? 'active' : '' ) : '' }}">
    <div class="row">
        <h1 class="panel-title">Healthpoint Executive Management Team</h1>
        {!! $optionFetcher->getBySlug('executive-management') !!}

        <div class="col-md-12">
            <div class="row">
                <ul class="user-list floating-list">
                    @if(count($management))
                        @foreach($management as $user)
                            <a href="#" data-toggle="modal" data-target="#profile{{$user->id}}">
                                <li class="col-md-4 col-sm-4 col-xs-6">
                                    <div class="col-md-4">
                                        <div class="row"><img src="{{ $user->thumbnail }}" width="100%"> </div>
                                    </div>
                                    <div class="col-md-8 col-xs-top-20 col-xs-center">
                                        <h1>{{ $user->name }}</h1>
                                        <span style="color:#555">{!! $user->position !!}</span>
                                    </div>
                                </li>
                            </a>
                            <div class="modal fade" tabindex="-1" role="dialog" id="profile{{$user->id}}" aria-labelledby="profile{{$user->id}}">
                                <div class="modal-dialog" role="document">
                                    <div class="modal-content">
                                        <div class="modal-header">
                                            <button type="button" style="background: none" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                                        </div>
                                        <div class="modal-body">
                                            <div class="row">
                                                <div class="col-md-12">
                                                    <div class="col-md-4 col-sm-4"><img src="{{ $user->thumbnail }}" width="100%" class="margin-b-10"></div>
                                                    <div class="col-md-8 col-sm-8">
                                                        {!! $user->description !!}
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div><!-- /.modal-content -->
                                </div><!-- /.modal-dialog -->
                            </div><!-- /.modal -->
                        @endforeach
                    @else
                        <li class="col-md-12 text-center">None</li>
                    @endif
                    {{--<li class="col-md-4 col-sm-4 col-xs-6">--}}
                        {{--<div class="row">--}}
                            {{--<div class="col-md-4">--}}
                                {{--<div class="row"><img src="{{ asset('public/images/placeholders/management/Nader Darwich_Medical Director_360.jpg') }}" width="100%"> </div>--}}
                            {{--</div>--}}
                            {{--<div class="col-md-8 padding-r-0">--}}
                                {{--<h1>Dr. Nader Darwich</h1>--}}
                                {{--<p>Medical Director,</p>--}}
                                {{--<p>Healthpoint</p>--}}
                            {{--</div>--}}
                        {{--</div>--}}
                    {{--</li>--}}
                    {{--<li class="col-md-4 col-sm-4 col-xs-6">--}}
                        {{--<div class="row">--}}
                            {{--<div class="col-md-4">--}}
                                {{--<div class="row"><img src="{{ asset('public/images/placeholders/management/Rohit Gupta_360.jpg') }}" width="100%"> </div>--}}
                            {{--</div>--}}
                            {{--<div class="col-md-8 padding-r-0">--}}
                                {{--<h1>Rohit Gupta</h1>--}}
                                {{--<p>Senior Vice President Finance, </p>--}}
                                {{--<p>Mubadala Development Company</p>--}}
                            {{--</div>--}}
                        {{--</div>--}}
                    {{--</li>--}}
                    {{--<li class="col-md-4 col-sm-4 col-xs-6">--}}
                        {{--<div class="row">--}}
                            {{--<div class="col-md-4">--}}
                                {{--<div class="row"><img src="{{ asset('public/images/placeholders/management/Kolbrun Kristjansdottir_Nursing Director_360.jpg') }}" width="100%"> </div>--}}
                            {{--</div>--}}
                            {{--<div class="col-md-8 padding-r-0">--}}
                                {{--<h1>Kolbrun Kristjansdottir</h1>--}}
                                {{--<p>Director of Nursing,</p>--}}
                                {{--<p>Healthpoint</p>--}}
                            {{--</div>--}}
                        {{--</div>--}}
                    {{--</li>--}}
                    {{--<li class="col-md-4 col-sm-4 col-xs-6">--}}
                        {{--<div class="row">--}}
                            {{--<div class="col-md-4">--}}
                                {{--<div class="row"><img src="{{ asset('public/images/placeholders/management/SarahStaal.jpg') }}" width="100%"> </div>--}}
                            {{--</div>--}}
                            {{--<div class="col-md-8 padding-r-0">--}}
                                {{--<h1>Sarah Staal</h1>--}}
                                {{--<p>Chief Administrative Officer, </p>--}}
                                {{--<p>Healthpoint</p>--}}
                            {{--</div>--}}
                        {{--</div>--}}
                    {{--</li>--}}
                </ul>
            </div>
        </div>
    </div>
</div>