
<div class="col-md-12 tab-contents {{ !isset($_GET['tab']) ?  'active' : '' }}">
    <div class="row">
        <h1 class="panel-title">About Us</h1>
        <div class="col-md-12 margin-b-10">
            <div class="row">
                {!! $optionFetcher->getBySlug('about-us') !!}
            </div>
        </div>
        <h1 class="panel-title">Mubadala Development Company</h1>
        <div class="col-md-5">
            <div class="row">
                <div class="videoWrapper">
                    <video width="100%" poster="{{ asset('public/videos/muba.png') }}">
                        <source src="{{ asset('public/videos/who-we-are.mp4') }}" type="video/mp4">
                        Your browser does not support the video tag.
                    </video>
                    <span class="bt"></span>
                    <a href="{{ asset('public/videos/who-we-are.mp4') }}" class="html5lightbox cover" data-width="780" data-height="480" title="Wo we are">&nbsp;</a>
                </div>
            </div>
        </div>
        <div class="col-md-7 padding-t-10 col-xs-left-0 col-xs-right-0">
            {!! $optionFetcher->getBySlug('mubadala-development-company') !!}
        </div>
    </div>
</div>