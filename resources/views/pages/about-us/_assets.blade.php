@inject('artFetcher','App\Services\ArticleFetcher')

<?php $articles = $artFetcher->getByCategory(1,6); ?>

<div class="col-md-12 tab-contents {{ isset($_GET['tab']) ? ( $_GET['tab'] == 'assets' ? 'active' : '' ) : '' }}" >
    <div class="row">
        <h1 class="panel-title">Mubadala Healthcare Network</h1>
        <div class="col-md-12">
            <div class="row">
                <ul class="box-list">
                    @forelse($articles as $index=>$article)
                        @if($index%2==0)
                            <div class="clearfix">
                                @endif
                                <li class="col-md-6 col-sm-6">
                                    <img src="{{ $article->thumbnail }}" width="100%">
                                    <h1 class="margin-t-8">{{ $article->title }}</h1>
                                    <p>{!! strip_tags($article->content) !!}</p>
                                </li>
                        @if($index%2==1)
                            </div>
                        @endif
                    @empty
                        <li class="col-md-12 no-border">None</li>
                    @endforelse
                    {{--<li class="col-md-6  col-sm-6">--}}
                        {{--<img src="{{ asset('public/images/placeholders/rectangle-x-sm.jpg') }}" width="100%">--}}
                        {{--<h1>Abu Dhabi Telemedicine Center</h1>--}}
                        {{--<p>A joint venture between Mubadala and Switzerland’s leading telemedicine provider, Medgate, to offer high quality, convenient and confidential medical consultations, over the phone.</p>--}}
                    {{--</li>--}}
                    {{--<li class="col-md-6 col-sm-6">--}}
                        {{--<img src="{{ asset('public/images/placeholders/rectangle-x-sm.jpg') }}" width="100%">--}}
                        {{--<h1>Capital Health Screening Centre</h1>--}}
                        {{--<p>A state-of-the-art medical facility that provides Health Authority – Abu Dhabi certified medical screenings for residency visas, occupational health, pensions and benefits. </p>--}}
                    {{--</li>--}}
                    {{--<li class="col-md-6 col-sm-6">--}}
                        {{--<img src="{{ asset('public/images/placeholders/rectangle-x-sm.jpg') }}" width="100%">--}}
                        {{--<h1>National Reference Laboratory</h1>--}}
                        {{--<p>Offering more than 4,000 sophisticated and specialized medical diagnostic tests to the UAE population.</p>--}}
                    {{--</li>--}}
                    {{--<li class="col-md-6 col-sm-6">--}}
                        {{--<img src="{{ asset('public/images/placeholders/rectangle-x-sm.jpg') }}" width="100%">--}}
                        {{--<h1>Tawam Molecular Imaging Centre</h1>--}}
                        {{--<p>A world-class specialist molecular imaging facility in Al Ain, clinically operated by Johns Hopkins Medicine International.</p>--}}
                    {{--</li>--}}
                </ul>
            </div>
        </div>
    </div>
</div>