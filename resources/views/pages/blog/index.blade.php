@include('header')

@inject('artFetcher','App\Services\ArticleFetcher')

<?php

    $limit = !isset($_GET['page']) || $_GET['page']==1 ? 10 : 9;

    $articles = $artFetcher->getByCategory(4,$limit);
    $noFeatured = false;
?>

@section('custom-css')
    <style>
        .stButton {
            position: absolute;
            left:0;
        }
    </style>
@endsection

<div class="pull-left">
    <div class="row">
        <div class="page col-md-9 main-content">
            <h1 class="panel-title">Health Tips</h1>
            <div class="col-md-12">
                <div class="row">

                    @if(count($articles))

                        @if(!isset($_GET['page']) || $_GET['page']==1)
                            <div class="row padding-t-0 padding-b-10 featured-post">
                                <div class="col-md-7">
                                    <a href="{{ route('blog.single',$articles[0]->slug) }}"><img src="{{ $articles[0]->thumbnail }}" width="100%"></a>
                                </div>
                                <div class="col-md-5">
                                    <a href="{{ route('blog.single',$articles[0]->slug) }}"><h1>{{ $articles[0]->title }}</h1></a>
                                    <p>{!! \Illuminate\Support\Str::limit(strip_tags($articles[0]->content),340) !!}</p>

                                    <span class="date">{{ $articles[0]->created_at->format('M d Y') }}</span>
                                </div>
                            </div>
                        @else
                            <?php $noFeatured = true; ?>
                        @endif

                        <div class="row">

                            <ul class="float-list article cols-3">
                                @foreach($articles as $index=>$article)

                                    @if(($index-1)%3==0)
                                        <div class="clearfix">
                                    @endif

                                        @if($index==0 && $noFeatured==false)
                                        @else
                                            <li class="col-md-4 col-sm-6 col-xs-12">
                                                <a href="{{ route('blog.single',$article->slug) }}">
                                                    <img src="{{ $article->thumbnail }}" width="100%">
                                                    <h1 class="margin-t-8">{{ $article->title }}</h1>
                                                </a>
                                                {!! \Illuminate\Support\Str::limit(strip_tags($article->content),100) !!}
                                            </li>
                                        @endif

                                    @if(($index-1)%3==2)
                                        </div>
                                    @endif

                                @endforeach
                                {{--<li class="col-md-4 col-sm-6 col-xs-6">--}}
                                    {{--<a href="{{ url('health-tips/article') }}">--}}
                                        {{--<img src="{{ asset('public/images/placeholders/rectangle-x-sm.jpg') }}" width="100%">--}}
                                        {{--<h1 class="margin-t-8">Capital Health Screening Centre</h1>--}}
                                    {{--</a>--}}
                                    {{--<p>A state-of-the-art medical facility that provides Health Authority – Abu Dhabi certified medical screenings for residency visas, occupational health, pensions and benefits. </p>--}}
                                {{--</li>--}}

                            </ul>


                        </div>

                        <div class="pagination-wrap text-center">
                            {{ $articles->links() }}
                        </div>

                    @else
                        None
                    @endif



                </div>
            </div>
        </div>

        <div class="col-md-3 sidebar">
            @include('templates.sidebar.help')
            @include('templates.sidebar.most-popular')
            @include('templates.sidebar.tweets')
        </div>
    </div>
</div>

@section('custom-js')
    <script>
    </script>
@endsection

@include('footer')


