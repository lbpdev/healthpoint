@include('header')
<div class="pull-left">
    <div class="row">
        <div class="col-md-9 main-content page">
            <h1 class="panel-title">Request an Appointment</h1>
            <p class="alert-success alert pull-left width-full">Your appointment request has been sent successfully. We will be in touch shortly.</p>
            <a href="{{ route('appointment.index') }}">Back</a>
        </div>
        <div class="col-md-3 col-sm-3 col-xs-12 clearfix sidebar margin-t-45">
            @include('templates.sidebar.help')
            @include('templates.sidebar.tweets')
        </div>
    </div>
</div>

@section('custom-js')
    <!-- Google Code for Healthpoint Conversion Code Conversion Page -->
    <script type="text/javascript">

        /* <![CDATA[ */

        var google_conversion_id = 861037558;

        var google_conversion_language = "en";

        var google_conversion_format = "3";

        var google_conversion_color = "ffffff";

        var google_conversion_label = "oGWACIefnW4Q9sfJmgM";

        var google_remarketing_only = false;

        /* ]]> */

    </script>

    <script type="text/javascript" src="//www.googleadservices.com/pagead/conversion.js">

    </script>

    <noscript>

        <div style="display:inline;">

            <img height="1" width="1" style="border-style:none;" alt="" src="//www.googleadservices.com/pagead/conversion/861037558/?label=oGWACIefnW4Q9sfJmgM&amp;guid=ON&amp;script=0"/>

        </div>

    </noscript>
@endsection

@include('footer')


