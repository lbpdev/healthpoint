<div class="col-md-12">
    <div class="row">
        <ul class="border-line-list short clearfix">
            <li class="col-md-4 col-xs-4 {{ str_contains(Request::path(), 'our-services') ? 'active' : '' }}"><a href="{{ url('patients/our-services') }}">Our Services</a></li>
            <li class="col-md-4 col-xs-4 {{ str_contains(Request::path(), 'insurance') ? 'active' : '' }}"><a href="{{ url('patients/insurance') }}">Insurance</a></li>
            <li class="col-md-4 col-xs-4 {{ str_contains(Request::path(), 'at-healthpoint') ? 'active' : '' }}"><a href="{{ url('patients/at-healthpoint') }}">Innovations at Healthpoint</a></li>
        </ul>
    </div>
</div>