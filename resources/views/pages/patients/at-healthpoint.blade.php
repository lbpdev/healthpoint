@include('header')
<div class="pull-left">
    <div class="row">
        <div class="page col-md-9 main-content">
            <h1 class="panel-title">Patient & Visitors</h1>
            @inject('optionFetcher','App\Services\OptionFetcher')

            @if($optionFetcher->getBySlug('patients-banner-image'))
                <div class="banner-box patient" style="background-image: url({{ asset('public'.$optionFetcher->getBySlug('patients-banner-image')) }})"></div>
            @else
                <div class="banner-box patient"></div>
            @endif

            <div class="col-md-12 pull-left">
                <div class="row">

                    @include('pages.patients.nav')

                    <h1 class="panel-title">Innovations at Healthpoint</h1>

                    <div class="row pull-left">

                        @inject('artFetcher','App\Services\ArticleFetcher')

                        <?php $articles = $artFetcher->getByCategory(3,12); ?>

                        <ul class="float-list article innovations cols-3">

                            @forelse($articles as $index=>$article)
                                @if($index%3==0)
                                    <div class="clearfix">
                                @endif
                                        <li class="col-md-4 col-sm-6">
                                            <img src="{{ $article->thumbnail }}" width="100%">
                                            <h1 class="margin-t-8">{{ $article->title }}</h1>
                                            <p>{!! strip_tags($article->content) !!}</p>
                                        </li>
                                @if($index%3==2)
                                    </div>
                                @endif
                            @empty
                                <li class="col-md-12 no-border">None</li>
                            @endforelse
                            {{----}}
                            {{----}}
                            {{--<li class="col-md-4 col-sm-6">--}}
                                {{--<a href="#">--}}
                                    {{--<img src="{{ asset('public/images/placeholders/rectangle-x-sm.jpg') }}" width="100%">--}}
                                    {{--<h1 class="margin-t-8">Capital Health Screening Centre</h1>--}}
                                {{--</a>--}}
                                {{--<p>A state-of-the-art medical facility that provides Health Authority – Abu Dhabi certified medical screenings for residency visas, occupational health, pensions and benefits. </p>--}}
                            {{--</li>--}}
                            {{--<li class="col-md-4 col-sm-6">--}}
                                {{--<a href="#">--}}
                                    {{--<img src="{{ asset('public/images/placeholders/rectangle-x-sm.jpg') }}" width="100%">--}}
                                    {{--<h1 class="margin-t-8">National Reference Laboratory</h1>--}}
                                {{--</a>--}}
                                {{--<p>Offering more than 4,000 sophisticated and specialized medical diagnostic tests to the UAE population.</p>--}}
                            {{--</li>--}}
                            {{--<li class="col-md-4 col-sm-6">--}}
                                {{--<a href="#">--}}
                                    {{--<img src="{{ asset('public/images/placeholders/rectangle-x-sm.jpg') }}" width="100%">--}}
                                    {{--<h1 class="margin-t-8">Abu Dhabi Telemedicine Center</h1>--}}
                                {{--</a>--}}
                                {{--<p>A joint venture between Mubadala and Switzerland’s leading telemedicine provider, Medgate, to offer high quality, convenient and confidential medical consultations, over the phone.</p>--}}
                            {{--</li>--}}
                            {{--<li class="col-md-4 col-sm-6">--}}
                                {{--<a href="#">--}}
                                    {{--<img src="{{ asset('public/images/placeholders/rectangle-x-sm.jpg') }}" width="100%">--}}
                                    {{--<h1 class="margin-t-8">Capital Health Screening Centre</h1>--}}
                                {{--</a>--}}
                                {{--<p>A state-of-the-art medical facility that provides Health Authority – Abu Dhabi certified medical screenings for residency visas, occupational health, pensions and benefits. </p>--}}
                            {{--</li>--}}
                            {{--<li class="col-md-4 col-sm-6">--}}
                                {{--<a href="#">--}}
                                    {{--<img src="{{ asset('public/images/placeholders/rectangle-x-sm.jpg') }}" width="100%">--}}
                                    {{--<h1 class="margin-t-8">National Reference Laboratory</h1>--}}
                                {{--</a>--}}
                                {{--<p>Offering more than 4,000 sophisticated and specialized medical diagnostic tests to the UAE population.</p>--}}
                            {{--</li>--}}
                        </ul>
                    </div>
                </div>
            </div>
        </div>

        <div class="col-md-3 sidebar">
            @include('templates.sidebar.help')
            @include('templates.sidebar.tweets')
        </div>
    </div>
</div>

@section('custom-js')
    <script>
        $('.panel-body').find('ul').addClass('standard-list');
        $('.panel-body').find('ol li').css('color','#4b3328');
    </script>
@endsection
@include('footer')


