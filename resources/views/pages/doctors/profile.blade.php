@include('header')
<div class="pull-left">
    <div class="row">
        <div class="col-md-9 padding-r-0">
            <h1 class="panel-title">Find a Physician</h1>
            <div class="col-md-3 sidebar">
                <div class="row">
                    @include('pages.doctors.sidebar')
                </div>
            </div>
            <div class="col-md-9 main-content page padding-l-0">

                @if($physician)
                <div class="col-md-12 profile">
                    <div class="row">
                        <div class="col-md-11">
                            <div class="row">
                                <div class="col-md-3 col-sm-3 col-xs-12">
                                    <div class="row padding-t-0">
                                        <img src="{{ $physician->thumbnailFull }}" width="100%">
                                    </div>
                                </div>
                                <div class="col-md-9 col-sm-9 col-xs-12 col-xs-left-0 col-xs-right-0 col-xs-top-10">
                                    <h1>{{ $physician->name }}</h1>
                                    {!! $physician->position !!}
                                    @if($physician->specialty)
                                        <p>{{ $physician->specialty }}</p>
                                    @endif
                                    {{ $physician->contactNumber ? '<p class="icon-text phone margin-t-5">'.$physician->contactNumber.'</p>' : '' }}
                                </div>
                            </div>
                        </div>
                        <div class="col-md-1 text-right">
                            <a class="icon-text padding-l-0 standard-bold" href="{{ url('find-a-doctor') }}">Back</a>
                        </div>
                    </div>
                </div>

                <div class="col-md-12">
                    <div class="row padding-t-0">

                        <hr>

                        <h1>Professional Summary</h1>
                        {!! $physician->description ? $physician->description : 'N/A' !!}

                        <hr>

                        @foreach($metaTypes as $type)
                            @if($type->slug != 'contact-number')
                                @if(isset($physician))
                                    @if(isset($physician['meta'][$type->slug][0]))
                                        @if($physician['meta'][$type->slug][0]!="")
                                            <h1>{{$type->name}}</h1>
                                            {!! $physician['meta'][$type->slug][0]['value'] !!}
                                            <hr>
                                        @endif
                                    @endif
                                @endif
                            @endif
                        @endforeach
                    </div>
                </div>
            @else
                <div class="row">
                    <a class="icon-text padding-l-0 standard-bold" href="{{ url('find-a-doctor') }}">Back</a>
                    <h1 class="alert-danger alert">Unknown User</h1>
                </div>
            @endif
            </div>
        </div>
        <div class="col-md-3 sidebar margin-t-45">
            @include('templates.sidebar.help')
            @include('templates.sidebar.tweets')
        </div>
    </div>
</div>

@section('custom-js')
    <script>
        $('.main-content').find('ul').addClass('standard-list');
        $('.main-content').find('ol li').css('color','#4b3328');
    </script>
@endsection

@include('footer')


