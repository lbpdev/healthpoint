@include('header')
<div class="pull-left">
    <div class="row">
        <div class="col-md-9 padding-r-0">
            <h1 class="panel-title">Find a Physician</h1>
            <div class="col-md-3 sidebar">
                <div class="row">
                    @include('pages.doctors.sidebar')
                </div>
            </div>
            <div class="col-md-9 main-content page col-xs-left-0">
                <div class="col-md-12">
                    <div class="row">
                        <ul class="doctor-list">
                            <?php $counter = 1; ?>
                            @forelse($physicians as $physician)
                                {!! $counter%2 ? '<div class="clearfix">' : ''  !!}
                                    <li class="col-md-6 col-sm-6  col-xs-6 {{ $counter%1 ? 'padding-r-0' : 'col-xs-right-0'  }}">
                                        <div class="row">
                                            <div class="col-md-3 col-sm-3 col-xs-12">
                                                <div class="row">
                                                    <a href="{{ route('physician.single',$physician->slug) }}"><img src="{{ $physician->thumbnail }}" width="100%"></a>
                                                </div>
                                            </div>
                                            <div class="col-md-8 col-sm-8 col-xs-12 padding-r-0">
                                                <h1><a href="{{ route('physician.single',$physician->slug) }}">{{ $physician->name }}</a></h1>
                                                <p class="padding-b-10 specialty">{!! strip_tags($physician->specialty) !!}</p>
                                                <p class="padding-b-10 position">{!! strip_tags($physician->position) !!}</p>
                                                <p class="padding-b-10 description">{!! strip_tags($physician->description) !!}</p>
                                            </div>
                                        </div>
                                        <div class="collapse"></div>
                                    </li>
                                    {{--<li class="col-md-6 col-sm-6 col-xs-6">--}}
                                        {{--<div class="row">--}}
                                            {{--<div class="col-md-3 col-sm-3 col-xs-12">--}}
                                                {{--<div class="row">--}}
                                                    {{--<a href="{{ url('/find-a-doctor/doctor') }}"><img src="{{ asset('public/images/placeholders/rectangle-y-sm.jpg') }}" width="100%"></a>--}}
                                                {{--</div>--}}
                                            {{--</div>--}}
                                            {{--<div class="col-md-9 col-sm-9 col-xs-12">--}}
                                                {{--<h1><a href="{{ url('/find-a-doctor/doctor') }}">Suhail Mahmood Al Ansari</a></h1>--}}
                                                {{--<p>Executive Director,</p>--}}
                                                {{--<p>Mubadala Healthcare;</p>--}}
                                                {{--<p>Chairman, Healthpoint</p>--}}
                                            {{--</div>--}}
                                        {{--</div>--}}
                                    {{--</li>--}}
                                {!! $counter%2 ? '' : '</div>'  !!}
                                <?php $counter++; ?>
                            @empty
                                <p class="alert alert-danger">Search has no results</p>
                            @endforelse
                        </ul>

                        <div class="pagination-wrap text-center">
                            @include('pagination.default', ['paginator' => $physicians])

                            {{--{{ $physicians->links() }}--}}
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="col-md-3 sidebar margin-t-45">
            @include('templates.sidebar.help')
            @include('templates.sidebar.tweets')
        </div>
    </div>
</div>

@section('custom-js')
    <script>
        $('.collapse').on('click',function(){

            parent = $(this).parent();

            if($(parent).hasClass('collapsed'))
                $(parent).removeClass('collapsed');
            else{
                $('.doctor-list li.collapsed').removeClass('collapsed');
                $(parent).addClass('collapsed');
            }

        });
    </script>
@endsection

@include('footer')


