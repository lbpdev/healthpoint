@include('header')
<div class="pull-left">
    <div class="row">
        <div class="col-md-9 main-content page">
            <h1 class="panel-title">Request an Appointment</h1>
            <p>Complete the form below to request an appointment at Healthpoint. An appointment representative will contact you to book
                your appointment. </p>

            <p>If you have a medical emergency, call 998.</p>

            <p class="brown-text">All fields are required unless marked optional.</p>

            {!! Form::open(['class'=>'appointment','url'=>'request-appointment']) !!}
            <hr>

                <h1>Requester Information</h1>

                <div class="row">
                    <div class="col-md-3 col-sm-3 col-xs-12 clearfix">
                        <div class="padding-t-0 ">
                            <p class="text-left">Who is this appointment for?</p>
                        </div>
                    </div>
                    <div class="col-md-9">
                        <div class="">
                            <input type="radio" id="r1" checked name="Who is this appointment for?[Self]"/>
                            <label for="r1">Self</label>
                            <br>
                            <input type="radio" id="r2" name="Who is this appointment for?[Other]"/>
                            <label for="r2">Other</label>
                        </div>
                    </div>
                </div>
                <hr>


                <h1>Patient Information</h1>
                <p>Please provide patient information as it appears on legal documents.</p>

                <div class="row">
                    <div class="col-md-3 col-sm-3 col-xs-12 clearfix">
                        <div class="padding-t-0">
                            <p class="">Have you previously received care at Healthpoint?</p>
                        </div>
                    </div>
                    <div class="col-md-9">
                        <div class="">
                            <input type="radio" id="r3" checked name="prevYes"/>
                            <label for="r3">Yes</label>
                            <br>
                            <input type="radio" id="r4" name="prevNo"/>
                            <label for="r4">No</label>
                            <br>
                            <input type="radio" id="r5" name="Don't Know"/>
                            <label for="r5">Don't know</label>
                        </div>
                    </div>
                </div>

                <div class="row">
                    <div class="col-md-3 col-sm-3 col-xs-12 clearfix">
                        <div class="padding-t-0 ">
                            <p class=" label">First Name:</p>
                        </div>
                    </div>
                    <div class="col-md-9">
                        <div class="">
                            <input type="text" required value="GEne" name="First Name"/>
                        </div>
                    </div>
                </div>

                <div class="row">
                    <div class="col-md-3 col-sm-3 col-xs-12 clearfix">
                        <div class="padding-t-0 ">
                            <p class=" label">Surname:</p>
                        </div>
                    </div>
                    <div class="col-md-9">
                        <div class="">
                            <input type="text" required value="Ellorin"  name="Surname"/>
                        </div>
                    </div>
                </div>

                <div class="row">
                    <div class="col-md-3 col-sm-3 col-xs-12 clearfix">
                        <div class="padding-t-0 ">
                            <p class=" label">Address:</p>
                        </div>
                    </div>
                    <div class="col-md-9">
                        <div class="">
                            <input type="text" required  value="Al rigga" name="Address"/>
                        </div>
                    </div>
                </div>

                <div class="row">
                    <div class="col-md-3 col-sm-3 col-xs-12 clearfix">
                        <div class="padding-t-0 ">
                            <p class=" label">City:</p>
                        </div>
                    </div>
                    <div class="col-md-9">
                        <div class="">
                            <input type="text" required value="Dubai City"  name="City"/>
                        </div>
                    </div>
                </div>

                <div class="row">
                    <div class="col-md-3 col-sm-3 col-xs-12 clearfix">
                        <div class="padding-t-0 ">
                            <p class=" label">Emirate:</p>
                        </div>
                    </div>
                    <div class="col-md-9">
                        <div class="">
                            <input type="text" required value="Dubai"  name="Emirate"/>
                        </div>
                    </div>
                </div>

                <div class="row">
                    <div class="col-md-3 col-sm-3 col-xs-12 clearfix">
                        <div class="padding-t-0 ">
                            <p class=" label">P.O Box:</p>
                        </div>
                    </div>
                    <div class="col-md-9">
                        <div class="">
                            <input type="text"  value="POBOX 123123123123" name="P.O Box"/>
                        </div>
                    </div>
                </div>

                <div class="row">
                    <div class="col-md-3 col-sm-3 col-xs-12 clearfix">
                        <div class="padding-t-0 ">
                            <p class=" label">Primary phone:</p>
                        </div>
                    </div>
                    <div class="col-md-9">
                        <div class="">
                            <input type="text" required value="0560404339"  name="Primary phone" class="margin-b-10"/>
                            <br>
                            <input type="radio" id="r3" checked name="home"/>
                            <label for="r3">Home</label>
                            <br>
                            <input type="radio" id="r4" name="mobile"/>
                            <label for="r4">Mobile</label>
                            <br>
                            <input type="radio" id="r5" name="office"/>
                            <label for="r5">Office</label>
                        </div>
                    </div>
                </div>

                <div class="row">
                    <div class="col-md-3 col-sm-3 col-xs-12 clearfix">
                        <div class="padding-t-0 ">
                            <p class=" label">Secondary phone:</p>
                        </div>
                    </div>
                    <div class="col-md-9">
                        <div class="">
                            <input type="text" name="Secondary phone" value="23123123123123"  class="margin-b-10"/>
                            <br>
                            <input type="radio" id="r3" checked name="home2"/>
                            <label for="r3">Home</label>
                            <br>
                            <input type="radio" id="r4" name="mobile2"/>
                            <label for="r4">Mobile</label>
                            <br>
                            <input type="radio" id="r5" name="office2"/>
                            <label for="r5">Office</label>
                        </div>
                    </div>
                </div>


                <div class="row">
                    <div class="col-md-3 col-sm-3 col-xs-12 clearfix">
                        <div class="padding-t-0 ">
                            <p class=" label">Email:</p>
                        </div>
                    </div>
                    <div class="col-md-9">
                        <div class="">
                            <input type="text" required value="elloringene@gmail.com"  name="Email"/>
                        </div>
                    </div>
                </div>

                <div class="row">
                    <div class="col-md-3 col-sm-3 col-xs-12 clearfix">
                        <div class="padding-t-0 ">
                            <p class=" label">Gender:</p>
                        </div>
                    </div>
                    <div class="col-md-9">
                        <div class="">
                            <input type="radio" id="r3" name="Male"/>
                            <label for="r3">Male</label>
                            <br>
                            <input type="radio" id="r4" checked name="Female"/>
                            <label for="r4">Female</label>
                        </div>
                    </div>
                </div>

                <div class="row">
                    <div class="col-md-3 col-sm-3 col-xs-12 clearfix">
                        <div class="padding-t-0 ">
                            <p class=" label">Date of Birth:</p>
                        </div>
                    </div>
                    <div class="col-md-9">
                        <div class="date-input">
                            <select class="selectpicker pull-left" name="month">
                                <option>1</option>
                                <option>2</option>
                                <option>3</option>
                            </select>
                            <select class="selectpicker pull-left" name="day">
                                <option>1</option>
                                <option>2</option>
                                <option>3</option>
                            </select>
                            <select class="selectpicker pull-left" name="year">
                                <option>1991</option>
                            </select>
                        </div>
                    </div>
                </div>

                <div class="row">
                    <div class="col-md-3 col-sm-3 col-xs-12 clearfix">
                        <div class="padding-t-0 ">
                            <p class=" label">Parent name/Guardian name:</p>
                        </div>
                    </div>
                    <div class="col-md-9">
                        <div class="">
                            <input type="text" required value="Nico"  name="guardian"/>
                            <p class="font-8">The name of a parent is required if <br>the patient is under the age of 16.</p>
                        </div>
                    </div>
                </div>


                <div class="row">
                    <div class="col-md-3 col-sm-3 col-xs-12 clearfix">
                        <div class="padding-t-0 ">
                            <p class=" label">Does the patient need <br>an interpreter?</p>
                        </div>
                    </div>
                    <div class="col-md-9">
                        <div class=" ">
                            <input type="radio" id="r3" checked name="Does the patient need an interpreter?[No]"/>
                            <label for="r3">No</label>
                            <br>
                            <input type="radio" id="r3" name="Does the patient need an interpreter?[Male]"/>
                            <label for="r3">Male</label>
                            <br>
                            <input type="radio" id="r4" name="Does the patient need an interpreter?[Female]"/>
                            <label for="r4">Female</label>
                        </div>
                    </div>
                </div>

            <hr>


            <h1>Patient Insurance Information</h1>

            <div class="row">
                <div class="col-md-3 col-sm-3 col-xs-12 clearfix">
                    <div class="padding-t-0 ">
                        <p class="">Does the patient have
                            health insurance?</p>
                    </div>
                </div>
                <div class="col-md-9">
                    <div class="">
                        <input type="radio" id="r1" checked name="Does the patient have health insurance?[Yes]"/>
                        <label for="r1">Yes</label>
                        <br>
                        <input type="radio" id="r2" name="Does the patient have health insurance?[No]"/>
                        <label for="r2">No</label>
                    </div>
                </div>
            </div>

            <hr>

            <h1>Medical Concern</h1>

            <div class="row">
                <div class="col-md-3 col-sm-3 col-xs-12 clearfix">
                    <div class="padding-t-0 ">
                        <p class="">What is the primary medical
                            problem or diagnosis for the
                            appointment request?</p>
                    </div>
                </div>
                <div class="col-md-9">
                    <div class="">
                        <textarea id="r1" rows="5" required name="problem">My Problem</textarea>
                    </div>
                </div>
            </div>

            <div class="row">
                <div class="col-md-3 col-sm-3 col-xs-12 clearfix">
                    <div class="padding-t-0 ">
                        <p class="">How long has the patient had this problem?</p>
                    </div>
                </div>
                <div class="col-md-9">
                    <div class="date-input">
                        <select class="selectpicker" name="longMonth">
                            <option>1</option>
                            <option>2</option>
                            <option>3</option>
                        </select>
                        <select class="selectpicker" name="longYear">
                            <option>1991</option>
                        </select>
                    </div>
                </div>
            </div>

            <div class="row">
                <div class="col-md-3 col-sm-3  col-xs-12 clearfix">
                    <div class="padding-t-0 ">
                        <p class="">Are there additional medical
                            problems the patient needs
                            assessed during this visit?</p>
                    </div>
                </div>
                <div class="col-md-9">
                    <div class="">
                        <textarea id="r1" rows="5" required  name="addProblems"></textarea>
                        <p class="font-8">You have 300 characters remaining out of 300 total. (optional)</p>
                    </div>
                </div>
            </div>

            <hr>
            <p class="brown-text standard-bold font-13">Important: After submission, please do not leave this form until you see the confirmation message.</p>

            <div class="row">
                <div class="col-md-9 col-md-offset-3">
                    <div class="padding-t-10">
                        <input type="submit" value="Send Request">
                    </div>
                </div>
            </div>

            </form>
        </div>
        <div class="col-md-3 col-sm-3 col-xs-12 clearfix sidebar margin-t-45">
            @include('templates.sidebar.help')
            @include('templates.sidebar.tweets')
        </div>
    </div>
</div>

@section('custom-js')
    <script>
    </script>
@endsection

@include('footer')


