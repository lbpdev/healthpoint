@section('custom-styles-alt')
    <style type="text/css">
        .main-content p {
            min-height: 1px;
        }
    </style>
@endsection

@include('header')

<div class="pull-left">
    <div class="row">
        <div class="col-md-9">
            <h1 class="panel-title">Clinical Services & Departments</h1>
            <p>As an integrated, multi-disciplinary hospital, Healthpoint’s teams of physicians work together across our various departments to provide complete care to all patients.</p>
            <div class="col-md-3 sidebar col-xs-right-0">
                <div class="row">
                    @include('pages.departments.sidebar')
                </div>
            </div>
            <div class="col-md-9 main-content page col-xs-left-0 col-xs-right-0">
                <div class="col-md-12">
                    <div class="row">
                        @if($department)
                            <h1 class="panel-title">{{ $department->name }}</h1>
                            {!! $department->description !!}
                        @else
                            <h1 class="panel-title">Department Not Found</h1>
                            <p>Please select a department from the list on the left.</p>
                        @endif
                    </div>
                </div>
            </div>
        </div>
        <div class="col-md-3 sidebar margin-t-45">
            @include('templates.sidebar.help')
            @include('templates.sidebar.doctors')
        </div>

        <div class="col-md-12">
            @include('templates.partners')
        </div>
    </div>
</div>

@section('custom-js')
    <script>
        $('.main-content').find('ul').addClass('standard-list');
        $('.main-content').find('&nbsp').addClass('standard-list');
    </script>
@endsection

@include('footer')


