<div class="col-md-12 page padding-r-0">
    <div class="row yellowish-bg padding-l-0 padding-r-0">
        <div class="col-md-12 padding-r-0">
            <ul class="filters">

                @inject('departmentFetcher','App\Services\DepartmentFetcher')

                <?php $departments = $departmentFetcher->getAll(); ?>

                @foreach($departments as $data)
                    <li>
                        <a href="{{ route('department.single',$data->slug) }}" class="{{ isset($department) ? ( $department->slug == $data->slug ? 'active' : false ) : '' }}">{{ $data->name }}</a>
                    </li>
                @endforeach
                {{--<li>--}}
                    {{--<a href="#">Cardiology</a>--}}
                {{--</li>--}}
            </ul>
        </div>

    </div>
</div>