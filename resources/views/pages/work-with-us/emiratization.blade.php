@include('header')
<div class="pull-left">
    <div class="row pull-left">
        <div class="page col-md-9 main-content">
            <h1 class="panel-title">Patient & Visitors</h1>

            @include('templates.working-for-us')

            <div class="col-md-12 pull-left">
                <div class="row">

                    @include('pages.work-with-us.nav')

                    @inject('optionFetcher','App\Services\OptionFetcher')

                    <h1 class="panel-title">Emiratization</h1>
                    <div class="col-md-5">
                        <div class="row pull-left">

                            <div class="videoWrapper">
                                <video width="100%" controls poster="">
                                    <source src="{{ $optionFetcher->getBySlug('emiratization-video') ? $optionFetcher->getBySlug('emiratization-video') : asset('public/videos/testimonials/Danya AlMaheiri - Employee Testimonial.mp4') }}#t=5" type="video/mp4">
                                    Your browser does not support the video tag.
                                </video>
                                <span class="bt"></span>
                                <a href="{{ $optionFetcher->getBySlug('emiratization-video') ? $optionFetcher->getBySlug('emiratization-video') :  asset('public/videos/testimonials/Danya AlMaheiri - Employee Testimonial.mp4') }}" class="html5lightbox cover" data-width="780" data-height="480" title="Danya AlMaheiri - Employee Testimonial">&nbsp;</a>
                            </div>

                            <br>
                            <br>


                            <div class="videoWrapper">
                                <video width="100%" controls poster="">
                                    <source src="{{ $optionFetcher->getBySlug('emiratization-video-2') ? $optionFetcher->getBySlug('emiratization-video-2') : asset('public/videos/testimonials/Danya AlMaheiri - Employee Testimonial.mp4') }}#t=5" type="video/mp4">
                                    Your browser does not support the video tag.
                                </video>
                                <span class="bt"></span>
                                <a href="{{ $optionFetcher->getBySlug('emiratization-video-2') ? $optionFetcher->getBySlug('emiratization-video-2') : asset('public/videos/testimonials/Ali Al Mousawi - Employee Testimonial.mp4') }}" class="html5lightbox cover" data-width="780" data-height="480" title="Ali Al Mousawi - Employee Testimonial">&nbsp;</a>
                            </div>

                        </div>
                    </div>
                    <div class="col-md-7 padding-t-10 padding-l-20 pull-left col-xs-left-0 col-xs-right-0">
                        {!! str_replace("&nbsp;"," ",$optionFetcher->getBySlug('emiratization')) !!}
                    </div>
                </div>
            </div>
        </div>

        <div class="col-md-3 sidebar">
            @include('templates.sidebar.help')
            @include('templates.sidebar.patient-rights')
        </div>
    </div>
</div>

@section('custom-js')
    <script src="{{ asset('public/js/jquery.magnific-popup.min.js') }}"></script>

    <script>
        $('.album').magnificPopup({
            delegate: 'a', // child items selector, by clicking on it popup will open
            type: 'image'
            // other options
        });
    </script>

    <script src="{{ asset('public/js/html5lightbox.js') }}"></script>
    <script type="text/javascript">
        var html5lightbox_options = {
            watermark: " ",
            watermarklink: " "
        };
    </script>
@endsection
@include('footer')


