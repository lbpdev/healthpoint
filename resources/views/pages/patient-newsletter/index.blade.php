@include('header')

<div class="pull-left">
    <div class="row">
        <div class="page col-md-9 main-content">
            <h1 class="panel-title">Patient Newsletters</h1>

            <div class="col-md-12">
                <div class="row">

                    <div class="row pull-left width-full">
                        <ul class="float-list article cols-3">

                            @forelse($articles as $index=>$article)
                                @if($index%3==0)
                                    <div class="clearfix">
                                        @endif
                                        <li class="col-md-4 col-sm-6 col-xs-12">
                                            <a target="_blank" href="{{ $article->external_link }}">
                                                <img src="{{ $article->thumbnailOriginal }}" width="100%">
                                                <h1 class="margin-t-8">{{ $article->title }}</h1>
                                            </a>
                                            <p>{!! \Illuminate\Support\Str::limit(preg_replace("/&#?[a-z0-9]+;/i","",strip_tags($article->content)),140) !!}</p>
                                        </li>
                                        @if($index%3==2)
                                    </div>
                                @endif
                            @empty
                                <li class="col-md-12">None</li>
                            @endforelse
                            {{--<li class="col-md-4 col-sm-6">--}}
                                {{--<a href="{{ url('media/news/post') }}">--}}
                                    {{--<img src="{{ asset('public/images/placeholders/rectangle-x-sm.jpg') }}" width="100%">--}}
                                    {{--<h1 class="margin-t-8">Abu Dhabi Telemedicine Center</h1>--}}
                                {{--</a>--}}
                                {{--<p>A joint venture between Mubadala and Switzerland’s leading telemedicine provider, Medgate, to offer high quality, convenient and confidential medical consultations, over the phone.</p>--}}
                            {{--</li>--}}
                        </ul>
                    </div>

                    <div class="pagination text-center">
                        {{ $articles->links() }}
                    </div>
                </div>
            </div>
        </div>

        <div class="col-md-3 sidebar">
            @include('templates.sidebar.help')
            @include('templates.sidebar.media-contacts')
            @include('templates.sidebar.media-pack')
            @include('templates.sidebar.tweets')
        </div>
    </div>
</div>

@section('custom-js')
    <script>
    </script>
@endsection

@include('footer')


