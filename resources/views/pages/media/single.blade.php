
@section('metas')

        <!-- Place this data between the <head> tags of your website -->
<meta name="description" content="{!! \Illuminate\Support\Str::limit(strip_tags($article->content), 150) !!}" />

<!-- Schema.org markup for Google+ -->
<meta itemprop="name" content="{{ $article->title }}">
<meta itemprop="description" content="{!! \Illuminate\Support\Str::limit(strip_tags($article->content), 150) !!}">
<meta itemprop="image" content="{{ asset( $article->thumbnail) }}">

<!-- Twitter Card data -->
<meta name="twitter:card" content="summary_large_image">
<meta name="twitter:site" content="www.middleeastdoctor.com">
<meta name="twitter:title" content="{{ $article->title }}">
<meta name="twitter:description" content="{!!\Illuminate\Support\Str::limit(strip_tags($article->content), 150) !!}">
<meta name="twitter:creator" content="Middle East Doctor">
<!-- Twitter summary card with large image must be at least 280x150px -->
<meta name="twitter:image:src" content="{{ asset( $article->thumbnail) }}">

<!-- Open Graph data -->
<meta property="og:title" content="{{ $article->title }}" />
<meta property="og:type" content="article" />
<meta property="og:url" content="{{ Request::fullUrl()}}" />
<meta property="og:image" content="{{ asset( $article->thumbnail) }}" />
<meta property="og:description" content="{!!\Illuminate\Support\Str::limit(strip_tags($article->content), 150) !!}" />
<meta property="og:site_name" content="www..com" />
<meta property="article:published_time" content="{{ $article->created_at }}" />
<meta property="article:modified_time" content="{{ $article->updated_at }}" />
<meta property="article:section" content="Article Section" />
<meta property="article:tag" content="Article Tag" />
<meta property="fb:admins" content="100000370643856" />

<style>
    .stButton .stLarge {
        height: 24px !important;
        width: 21px !important;
        position: relative !important;
        opacity: 0 !important;
    }
    .stButton .stLarge:hover {
        height: 24px !important;
        width: 21px !important;
        opacity: 0 !important;
    }
</style>
@stop

@include('header')

<div class="pull-left">
    <div class="row">
        <div class="page col-md-2">
            <h1 class="panel-title">Share on</h1>
            <ul class="socials pull-left margin-t-0">

                {{--<span class='st_facebook_large' displayText='Facebook'></span>--}}
                {{--<span class='st_twitter_large' displayText='Tweet'></span>--}}
                {{--<span class='st_googleplus_large' displayText='Google +'></span>--}}
                {{--<span class='st_linkedin_large' displayText='LinkedIn'></span>--}}
                {{--<span class='st_email_large' displayText='Email'></span>--}}

                <li><a href="#" class="st_facebook_large social-icon fb"></a></li>
                <li><a href="#" class="st_twitter_large social-icon tw"></a></li>
                <li><a href="#" class="st_linkedin_large social-icon lk"></a></li>
            </ul>
            <p class="brown-text">{{ $article ? $article->created_at->format('M d Y') : '' }}</p>
        </div>
        <div class="page col-md-7 main-content gray-border-left">
            <div class="col-md-12">
                <div class="row">
                    @if($article)
                        <h1 class="panel-title">{{ $article->title }}</h1>

                        <img src="{{ $article->thumbnailLong }}" width="100%" class="margin-b-10">

                        {!! str_replace("&nbsp;"," ",$article->content) !!}

                        @if($article->slug=='healthpoints-manchester-city-football-club-mascot-competition-1')
                            <div class="tintup" data-id="webadmin4" data-columns="" data-expand="true" data-infinitescroll="true" data-personalization-id="835481" style="height:500px;width:100%;"></div>
                            <script async src="https://d36hc0p18k1aoc.cloudfront.net/pages/a5b5e5.js"></script>
                        @endif

                    @else
                        <h1 class="alert-danger alert">Article not found.</h1>
                    @endif
                    <br>
                    <a class="pull-left" href="{{ redirect()->back()->getTargetUrl() }}">Back</a>
                </div>
            </div>
        </div>

        <div class="col-md-3 sidebar">
            @include('templates.sidebar.help')
            @include('templates.sidebar.more-news')
        </div>
    </div>
</div>

@section('custom-js')
    <script>
        $('.main-content').find('ul').addClass('standard-list');
        $('.main-content').find('ol li').css('color','#4b3328');
    </script>
@endsection

@include('footer')


