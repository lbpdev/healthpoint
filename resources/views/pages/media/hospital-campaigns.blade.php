@include('header')
<div class="pull-left">
    <div class="row">
        <div class="page col-md-9 main-content">
            <h1 class="panel-title">Media</h1>

            @include('templates.media-banner')

            <div class="col-md-12">
                <div class="row">

                    @include('pages.media.nav')

                    <h1 class="panel-title col-xs-top-20">Hospital Campaigns</h1>

                    <div class="row pull-left" style="width: 100%">
                        <ul class="float-list article cols-3">
                            @forelse($articles as $index=>$article)

                                @if($index%3==0)
                                    <div class="clearfix">
                                @endif
                                    <li class="col-md-4 col-sm-6 col-xs-12">
                                        @if($article->external_link)
                                            <a href="{{ $article->external_link }}">
                                        @else
                                            <a href="{{ route('media.news.single',$article->slug) }}">
                                        @endif
                                            <img src="{{ $article->thumbnail }}" width="100%">
                                            <h1 class="margin-t-8">{{ \Illuminate\Support\Str::limit(strip_tags($article->title),50) }}</h1>
                                        </a>
                                        <p>{!! \Illuminate\Support\Str::limit(strip_tags($article->content),140) !!}</p>
                                    </li>
                                @if($index%3==2)
                                    </div>
                                @endif
                            @empty
                                <li class="col-md-12">None</li>
                            @endforelse
                        </ul>
                    </div>

                    <div class="pagination text-center">
                        {{ $articles->links() }}
                    </div>
                </div>
            </div>
        </div>

        <div class="col-md-3 sidebar">
            @include('templates.sidebar.help')
            @include('templates.sidebar.media-contacts')
            @include('templates.sidebar.media-pack')
            @include('templates.sidebar.tweets')
        </div>
    </div>
</div>

@section('custom-js')
    <script>
    </script>
@endsection

@include('footer')


