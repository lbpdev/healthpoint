@section('custom-styles')
    <link rel="stylesheet" href="{{ asset('public/css/magnific-popup.css') }}">
    <style type="text/css">
        .lightboxcontainer {
            width:100%;
            text-align:left;
        }
        .lightboxleft {
            width: 40%;
            float:left;
        }
        .lightboxright {
            width: 60%;
            float:left;
        }
        .lightboxright iframe {
            min-height: 390px;
        }
        .divtext {
            margin: 36px;
        }
        @media (max-width: 800px) {
            .lightboxleft {
                width: 100%;
            }
            .lightboxright {
                width: 100%;
            }
            .divtext {
                margin: 12px;
            }
        }

        #html5-watermark {
            display: none !important;
        }
    </style>
@endsection

@include('header')

<div class="pull-left">
    <div class="row">
        <div class="page col-md-9 main-content">

            <div class="col-md-12">
                <div class="row">
                    @if($data)
                        <h1 class="panel-title">{{ $data->title }}</h1>

                        <div class="col-md-12">
                            <div class="row padding-t-0 padding-b-10">
                                <p class="pull-left">{{ $data->created_at->format('M d Y') }}</p>
                                <a href="{{ route('pages.media.album') }}" class="brown-text pull-right">Back</a>
                            </div>
                        </div>
                        <div class="row padding-t-0">
                            <ul class="float-list album cols-3">
                                @foreach($data->contents as $photo)
                                    @if(count($photo->uploads))
                                        @if(strpos($photo->uploads[0]->mime_type, 'image')!== false)
                                            <li class="col-md-4 col-sm-6 col-xs-6">
                                                <div class="wrap clearfix">
                                                    <a href="{{ $photo->highRes }}">
                                                        <img src="{{ $photo->thumbnail }}" width="100%">
                                                    </a>
                                                </div>
                                            </li>
                                        @elseif(strpos($photo->uploads[0]->mime_type, 'video')!== false)
                                            <li class="col-md-4 col-sm-6 col-xs-6 video">
                                                <div class="wrap clearfix">
                                                    <div class="videoWrapper">
                                                        <div id="unmoved-fixture">
                                                            <video width="100%" preload="metadata" id="video-target">
                                                                <source src="{{ asset('public').$photo->uploads[0]->url }}#t=4" type="video/mp4">
                                                                Your browser does not support the video tag.
                                                            </video>
                                                        </div>
                                                        <span class="bt"></span>
                                                        <a href="{{ asset('public').$photo->uploads[0]->url }}" class="html5lightbox cover" data-width="780" data-height="480" title="{{ $photo->name }}">&nbsp;</a>
                                                    </div>
                                                    <p>{{ str_replace(array(".mp4",".m4v",'_'),array("",""," "),$photo->name) }}</p>
                                                </div>
                                            </li>
                                        @else
                                            <li>Unknown File</li>
                                        @endif
                                    @endif

                                @endforeach
                            </ul>
                        </div>
                    @else
                        <h1 class="alert alert-danger">Unknown Album</h1>
                    @endif
                </div>
            </div>
        </div>

        <div class="col-md-3 sidebar">
            @include('templates.sidebar.help')
            @include('templates.sidebar.media-contacts')
            @include('templates.sidebar.media-pack')
            @include('templates.sidebar.tweets')
        </div>
    </div>
</div>

@section('custom-js')
    <script src="{{ asset('public/js/jquery.magnific-popup.min.js') }}"></script>

    <script>
        $('.album').magnificPopup({
            delegate: 'a', // child items selector, by clicking on it popup will open
            type: 'image'
            // other options
        });
    </script>

    <script src="{{ asset('public/js/html5lightbox.js') }}"></script>
    <script type="text/javascript">
        var html5lightbox_options = {
            watermark: "",
            watermarklink: ""
        };
    </script>
@endsection

@include('footer')



