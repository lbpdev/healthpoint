@section('custom-styles')
    <style type="text/css">
        .lightboxcontainer {
            width:100%;
            text-align:left;
        }
        .lightboxleft {
            width: 40%;
            float:left;
        }
        .lightboxright {
            width: 60%;
            float:left;
        }
        .lightboxright iframe {
            min-height: 390px;
        }
        .divtext {
            margin: 36px;
        }
        @media (max-width: 800px) {
            .lightboxleft {
                width: 100%;
            }
            .lightboxright {
                width: 100%;
            }
            .divtext {
                margin: 12px;
            }
        }

        #html5-watermark {
            display: none !important;
        }
    </style>
@endsection

@include('header')

<div class="pull-left">
    <div class="row">
        <div class="page col-md-9 main-content">
            <div class="col-md-12">
                <div class="row">


                    <h1 class="panel-title">Manchester City FC Partnership Videos</h1>
                    <p>November 2014</p>
                    <div class="row padding-t-0">
                        <ul class="float-list videos cols-3">
                            <li class="col-md-4 col-sm-6">
                                <div class="wrap clearfix">
                                    <div class="videoWrapper">
                                        <video width="100%">
                                            <source src="{{ asset('public/videos/sample.mp4') }}" type="video/mp4">
                                            Your browser does not support the video tag.
                                        </video>
                                        <a href="{{ asset('public/videos/sample.mp4') }}" class="html5lightbox cover" data-width="480" data-height="320" title="Big Buck Bunny">&nbsp;</a>
                                    </div>
                                    <h1>Manchester City FC 2013/14 Season Review</h1>
                                </div>
                            </li>
                            <li class="col-md-4 col-sm-6">
                                <div class="wrap clearfix">
                                    <div class="videoWrapper">
                                        <video width="100%">
                                            <source src="{{ asset('public/videos/sample.mp4') }}" type="video/mp4">
                                            Your browser does not support the video tag.
                                        </video>
                                        <a href="{{ asset('public/videos/sample.mp4') }}" class="html5lightbox cover" data-width="480" data-height="320" title="Big Buck Bunny">&nbsp;</a>
                                    </div>
                                    <h1>Manchester City FC 2013/14 Season Review</h1>
                                </div>
                            </li>
                            <li class="col-md-4 col-sm-6">
                                <div class="wrap clearfix">
                                    <div class="videoWrapper">
                                        <video width="100%">
                                            <source src="{{ asset('public/videos/sample.mp4') }}" type="video/mp4">
                                            Your browser does not support the video tag.
                                        </video>
                                        <a href="{{ asset('public/videos/sample.mp4') }}" class="html5lightbox cover" data-width="480" data-height="320" title="Big Buck Bunny">&nbsp;</a>
                                    </div>
                                    <h1>Manchester City FC 2013/14 Season Review</h1>
                                </div>
                            </li>
                            <li class="col-md-4 col-sm-6">
                                <div class="wrap clearfix">
                                    <div class="videoWrapper">
                                        <video width="100%">
                                            <source src="{{ asset('public/videos/sample.mp4') }}" type="video/mp4">
                                            Your browser does not support the video tag.
                                        </video>
                                        <a href="{{ asset('public/videos/sample.mp4') }}" class="html5lightbox cover" data-width="480" data-height="320" title="Big Buck Bunny">&nbsp;</a>
                                    </div>
                                    <h1>Manchester City FC 2013/14 Season Review</h1>
                                </div>
                            </li>
                            <li class="col-md-4 col-sm-6">
                                <div class="wrap clearfix">
                                    <div class="videoWrapper">
                                        <video width="100%">
                                            <source src="{{ asset('public/videos/sample.mp4') }}" type="video/mp4">
                                            Your browser does not support the video tag.
                                        </video>
                                        <a href="{{ asset('public/videos/sample.mp4') }}" class="html5lightbox cover" data-width="480" data-height="320" title="Big Buck Bunny">&nbsp;</a>
                                    </div>
                                    <h1>Manchester City FC 2013/14 Season Review</h1>
                                </div>
                            </li>
                            <li class="col-md-4 col-sm-6">
                                <div class="wrap clearfix">
                                    <div class="videoWrapper">
                                        <video width="100%">
                                            <source src="{{ asset('public/videos/sample.mp4') }}" type="video/mp4">
                                            Your browser does not support the video tag.
                                        </video>
                                        <a href="{{ asset('public/videos/sample.mp4') }}" class="html5lightbox cover" data-width="480" data-height="320" title="Big Buck Bunny">&nbsp;</a>
                                    </div>
                                    <h1>Manchester City FC 2013/14 Season Review</h1>
                                </div>
                            </li>
                            <li class="col-md-4 col-sm-6">
                                <div class="wrap clearfix">
                                    <div class="videoWrapper">
                                        <video width="100%">
                                            <source src="{{ asset('public/videos/sample.mp4') }}" type="video/mp4">
                                            Your browser does not support the video tag.
                                        </video>
                                        <a href="{{ asset('public/videos/sample.mp4') }}" class="html5lightbox cover" data-width="480" data-height="320" title="Big Buck Bunny">&nbsp;</a>
                                    </div>
                                    <h1>Manchester City FC 2013/14 Season Review</h1>
                                </div>
                            </li>
                            <li class="col-md-4 col-sm-6">
                                <div class="wrap clearfix">
                                    <div class="videoWrapper">
                                        <video width="100%">
                                            <source src="{{ asset('public/videos/sample.mp4') }}" type="video/mp4">
                                            Your browser does not support the video tag.
                                        </video>
                                        <a href="{{ asset('public/videos/sample.mp4') }}" class="html5lightbox cover" data-width="480" data-height="320" title="Big Buck Bunny">&nbsp;</a>
                                    </div>
                                    <h1>Manchester City FC 2013/14 Season Review</h1>
                                </div>
                            </li>
                        </ul>
                    </div>

                    <div class="pagination text-center">
                        <a href="#" class="active">1</a>
                        <a href="#">2</a>
                        <a href="#">3</a>
                        <a href="#">4</a>
                        <a href="#">5</a>
                        <a href="#">6</a>
                        <a href="#">7</a>
                    </div>
                </div>
            </div>
        </div>

        <div class="col-md-3 sidebar">
            @include('templates.sidebar.help')
            @include('templates.sidebar.media-contacts')
            @include('templates.sidebar.media-pack')
            @include('templates.sidebar.tweets')
        </div>
    </div>
</div>

@section('custom-js')
    <script src="{{ asset('public/js/html5lightbox.js') }}"></script>
    <script type="text/javascript">
        var html5lightbox_options = {
            watermark: "",
            watermarklink: ""
        };
    </script>
@endsection

@include('footer')


