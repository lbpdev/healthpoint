@include('header')
<div class="pull-left">
    <div class="row">
        <div class="page col-md-9 main-content">
            <h1 class="panel-title">Media</h1>

            @include('templates.media-banner')

            <div class="col-md-12">
                <div class="row">

                    @include('pages.media.nav')

                    <h1 class="panel-title">Video & Photo Galleries</h1>
                    <p>See our physicians and employees in action at the numerous community events featuring Healthpoint since our opening in 2013.</p>
                    <div class="row padding-t-0">
                        <ul class="float-list gallery cols-3">
                            @foreach($galleries as $index=>$gallery)
                                @if($index%3==0)
                                    <div class="clearfix">
                                @endif
                                        <li class="col-md-4 col-sm-6 col-xs-12">
                                            <div class="wrap clearfix">
                                                <a href="{{ route('pages.media.album.single',$gallery->slug) }}">
                                                    <img src="{{ $gallery->thumbnail }}" width="100%">
                                                    <h1 class="margin-t-8">{{ $gallery->title }}</h1>
                                                </a>
                                                {{--<span class="date">{{ $gallery->created_at->format('M d Y') }}</span>--}}
                                            </div>
                                        </li>
                                @if($index%3==2)
                                    </div>
                                @endif
                            @endforeach
                        </ul>
                    </div>

                    <div class="pagination text-center">
                        {{ $galleries->links() }}
                    </div>
                </div>
            </div>
        </div>

        <div class="col-md-3 sidebar">
            @include('templates.sidebar.help')
            @include('templates.sidebar.media-contacts')
            @include('templates.sidebar.media-pack')
            @include('templates.sidebar.tweets')
        </div>
    </div>
</div>

@section('custom-js')
    <script>
    </script>
@endsection

@include('footer')


