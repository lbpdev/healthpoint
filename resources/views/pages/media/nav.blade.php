<div class="col-md-12">
    <div class="row">
        <ul class="border-line-list col-4 clearfix short">
            <li class="col-md-4 col-xs-12 {{ str_contains(Request::path(), 'news') ? 'active' : '' }}"><a href="{{ url('media/news') }}">News </a></li>
{{--            <li class="col-md-3 col-xs-12 {{ str_contains(Request::path(), 'seminars-and-workshops') ? 'active' : '' }}"><a href="{{ url('media/seminars-and-workshops') }}">Seminars & Workshops</a></li>--}}
            <li class="col-md-4 col-xs-12 {{ str_contains(Request::path(), 'hospital-campaigns') ? 'active' : '' }}"><a href="{{ url('media/hospital-campaigns') }}">Hospital Campaigns</a></li>
            <li class="col-md-4 col-xs-12 {{ str_contains(Request::path(), 'galleries') ? 'active' : '' }}"><a href="{{ url('media/galleries') }}">Galleries</a></li>
        </ul>
    </div>
</div>