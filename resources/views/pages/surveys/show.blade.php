
@section('metas')

        <!-- Place this data between the <head> tags of your website -->
<meta name="description" content="{!! \Illuminate\Support\Str::limit(strip_tags($data->description), 150) !!}" />

<!-- Schema.org markup for Google+ -->
<meta itemprop="name" content="{{ $data->title }}">
<meta itemprop="description" content="{!! \Illuminate\Support\Str::limit(strip_tags($data->description), 150) !!}">
<meta itemprop="image" content="{{ asset( $data->thumbnail) }}">

<!-- Twitter Card data -->
<meta name="twitter:card" content="summary_large_image">
<meta name="twitter:site" content="www.middleeastdoctor.com">
<meta name="twitter:title" content="{{ $data->title }}">
<meta name="twitter:description" content="{!!\Illuminate\Support\Str::limit(strip_tags($data->description), 150) !!}">
<meta name="twitter:creator" content="Middle East Doctor">
<!-- Twitter summary card with large image must be at least 280x150px -->
<meta name="twitter:image:src" content="{{ asset( $data->thumbnail) }}">

<!-- Open Graph data -->
<meta property="og:title" content="{{ $data->title }}" />
<meta property="og:type" content="article" />
<meta property="og:url" content="{{ Request::fullUrl()}}" />
<meta property="og:image" content="{{ asset( $data->thumbnail) }}" />
<meta property="og:description" content="{!!\Illuminate\Support\Str::limit(strip_tags($data->description), 150) !!}" />
<meta property="og:site_name" content="www..com" />
<meta property="article:published_time" content="{{ $data->created_at }}" />
<meta property="article:modified_time" content="{{ $data->updated_at }}" />
<meta property="article:section" content="Article Section" />
<meta property="article:tag" content="Article Tag" />
<meta property="fb:admins" content="100000370643856" />

<style>
    .stButton .stLarge {
        height: 24px !important;
        width: 21px !important;
        position: relative !important;
        opacity: 0 !important;
    }
    .stButton .stLarge:hover {
        height: 24px !important;
        width: 21px !important;
        opacity: 0 !important;
    }
</style>
<link rel="stylesheet" href="{{ asset('public/admin') }}/plugins/daterangepicker/daterangepicker.css">
@stop

@include('header')

<div class="pull-left">
    <div class="row">
        <div class="page col-md-2">
            <h1 class="panel-title">Share on</h1>
            <ul class="socials pull-left margin-t-0">

                {{--<span class='st_facebook_large' displayText='Facebook'></span>--}}
                {{--<span class='st_twitter_large' displayText='Tweet'></span>--}}
                {{--<span class='st_googleplus_large' displayText='Google +'></span>--}}
                {{--<span class='st_linkedin_large' displayText='LinkedIn'></span>--}}
                {{--<span class='st_email_large' displayText='Email'></span>--}}

                <li><a href="#" class="st_facebook_large social-icon fb"></a></li>
                <li><a href="#" class="st_twitter_large social-icon tw"></a></li>
                <li><a href="#" class="st_linkedin_large social-icon lk"></a></li>
            </ul>
            <p class="brown-text">{{ $data ? $data->created_at->format('M d Y') : '' }}</p>
        </div>
        <div class="page col-md-7 main-content gray-border-left">
            <div class="col-md-12">
                <div class="row">
                    @if(Session::has('success'))
                        <div class="alert alert-success">
                            {{ Session::get('success') }}
                        </div>
                    @else
                        @if($data)
                            <h1 class="panel-title">{{ $data->title }}</h1>

                            @if($data->photo || $data->thumbnailLong)
                                <img src="{{ $data->photo ? $data->photo : $data->thumbnailLong }}" width="100%" class="margin-b-10">
                            @endif

                            {!! str_replace("&nbsp;"," ",$data->description) !!}

                            @if(count($data->questions))
                                {!! Form::open(['route'=>'surveys.store']) !!}
                                <input type="hidden" name="id" value="{{ $data->id }}">
                                @foreach($data->questions as $index=>$question)
                                    <?php $qID = $data->questions[$index]->id; ?>
                                    <div class="form-group clearfix ui-state-default" data-id="{{ $question->id }}">
                                        <label>{{ $question->question }}</label>
                                        <br>
                                        @if($question->type->slug=='radio' || $question->type->slug=='checkbox')
                                            @foreach($question->choices as $index=>$choice)
                                                @if($question->is_float)
                                                    <div class="pull-left margin-r-20">
                                                        <input id="{{$choice->id}}"  {{ $index==0 ? 'checked' : '' }} type="{{ $question->type->slug }}" name="questions[{{$qID}}][]" value="{{$choice->value}}">
                                                        <label for="{{$choice->id}}">{{$choice->value}}</label>
                                                    </div>
                                                @else
                                                    <input id="{{$choice->id}}"  {{ $index==0 ? 'checked' : '' }} type="{{ $question->type->slug }}" name="questions[{{$qID}}][]" value="{{$choice->value}}">
                                                    <label for="{{$choice->id}}">{{$choice->value}}</label>
                                                    <br>
                                                @endif
                                            @endforeach
                                        @elseif($question->type->slug=='rate')
                                                @foreach($question->choices as $cIndex=>$choice)
                                                    <div class="choice clearfix" style="width: 100%">
                                                        <input type="hidden" name="questions[{{ $question->id }}][choices][{{$choice->id}}][choice_id]" value="{{$choice->id}}">
                                                        <div class="pull-left margin-r-20">
                                                            {{ $choice->value  }}
                                                        </div>
                                                        @for($x=0;$x<$choice->ratability;$x++)
                                                            <input id="{{$choice->id}}{{$x}}" {{ ($x+1)==$choice->ratability ? 'checked' : '' }} type="radio" name="questions[{{ $question->id }}][choices][{{$choice->id}}][rate]" value="{{$x+1}}">
                                                            <label for="{{$choice->id}}{{$x}}" class="margin-r-10">{{$x+1}}</label>
                                                        @endfor
                                                    </div>
                                                @endforeach
                                        @elseif($question->type->slug=='textarea')
                                            <textarea name="questions[{{$qID}}]" {{ $question->is_required ? 'required="required"' : '' }}></textarea>
                                        @elseif($question->type->slug=='date')
                                            <input type="text" class="datepicker" name="questions[{{$qID}}]" {{ $question->is_required ? 'required="required"' : '' }}>
                                        @else
                                            <input type="text" name="questions[{{$qID}}]" {{ $question->is_required ? 'required="required"' : '' }}>
                                        @endif
                                    </div>

                                @endforeach
                                <input type="submit" value="Submit">
                                {!! Form::close() !!}
                            @endif
                        @else
                            <h1 class="alert-danger alert">Article not found.</h1>
                        @endif
                    @endif

                    <br>
                    <a class="pull-right" href="{{ redirect()->back()->getTargetUrl() }}">Back</a>
                </div>
            </div>
        </div>

        <div class="col-md-3 sidebar">
            @include('templates.sidebar.help')
            @include('templates.sidebar.more-news')
        </div>
    </div>
</div>

@section('custom-js')
    <!-- daterangepicker -->
    <script src="https://cdnjs.cloudflare.com/ajax/libs/moment.js/2.11.2/moment.min.js"></script>
    <script src="{{ asset('public/admin') }}/plugins/daterangepicker/daterangepicker.js"></script>

    <script>
        $('.main-content').find('ul').addClass('standard-list');
        $('.main-content').find('ol li').css('color','#4b3328');
        $( ".datepicker" ).daterangepicker({
            singleDatePicker: true,
            showDropdowns: true
        });
    </script>
@endsection

@include('footer')


