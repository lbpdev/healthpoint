@include('header')
<div class="pull-left">
    <div class="row pull-left">
        <div class="page col-md-9 main-content">
            <h1 class="panel-title">Search Results for "{{ $keyword }}"</h1>


            <div class="col-md-12 pull-left">
                <div class="row">

                    @if(isset($articles))
                        @if(count($articles))
                            <h1>Articles / Pages</h1>
                            <ul class="standard-list">
                            @foreach($articles as $article)
                                <li>
                                    <h2><a href="{{ route('media.news.single',$article->slug) }}" class="">{{ $article->title }}</a></h2>
                                    <p style="margin-left: 20px">{{ \Illuminate\Support\Str::limit(strip_tags($article->content),120) }}</p>
                                </li>
                            @endforeach
                            </ul>
                            <hr>
                        @endif
                    @endif


                    @if(isset($physicians))
                        @if(count($physicians))
                            <h1>Phyisicians</h1>
                            <ul class="standard-list">
                            @foreach($physicians as $physician)
                                <li>
                                    <h2><a href="{{ route('physician.single',$physician->slug) }}">{{ $physician->name }}</a></h2>
                                    <p style="margin-left: 20px">{{ \Illuminate\Support\Str::limit(strip_tags($physician->description),120) }}</p>
                                </li>
                            @endforeach
                            </ul>
                            <hr>
                        @endif
                    @endif


                    @if(isset($departments))
                        @if(count($departments))
                            <h1>Departments</h1>
                            <ul class="standard-list">
                            @foreach($departments as $department)
                                    <li>
                                        <h2><a href="{{ route('department.single',$department->slug) }}" >{{ $department->name }}</a></h2>
                                        <p style="margin-left: 20px">{{ \Illuminate\Support\Str::limit(strip_tags($department->description),120) }}</p>
                                    </li>
                            @endforeach
                            </ul>
                        @else
                            @if(!isset($articles))
                                <p>Sorry we could not find a service from your keyword "{{ $keyword }}". You can find all our services here: <a href="{{ route('department.index') }}">Clinical Services</a> </p>
                            @endif
                        @endif
                    @endif


                    @if(isset($albums))
                        @if(count($albums))
                            <h1>Gallery</h1>
                            <ul class="standard-list">
                            @foreach($albums as $album)
                                    <li>
                                        <h2><a href="{{ route('pages.media.album.single',$album->slug) }}" >{{ $album->title }}</a></h2>
                                    </li>
                            @endforeach
                            </ul>
                        @else
                        @endif
                    @endif

                </div>
            </div>
        </div>

        <div class="col-md-3 sidebar">
            @include('templates.sidebar.help')
            @include('templates.sidebar.patient-rights')
        </div>
    </div>
</div>

@section('custom-js')
    <script>
    </script>
@endsection

@include('footer')


