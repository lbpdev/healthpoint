<?php
namespace App\Repositories;

use App\Models\Article;
use App\Services\Uploaders\ArticleThumbnailUploader;
use Carbon\Carbon;
use GuzzleHttp\Psr7\Request;

class ArticleManager {
    use CanCreateSlug;

    public function __construct(Article $model, ArticleThumbnailUploader $uploader)
    {
        $this->model = $model;
        $this->uploader = $uploader;
    }

    public function getByCategory($cat_id,$limit){
        $data = $this->model->where('category_id',$cat_id)->limit($limit)->get();
        return $data;
    }

    public function store($request){
        $input = $request->input();

        $file = $request->file('thumbnail');

        $input['slug'] = $this->generateSlug($input['title']);
        $input['created_at'] = $input['created_at'] ? strtotime($input['created_at']) : Carbon::now();
        $article = $this->model->create($input);


        if($article){
            $photo = ($file != null ? $this->uploader->upload($file) : false);

            if($photo)
                $article->uploads()->createMany($photo);
        }

        return $article;
    }

    public function update($request){
        $input = $request->except('id');
        $file = $request->file('thumbnail');

        $article = $this->model->where('id',$request->input('id'))->first();

        if($article){

            if($input['title'] != $article->title)
                $input['slug'] = $this->generateSlug($input['title']);

            $input['created_at'] = $input['created_at'] ? strtotime($input['created_at']) : Carbon::now();

            $article->update($input);

            if(isset($input['remove_thumb']))
                $article->uploads()->delete();

            if($file != null){

                $photo = ($file != null ? $this->uploader->upload($file) : false);

                if($photo){
                    $article->uploads()->delete();
                    $article->uploads()->createMany($photo);
                }
            }
        }

        return $article;
    }
}