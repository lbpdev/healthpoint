<?php
namespace App\Repositories;

use App\Models\AppointmentRequest;
use App\Models\AppointmentRequestInfo;
use App\Models\Article;
use App\Models\Insurance;
use App\Services\Uploaders\InsuranceThumbnailUploader;
use GuzzleHttp\Psr7\Request;
use Illuminate\Support\Facades\Mail;

class AppointmentRequestManager {
    use CanCreateSlug;

    public function __construct(AppointmentRequest $model, AppointmentRequestInfo $info)
    {
        $this->model = $model;
        $this->info = $info;
    }

    public function store($input){

        $data['name'] = $input['First_Name'] . ' ' . $input['Surname'];

        $appointment = $this->model->create($data);

        $input['Date_of_Birth'] = $input['Date_of_Birth']['month'] . ' '. $input['Date_of_Birth']['day'] . ' '. $input['Date_of_Birth']['year'];

        foreach($input as $key=>$value){
            $data = [];

            $data['key'] = str_replace("_", " ", $key);
            $data['value'] = $value;

            $appointment->info()->create($data);
        }

        return $appointment;
    }

    public function update($request){
        $input = $request->except('id');
        $file = $request->file('thumbnail');

        $data = $this->model->where('id',$request->input('id'))->first();

        if($data){

            if($input['name'] != $data->title)
                $input['slug'] = $this->generateSlug($input['name']);

            $data->update($input);

            if(isset($input['remove_thumb']))
                $data->uploads()->delete();

            if($file != null){

                $photo = ($file != null ? $this->uploader->upload($file) : false);

                if($photo){
                    $data->uploads()->delete();
                    $data->uploads()->createMany($photo);
                }
            }
        }

        return $data;
    }
}