<?php
namespace App\Repositories\Traits;

trait Fetcher {

    public function getAll(){
        return $this->model->orderBy('name','ASC')->get();
    }

    public function getByDate(){
        return $this->model->orderBy('date','desc')->get();
    }

    public function getAllArray(){
        return $this->model->orderBy('name','ASC')->get()->toArray();
    }

    public function getLimit($limit){
        return $this->model->orderBy('name','ASC')->limit($limit)->get();
    }

    public function getLatest($limit){
        return $this->model->orderBy('created_at','DESC')->limit($limit)->get();
    }
}