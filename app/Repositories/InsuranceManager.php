<?php
namespace App\Repositories;

use App\Models\Article;
use App\Models\Insurance;
use App\Services\Uploaders\InsuranceThumbnailUploader;
use GuzzleHttp\Psr7\Request;

class InsuranceManager {
    use CanCreateSlug;

    public function __construct(Insurance $model, InsuranceThumbnailUploader $uploader)
    {
        $this->model = $model;
        $this->uploader = $uploader;
    }

    public function getByCategory($cat_id,$limit){
        $data = $this->model->where('category_id',$cat_id)->limit($limit)->get();
        return $data;
    }

    public function store($request){
        $input = $request->input();
        $file = $request->file('thumbnail');

        $input['slug'] = $this->generateSlug($input['name']);
        $data = $this->model->create($input);

        if($data){
            $photo = ($file != null ? $this->uploader->upload($file) : false);

            if($photo)
                $data->uploads()->createMany($photo);
        }

        return $data;
    }

    public function update($request){
        $input = $request->except('id');
        $file = $request->file('thumbnail');

        $data = $this->model->where('id',$request->input('id'))->first();

        if($data){

            if($input['name'] != $data->title)
                $input['slug'] = $this->generateSlug($input['name']);

            $data->update($input);

            if(isset($input['remove_thumb']))
                $data->uploads()->delete();

            if($file != null){

                $photo = ($file != null ? $this->uploader->upload($file) : false);

                if($photo){
                    $data->uploads()->delete();
                    $data->uploads()->createMany($photo);
                }
            }
        }

        return $data;
    }
}