<?php
namespace App\Repositories;

use App\Models\Option;
use App\Models\Physician;
use App\Models\PhysicianMetaData;
use App\Models\PhysicianMetaType;
use App\Services\Uploaders\PhysicianThumbnailUploader;

use GuzzleHttp\Psr7\Request;

class PhysicianManager {
    use CanCreateSlug;

    public function __construct(Physician $model, PhysicianThumbnailUploader $uploader)
    {
        $this->model = $model;
        $this->uploader = $uploader;
    }

    public function getByCategory($cat_id,$limit){
        $data = $this->model->where('category_id',$cat_id)->limit($limit)->get();
        return $data;
    }

    public function getMetas($user){
        $types = PhysicianMetaType::get();
        $data = [];

        if($user){
            foreach($types as $type){
                $data[$type->slug] = PhysicianMetaData::where('physician_id',$user->id)->where('meta_type_id',$type->id)->get();
            }
        }
        return $data;
    }

    public function store($request) {
        $input = $request->except('meta','department_id','contact-number');
        $deparment_id = $request->input('department_id');
        $contact_number = $request->input('contact-number');
        $meta = $request->input('meta');

        $file = $request->file('thumbnail');

        $slug = $this->generateSlug($input['fname'] . '-' . $input['lname']);

        $input['slug'] = $slug;

        $physician = $this->model->create($input);

        if($physician){
            $photo = ($file != null ? $this->uploader->upload($file) : false);

            if($photo)
                $physician->uploads()->createMany($photo);

            if($deparment_id)
                $physician->department()->attach($deparment_id);

            if($contact_number){
                $meta_type = PhysicianMetaType::where('slug','contact-number')->first();

                if($meta_type)
                    $physician->meta()->attach($meta_type->id, ['value' => $contact_number]);
            }

            foreach($meta as $key=>$type){

                $meta_type = PhysicianMetaType::where('slug',$key)->first();

                if($meta_type){
                    if(trim($type))
                        $physician->meta()->attach($meta_type->id, ['value' => $type]);
                }
            }
        }

        return $physician;
    }

    public function update($request){

        $input = $request->except('meta','department_id','contact-number','id');
        $deparment_id = $request->input('department_id');
        $contact_number = $request->input('contact-number');
        $meta = $request->input('meta');

        $file = $request->file('thumbnail');

        $physician = $this->model->where('id',$request->input('id'))->first();

        if($physician){
            $physician->update($input);

            $photo = ($file != null ? $this->uploader->upload($file) : false);

            if(isset($request->remove_thumb))
                $physician->uploads()->delete();

            if($photo){
                $physician->uploads()->delete();
                $physician->uploads()->createMany($photo);
            }

            if($deparment_id){
                $physician->department()->sync([$deparment_id]);
            }

            $physician->meta()->sync([]);

            if($contact_number){
                $meta_type = PhysicianMetaType::where('slug','contact-number')->first();
                if($meta_type)
                    $physician->meta()->attach($meta_type->id, ['value' => $contact_number]);
            }

            foreach($meta as $key=>$type){

                $meta_type = PhysicianMetaType::where('slug',$key)->first();

                if($meta_type){
                    if(trim($type))
                        $physician->meta()->attach($meta_type->id, ['value' => $type]);
                }
            }
        }

        return $physician;
    }

    public function feature($id){

        $physician = $this->model->where('id',$id)->first();
        $option    = Option::where('slug','featured-physician')->first();

        if($physician){

            if($option)
                $option->update(['value'=>$physician->id]);
            else
                Option::create(['name'=>'Featured Physician', 'slug' => 'featured-physician', 'value' => $physician->id]);

            return true;
        }

        return false;
    }

    public function unfeature($id){

        Option::where('slug','featured-physician')->delete();

    }
}