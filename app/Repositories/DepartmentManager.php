<?php
namespace App\Repositories;

use App\Models\Department;
use App\Services\Uploaders\ArticleThumbnailUploader;
use GuzzleHttp\Psr7\Request;

class DepartmentManager {
    use CanCreateSlug;

    public function __construct(Department $model)
    {
        $this->model = $model;
    }

    public function getByCategory($cat_id,$limit){
        $data = $this->model->where('category_id',$cat_id)->limit($limit)->get();
        return $data;
    }

    public function store($request){
        $input = $request->input();

        $input['slug'] = $this->generateSlug($input['name']);
        $data = $this->model->create($input);

        if($input['physician_id'][0] || $input['physician_id'][1]){
            foreach($input['physician_id'] as $head)
                if($head)
                    $ids[] = $head;

            $data->headPhysician()->sync($ids);
        }

        return $data;
    }

    public function update($request){
        $input = $request->except('id');

        $data = $this->model->where('id',$request->input('id'))->first();

        if($data){

            if($input['name'] != $data->name)
                $input['slug'] = $this->generateSlug($input['name']);

            if($input['physician_id'][0] || $input['physician_id'][1]){
                foreach($input['physician_id'] as $head)
                    if($head)
                        $ids[] = $head;

                $data->headPhysician()->sync($ids);
            } else {
                $data->headPhysician()->detach();
            }

            $data->update($input);
        }

        return $data;
    }
}