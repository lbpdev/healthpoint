<?php
namespace App\Repositories;

use App\Models\Album;
use App\Models\Article;
use App\Models\Insurance;
use App\Models\Video;
use App\Services\Uploaders\AlbumPhotosUploader;
use App\Services\Uploaders\AlbumThumbnailUploader;
use App\Services\Uploaders\InsuranceThumbnailUploader;
use App\Services\Uploaders\VideoThumbnailUploader;
use App\Services\Uploaders\VideoUploader;
use GuzzleHttp\Psr7\Request;

class VideoManager {
    use CanCreateSlug;

    public function __construct(Video $model, VideoUploader $uploader,VideoThumbnailUploader $thumbnailUploader)
    {
        $this->model = $model;
        $this->uploader = $uploader;
        $this->thumbUploader = $thumbnailUploader;
    }

    public function getByCategory($cat,$limit){
        $data = $this->model->where('category',$cat)->limit($limit)->get();
        return $data;
    }

    public function store($request){
        $input = $request->input();
        $file = $request->file('video');
        $photo = $request->file('photo');

        $input['slug'] = $this->generateSlug($input['name']);

        $video = ($file != null ? $this->uploader->upload($file) : false);

        if($video[0])
            $input['source'] = $video[0]['path'].'/'.$video[0]['file_name'];

        $data = $this->model->create($input);

        $thumbs = ($photo != null ? $this->thumbUploader->upload($photo) : false);

        if($thumbs)
            $data->uploads()->createMany($thumbs);

        return $data;
    }

    public function update($request){
        $input = $request->except('id');
        $file = $request->file('video');
        $photo = $request->file('photo');

        $data = $this->model->where('id',$request->input('id'))->first();

        if($data){

            if($input['name'] != $data->name)
                $input['slug'] = $this->generateSlug($input['name']);

            if(isset($input['remove_thumb']))
                $data->uploads()->delete();
            
            if($file){
                $video = ($file != null ? $this->uploader->upload($file) : false);
                $input['source'] = $video[0]['path'].'/'.$video[0]['file_name'];
            }

            $data->update($input);

            $thumbs = ($photo != null ? $this->thumbUploader->upload($photo) : false);

            if($thumbs)
                $data->uploads()->createMany($thumbs);
        }

        return $data;
    }
}