<?php
namespace App\Repositories;

use App\Models\Event;
use App\Services\Uploaders\ArticleThumbnailUploader;
use GuzzleHttp\Psr7\Request;

class EventManager {
    use CanCreateSlug;

    public function __construct(Event $model, ArticleThumbnailUploader $uploader)
    {
        $this->model = $model;
        $this->uploader = $uploader;
    }

    public function getByCategory($cat_id,$limit){
        $data = $this->model->where('category_id',$cat_id)->limit($limit)->get();
        return $data;
    }

    public function store($request){
        $input = $request->input();
        $file = $request->file('thumbnail');

        $input['slug'] = $this->generateSlug($input['title']);
        $input['date'] = strtotime($input['date']);
        $article = $this->model->create($input);

        if($article){
            $photo = ($file != null ? $this->uploader->upload($file) : false);

            if($photo)
                $article->uploads()->createMany($photo);
        }

        return $article;
    }

    public function update($request){
        $input = $request->except('id');
        $file = $request->file('thumbnail');

        $article = $this->model->where('id',$request->input('id'))->first();

        if($article){

            if($input['title'] != $article->title)
                $input['slug'] = $this->generateSlug($input['title']);

            $article->update($input);

            if(isset($input['remove_thumb']))
                $article->uploads()->delete();

            if($file != null){

                $photo = ($file != null ? $this->uploader->upload($file) : false);

                if($photo){
                    $article->uploads()->delete();
                    $article->uploads()->createMany($photo);
                }
            }
        }

        return $article;
    }
}