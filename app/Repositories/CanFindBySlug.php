<?php namespace App\Repositories\Eloquent;

trait CanFindBySlug {

    /**
     * Find an entity by its slug.
     *
     * @param  string $slug
     *
     * @return mixed|null
     */

    public function findBySlug($slug)
    {
        return $this->model->whereSlug($slug)->first();
    }
}