<?php

namespace App\Http\Controllers;

use App\Models\Article;
use App\Repositories\Traits\Fetcher;
use Illuminate\Http\Request;

use App\Http\Requests;

class BlogController extends Controller
{
    use Fetcher;

    public function __construct(Article $model)
    {
        $this->model = $model;
    }

    public function index(){
        return view('pages.blog.index');
    }

    public function single($slug){
        $article = $this->model->where('slug',$slug)->first();
        $article->views++;
        $article->save();

        return view('pages.blog.single',compact('article'));
    }
}
