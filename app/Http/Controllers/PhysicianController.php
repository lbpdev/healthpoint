<?php

namespace App\Http\Controllers;

use App\Models\Physician;
use App\Models\PhysicianMetaType;
use App\Repositories\PhysicianManager;
use App\Repositories\Traits\Fetcher;
use App\Services\PhysicianFetcher;
use Illuminate\Http\Request;

use App\Http\Requests;

class PhysicianController extends Controller
{
    use Fetcher;

    public function __construct(Physician $model,PhysicianFetcher $fetcher,PhysicianManager $manager)
    {
        $this->model = $model;
        $this->fetcher = $fetcher;
        $this->physicians = $manager;
    }

    public function index(){

        if(isset($_GET['gender']) || isset($_GET['specialty']) || isset($_GET['name']))
            $query = $this->fetcher->filter();
        else
            $query = $this->model;


        $physicians = $query->orderBy('lname','ASC')->paginate(18);

        return view('pages.doctors.index',compact('physicians'));
    }

    public function single($slug){
        $physician = $this->model->where('slug',$slug)->first();
        $physician['meta'] = $this->physicians->getMetas($physician);
        $metaTypes = PhysicianMetaType::where('slug','!=','contact-number')->get();

        return view('pages.doctors.profile',compact('physician','metaTypes'));
    }
}
