<?php

namespace App\Http\Controllers;

use App\Models\Department;
use App\Models\Insurance;
use Illuminate\Http\Request;

use App\Http\Requests;
use App\Repositories\ArticleManager;

class HomeController extends Controller
{
    public function __construct()
    {
    }

    public function index()
    {
        return view('home',compact('departments','insurances'));
    }
}
