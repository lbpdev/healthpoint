<?php

namespace App\Http\Controllers;

use App\Models\AppointmentRequest;
use App\Repositories\AppointmentRequestManager;
use Illuminate\Http\Request;

use App\Http\Requests;
use Illuminate\Support\Facades\Mail;

class AppointmentController extends Controller
{

    public function __construct(AppointmentRequestManager $manager)
    {
        $this->manager = $manager;
    }

    public function index(){
        return view('pages.appointments.index');
    }

    public function store(Request $request){
        $input = $request->except('_token');

        $data = $this->manager->store($input);

        $data->load('info');

        Mail::send('emails.appointments.new', ['data' => $data], function ($m) {
            $m->from('appointments@healthpoint.ae', 'Healthpoint Appointment');

            $m->to(env('MAIL_TO'), 'Healthpoint Appointment Manager')->subject('Healthpoint Appointment Request');
        });

        return view('pages.appointments.success');
    }
}
