<?php

namespace App\Http\Controllers\Admin;

use App\Models\Survey\Survey;
use App\Models\Survey\SurveyQuestion;
use App\Models\Survey\SurveyQuestionAnswer;
use App\Models\Survey\SurveyQuestionAnswerItem;
use App\Models\Survey\SurveyQuestionChoiceRating;
use App\Models\Survey\SurveyQuestionType;
use App\Repositories\CanCreateSlug;
use App\Services\Uploaders\SurveyImageUploader;
use App\Services\Uploaders\SurveyThumbnailUploader;
use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Session;
use Maatwebsite\Excel\Facades\Excel;

class SurveyController extends Controller
{
    use CanCreateSlug;

    public function __construct(Survey $model,SurveyThumbnailUploader $uploader, SurveyImageUploader $iuploader)
    {
        $this->model = $model;
        $this->uploader = $uploader;
        $this->iuploader = $iuploader;
    }

    public function index(){
        $data = $this->model->with('entries')->get();

        return view('admin.surveys.index',compact('data'));
    }

    public function create(){
        $data['types'] = SurveyQuestionType::pluck('name','slug');

        return view('admin.surveys.create',compact('data'));
    }

    public function store(Request $request){
        $surveyData = $request->input('survey');
        $questions = $request->input('question');


        $surveyData['slug'] = $this->generateSlug($surveyData['title']);

        $survey = $this->model->create($surveyData);

        if($survey)
        {
            if(count($questions)){
                foreach ($questions as $question){
                    $type = SurveyQuestionType::where('slug',$question['type'])->pluck('id')->first();

                    if($type){
                        $question['survey_question_type_id'] = $type;
                        $newQ = $survey->questions()->create($question);

                        if($newQ && isset($question['choices'])){
                            foreach($question['choices'] as $choice){
                                $data = [];

                                if(trim($choice['choice'])){
                                    $data['value'] = $choice['choice'];

                                    if(isset($choice['rate']))
                                        $data['ratability'] = $choice['rate'];

                                    $newQ->choices()->create($data);
                                }
                            }
                        }
                    }
                }
            }

            $file = $request->file('thumbnail');

            $photo = ($file != null ? $this->uploader->upload($file) : false);

            if($photo)
                $survey->uploads()->createMany($photo);

            // Actual Image

            $file = $request->file('photo');

            $photo = ($file != null ? $this->iuploader->upload($file) : false);

            if($photo)
                $survey->uploads()->createMany($photo);

            return redirect()->route('admin.surveys.show',$survey->id);
        }
    }

    public function update(Request $request){
        $surveyData = $request->input('survey');
        $questions = $request->input('question');
        $file = $request->file('thumbnail');

        $survey = $this->model->find($request->input('id'));

        if($survey){

            if($survey->title != $surveyData['title'])
                $surveyData['slug'] = $this->generateSlug($surveyData['title']);

            $questions['is_float'] = isset($questions['is_float']) ? 1 : 0;
            $questions['is_required'] = isset($questions['is_required']) ? 1 : 0;

            $survey->update($surveyData);
            $survey->questions()->delete();

            foreach ($questions as $id=>$question){

                $type = SurveyQuestionType::where('slug',$question['type'])->pluck('id')->first();

                if($type){
                    $question['survey_question_type_id'] = $type;

                    $newQ = $survey->questions()->create($question);

                    if($newQ && isset($question['choices'])){
                        foreach($question['choices'] as $choice){
                            if(trim($choice['choice'])){
                                $data['value'] = $choice['choice'];

                                if(isset($choice['rate']))
                                    $data['ratability'] = $choice['rate'];

                                $newQ->choices()->create($data);
                            }
                        }
                    }
                }
            }

            if($request->has('remove_thumb'))
                $survey->uploads()->delete();

            if($file != null){

                $photo = ($file != null ? $this->uploader->upload($file) : false);

                if($photo){
                    $survey->uploads()->where('template','!=','photo')->delete();
                    $survey->uploads()->createMany($photo);
                }
            }

            // Actual Image

            $file = $request->file('photo');

            if($file != null){

                $photo = ($file != null ? $this->iuploader->upload($file) : false);

                if($photo){
                    $survey->uploads()->where('template','photo')->delete();
                    $survey->uploads()->createMany($photo);
                }
            }

            return redirect()->route('admin.surveys.show',$survey->id);
        }
    }

    public function show($id){
        $survey = $this->model->find($id);

        if($survey){

            $data['types'] = SurveyQuestionType::pluck('name','slug');

            $survey->load('questions.type','questions.choices');

            return view('admin.surveys.show',compact('data','survey'));
        }
    }

    public function preview($id){
        $data = Survey::find($id);

        if($data){
            $data->load('questions.type','questions.choices');

            return view('admin.surveys.preview',compact('data'));
        }
    }

    public function togglePublish($id){
        $data = $this->model->find($id);

        if($data){
            if($data->is_active)
            {
                $data->is_active = 0;
                Session::flash('message','Survey unpublished.');
            }
            else
            {
                $data->is_active = 1;
                Session::flash('message','Survey published.');
            }

            $data->save();

            return redirect()->back();
        }
    }

    public function excel($id){
        $survey = $this->model->find($id);

        if($survey){
            $data = [];

            foreach ($survey->questions  as $index=>$question){
                $data[] = $question->question;
            }
            $data[] = 'Date Created';

            $header = $data;
            $col = [];
            $entries = [];

            foreach ($survey->entries as $e){
                $col = [];

                foreach ($survey->questions  as $index=>$question){

                        $items = '';

                        $rows = SurveyQuestionAnswerItem::where('survey_question_answer_id',$e->id)->where('survey_question_id',$question->id)->get();

                        if(count($rows)>0){
                            if(count($rows)>1){
                                foreach ($rows as $x=>$row)
                                    $items .= $row->value.( $x+1 <count($rows) ? ',' : '');
                            } else {
                                $items = $rows[0]->value;
                            }
                        } else {
                            foreach ($question->choices as $choice){
                                $rating = SurveyQuestionChoiceRating::where('survey_question_choice_id',$choice->id)->where('survey_question_answer_id',$e->id)->first();

                                if ($rating)
                                    $items .= $choice->value.': '.$rating->value. ' , ';
                            }
                        }

                        $col[] = $items;
                }

                $col[] = $e->created_at->format('d/m/Y H:s');
                $entries[] = $col;
            }

            Excel::create($survey->title, function($excel) use($header,$entries) {

                $excel->sheet('Sheetname', function($sheet) use($header,$entries) {

                    $sheet->appendRow($header);

                    foreach ($entries as $t)
                        $sheet->appendRow($t);

                });

            })->export('xls');

            $survey->load('questions.type','questions.choices');

            return view('admin.surveys.show',compact('data','survey'));
        }
    }

    public function entries($id){
        $survey = $this->model->find($id);

        if($survey){

            $survey->load('entries.data');

            return view('admin.surveys.entries.index',compact('survey'));
        }
    }

    public function delete($id){
        $survey = Survey::find($id);

        if($survey){
            $survey->delete();

            Session::flash('message','Survey Deleted.');

            return redirect()->route('admin.surveys.index');
        }
    }

    public function deleteEntry($id){
        $entry = SurveyQuestionAnswer::find($id);
        $survey_id = $entry->survey->id;

        if($entry){
            $entry->delete();
            return redirect()->back();
        }
    }

    public function showEntry($survey_id,$entry_id){
        $questions = SurveyQuestion::where('survey_id',$survey_id)->get();
        $survey = Survey::find($survey_id);

        if($questions && $survey){
            $data = [];

            foreach($questions as $index=>$question){
                $data[$index]['question'] = $question->question;

                if($question->type->slug=='rate'){
                    foreach($question->choices as $choice){
                        $data[$index]['choices'][$choice->id]['choice'] = $choice->value;
                        $data[$index]['choices'][$choice->id]['rate'] = SurveyQuestionChoiceRating::where('survey_question_answer_id',$entry_id)
                            ->where('survey_question_choice_id',$choice->id)->pluck('value')->first();
                    }
                } else {
                    $data[$index]['data'] = SurveyQuestionAnswerItem::where('survey_question_answer_id',$entry_id)->where('survey_question_id',$question->id)->get();
                }
            }

            return view('admin.surveys.entries.show',compact('data','survey'));
        }
    }

}
