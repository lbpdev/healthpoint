<?php

namespace App\Http\Controllers\Admin;

use App\Models\Article;
use App\Models\Physician;
use App\Repositories\ArticleManager;
use App\Repositories\PhysicianManager;
use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Session;
use App\Models\PhysicianMetaType;

class PhysicianController extends Controller
{

    public function __construct(Physician $model, PhysicianManager $manager)
    {
        $this->model = $model;
        $this->physicians = $manager;
    }

    public function create(){
        $metaTypes = PhysicianMetaType::get();

        return view('admin.physicians.create',compact('metaTypes'));
    }

    public function edit($id){
        $metaTypes = PhysicianMetaType::get();
        $data = $this->model->with('meta')->where('id',$id)->first();

        $data['meta'] = $this->physicians->getMetas($data);

        return view('admin.physicians.create',compact('data','metaTypes'));
    }

    public function store(Request $request){
        $data = $this->physicians->store($request);

        if($data){
            Session::flash('success','Saved Successfully');
            return redirect(route('admin.physicians.edit',$data->id));
        }

        Session::flash('error','There was an error. Please try again.');
        return redirect()->back();
    }

    public function update(Request $request){
        $this->physicians->update($request);

        Session::flash('success','Updated Successfully');
        return redirect()->back();
    }

    public function delete($article_id){
        $this->model->where('id',$article_id)->delete();

        Session::flash('success','Deleted Successfully');
        return redirect()->route('admin.physicians.index');
    }

    public function feature($id){
        if($this->physicians->feature($id))
            Session::flash('success','Updated Successfully');
        else
            Session::flash('error','Error');

        return redirect()->back();
    }

    public function unfeature($id){
        
        if($this->physicians->unfeature($id))
            Session::flash('success','Updated Successfully');
        else
            Session::flash('error','Error');

        return redirect()->back();
    }
}
