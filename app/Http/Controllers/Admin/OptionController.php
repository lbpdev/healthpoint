<?php

namespace App\Http\Controllers\Admin;

use App\Models\Article;
use App\Models\ArticleCategory;
use App\Models\Option;
use App\Repositories\ArticleManager;
use App\Repositories\OptionManager;
use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Session;

class OptionController extends Controller
{

    public function __construct(Option $model, OptionManager $manager)
    {
        $this->model = $model;
        $this->options = $manager;
    }

    public function create(){
        return view('admin.articles.create');
    }

    public function edit($article_id){
        $article = $this->model->where('id',$article_id)->first();
        return view('admin.articles.create',compact('article'));
    }

    public function store(Request $request){
        $data = $this->options->store($request);

        if($data)
            Session::flash('success','Saved Successfully');
        else
            Session::flash('error','There was an error. Please try again.');

        return redirect()->back();
    }

    public function update(Request $request){
        $data = $this->options->store($request);

        if($data)
            Session::flash('success','Saved Successfully');
        else
            Session::flash('error','There was an error. Please try again.');

        return redirect()->back();
    }

    public function delete($article_id){
        $this->model->where('id',$article_id)->delete();

        Session::flash('success','Deleted Successfully');
        return redirect()->back();
    }


}