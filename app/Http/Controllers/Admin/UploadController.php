<?php

namespace App\Http\Controllers\Admin;

use App\Models\File;
use App\Models\Upload;
use App\Repositories\CanCreateSlug;
use App\Services\Uploaders\FileUploader;
use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Session;


class UploadController extends Controller
{
    use CanCreateSlug;

    public function __construct(File $upload, FileUploader $fileUploader)
    {
        $this->model = $upload;
        $this->uploader = $fileUploader;
    }

    public function index(){
        $data = File::orderBy('created_at','DESC')->get();

        return view('admin.uploads.index',compact('data'));
    }

    public function store(Request $request)
    {
        $upload = $request->file('file');
        $input = $request->input();

        $slug = $this->generateSlug($input['name']);

        $uploadedFile = ($upload != null ? $this->uploader->upload($upload) : false);

        $input['slug'] = $slug;
        $input['src'] = $uploadedFile[0]['path'] .'/'. $uploadedFile[0]['file_name'];

        $this->model->create($input);

        Session::flash('success','Saved Successfully');

        return redirect()->back();
    }

    public function delete($article_id){
        $this->model->where('id',$article_id)->delete();

        Session::flash('success','Deleted Successfully');
        return redirect()->back();
    }

}
