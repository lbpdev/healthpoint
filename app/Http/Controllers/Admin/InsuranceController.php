<?php

namespace App\Http\Controllers\Admin;

use App\Models\Insurance;
use App\Repositories\InsuranceManager;
use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Session;

class InsuranceController extends Controller
{


    public function __construct(Insurance $model, InsuranceManager $manager)
    {
        $this->model = $model;
        $this->insurances = $manager;
    }

    public function create(){
        return view('admin.insurances.create');
    }

    public function edit($id){
        $data = $this->model->where('id',$id)->first();
        return view('admin.insurances.create',compact('data'));
    }

    public function store(Request $request){
        $data = $this->insurances->store($request);

        if($data){
            Session::flash('success','Saved Successfully');
            return redirect(route('admin.insurances.edit',$data->id));
        }

        Session::flash('error','There was an error. Please try again.');
        return redirect()->back();
    }

    public function update(Request $request){
        $this->insurances->update($request);

        Session::flash('success','Updated Successfully');
        return redirect()->back();
    }

    public function delete($id){
        $this->model->where('id',$id)->delete();

        Session::flash('success','Deleted Successfully');
        return redirect()->back();
    }
}
