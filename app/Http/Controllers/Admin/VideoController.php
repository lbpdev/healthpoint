<?php

namespace App\Http\Controllers\Admin;

use App\Models\Video;
use App\Repositories\VideoManager;
use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Session;

class VideoController extends Controller
{

    public function __construct(Video $model, VideoManager $manager)
    {
        $this->model = $model;
        $this->videos = $manager;
    }

    public function create(){
        return view('admin.videos.create');
    }

    public function edit($id){
        $data = $this->model->where('id',$id)->first();
        return view('admin.videos.create',compact('data'));
    }

    public function store(Request $request){
        $data = $this->videos->store($request);

        if($data){
            Session::flash('success','Saved Successfully');
            return redirect(route('admin.videos.edit',$data->id));
        }


        Session::flash('error','There was an error. Please try again.');
        return redirect()->back();
    }

    public function update(Request $request){
        $this->videos->update($request);

        Session::flash('success','Updated Successfully');
        return redirect()->back();
    }

    public function delete($id){
        $this->model->where('id',$id)->delete();

        Session::flash('success','Deleted Successfully');
        return redirect()->back();
    }
}
