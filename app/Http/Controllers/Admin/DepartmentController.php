<?php

namespace App\Http\Controllers\Admin;

use App\Models\Department;
use Illuminate\Http\Request;

use App\Http\Requests;
use App\Repositories\Traits\Fetcher;
use App\Http\Controllers\Controller;
use App\Repositories\DepartmentManager;
use Illuminate\Support\Facades\Session;

class DepartmentController extends Controller
{

    public function __construct(Department $model, DepartmentManager $manager)
    {
        $this->model = $model;
        $this->departments = $manager;
    }

    public function create(){
        return view('admin.departments.create');
    }

    public function edit($id){
        $data = $this->model->where('id',$id)->first();
        return view('admin.departments.create',compact('data'));
    }

    public function store(Request $request){
        $data = $this->departments->store($request);

        if($data){
            Session::flash('success','Saved Successfully');
            return redirect(route('admin.departments.edit',$data->id));
        }

        Session::flash('error','There was an error. Please try again.');
        return redirect()->back();
    }

    public function update(Request $request){
        $this->departments->update($request);

        Session::flash('success','Updated Successfully');
        return redirect()->back();
    }

    public function delete($article_id){
        $this->model->where('id',$article_id)->delete();

        Session::flash('success','Deleted Successfully');
        return redirect()->back();
    }
}
