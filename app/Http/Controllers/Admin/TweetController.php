<?php

namespace App\Http\Controllers\Admin;

use App\Models\Tweet;
use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Session;


class TweetController extends Controller
{

    public function __construct(Tweet $model)
    {
        $this->model = $model;
    }

    public function create(){
        return view('admin.tweets.create');
    }

    public function edit($id){
        $data = $this->model->where('id',$id)->first();
        return view('admin.tweets.create',compact('data'));
    }

    public function store(Request $request){
        $data = Tweet::create($request->input());

        if($data){
            Session::flash('success','Saved Successfully');
            return redirect(route('admin.tweets.edit',$data->id));
        }

        Session::flash('error','There was an error. Please try again.');
        return redirect()->back();
    }

    public function update(Request $request){
        $data = Tweet::where('id',$request->input('id'))->first();


        if($data){
            $data->update($request->input());
            Session::flash('success','Updated Successfully');
        }

        return redirect()->back();
    }

    public function delete($id){
        $this->model->where('id',$id)->delete();

        Session::flash('success','Deleted Successfully');
        return redirect()->back();
    }
}
