<?php

namespace App\Http\Controllers;

use App\Models\Album;
use App\Models\Article;
use App\Models\Department;
use App\Models\Physician;
use Illuminate\Http\Request;

use App\Http\Requests;

class SearchController extends Controller
{


    public function __construct(Article $article, Department $department, Physician $physician, Album $album)
    {
        $this->articles = $article;
        $this->department = $department;
        $this->physician = $physician;
        $this->album = $album;
    }

    public function search(Request $request){
        $keyword = $request->input('keyword');

        $articles = $this->articles->search($keyword, null, true, true)->get();
        $physicians = $this->physician->search($keyword, null, true, true)->get();
        $departments = $this->department->search($keyword, null, true, true)->get();
        $albums = $this->album->search($keyword, null, true, true)->get();

        return view('pages.search.index',compact('articles','physicians','departments','albums','keyword'));
    }

    public function searchService(Request $request){
        $keyword = $request->input('keyword');

        $departments = $this->department->search($keyword, null, true, true)->get();

        return view('pages.search.index',compact('departments','keyword'));
    }
}
