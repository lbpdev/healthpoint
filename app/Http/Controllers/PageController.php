<?php

namespace App\Http\Controllers;

use App\Models\Article;
use Illuminate\Http\Request;

use App\Http\Requests;

class PageController extends Controller
{

    public function article($slug){
        $article = Article::where('slug',$slug)->first();

        return view('pages.single', compact('article'));
    }
}
