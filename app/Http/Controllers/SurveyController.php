<?php

namespace App\Http\Controllers;

use App\Models\Option;
use App\Models\Survey\Survey;
use App\Models\Survey\SurveyQuestion;
use App\Models\Survey\SurveyQuestionAnswerItem;
use App\Models\Survey\SurveyQuestionChoiceRating;
use App\Models\Survey\SurveyQuestionType;
use Illuminate\Http\Request;

use App\Http\Requests;
use Illuminate\Support\Facades\Mail;
use Illuminate\Support\Facades\Session;

class SurveyController extends Controller
{

    public $eto;

    public function __construct(Survey $model)
    {
        $this->model = $model;
    }

    public function index(){

        $data = [];
        $results = [];

//        print_r($this->convert([[1,2,[3,4],5],6,[7,8,9],10,[11,[12,[13,14,[15,16,17]]]]]));

        $data = $this->model->where('is_active',1)->orderBy('created_at','DESC')->paginate(8);
        return view('pages.surveys.index',compact('data'));
    }

    public function show($slug){
        $data = $this->model->where('slug',$slug)->first();

        if($data){

            // if(!$data->is_active)
            //     return redirect()->route('surveys.index');

            $data->load('questions.type','questions.choices');

            return view('pages.surveys.show',compact('data'));
        }
    }

    public function store(Request $request){
        $survey = $this->model->find($request->input('id'));

        if($survey){
            $answers = $request->input('questions');

            $entry = $survey->entries()->create(['user_ip'=>$request->ip()]);

            foreach ($answers as $qID=>$answer){
                $question = SurveyQuestion::find($qID);

                if($question){
                    if($entry){

                        if(is_array($answer)){

                            if(isset($answer['choices'])){
                                foreach ($answer['choices'] as $rating){
                                    SurveyQuestionChoiceRating::create([
                                        'survey_question_choice_id'=>$rating['choice_id'],
                                        'survey_question_answer_id'=>$entry->id,
                                        'value'=>$rating['rate'],
                                    ]);
                                }
                            } else {
                                foreach ($answer as $item)
                                    $entry->data()->create(['value'=>$item,'survey_question_id'=>$question->id]);
                            }
                        }
                        else
                            $entry->data()->create(['value'=>$answer,'survey_question_id'=>$question->id]);
                    }
                }
            }


            $questions = SurveyQuestion::where('survey_id',$entry->survey->id)->get();
            $survey = Survey::find($entry->survey->id);

            if($questions && $survey){
                $data = [];
                $data['survey'] = $survey->title;

                foreach($questions as $index=>$question){
                    $data['questions'][$index]['question'] = $question->question;
                    $data['questions'][$index]['data'] = SurveyQuestionAnswerItem::where('survey_question_answer_id',$entry->id)->where('survey_question_id',$question->id)->get();
                }
            }


            $survey_emails = Option::where('slug','survey-emails')->pluck('value');

            foreach($survey_emails as $email){
                Mail::queue('emails.surveys.new', ['data' => $data], function ($m) use ($email) {
                    $m->from('surveys@healthpoint.ae', 'Healthpoint Survey');

                    $m->to($email, 'Healthpoint Survey Manager')->subject('Healthpoint Survey Entry');
                });
            }
        }

        Session::flash('success','Your entry has been submitted successfully. Thank you for your cooperation.');

        return redirect()->back();
    }


}
