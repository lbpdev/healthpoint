<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the controller to call when that URI is requested.
|
*/

Route::get('/', ['as'=>'home','uses'=>'HomeController@index']);

Route::get('about-us', function () {
    return view('pages.about-us.index');
});


Route::get('request-appointment', ['as'=>'appointment.index','uses'=>'AppointmentController@index']);
Route::post('appointment/store', ['as'=>'appointment.request','uses'=>'AppointmentController@store']);

Route::post('search',['as'=>'search','uses'=>'SearchController@search'] );
Route::post('search-service',['as'=>'search.service','uses'=>'SearchController@searchService'] );

/**
 * API Routes
 */

Route::group(['prefix' => 'find-a-doctor'], function() {
    Route::get('/',['as'=>'physician.index','uses'=>'PhysicianController@index']);
    Route::get('/{doctor_slug}',['as'=>'physician.single','uses'=>'PhysicianController@single']);
});


Route::group(['prefix' => 'clinical-services'], function() {
    Route::get('/', ['as' => 'department.index', 'uses' => 'DepartmentController@index']);
    Route::get('/{department_slug}', ['as' => 'department.single', 'uses' => 'DepartmentController@single']);
});


Route::group(['prefix' => 'patients'], function() {
    Route::get('/', function () { return view('pages.patients.services'); });
    Route::get('/our-services', function () { return view('pages.patients.services'); });
    Route::get('/insurance', function () { return view('pages.patients.insurance'); });
    Route::get('/at-healthpoint', function () { return view('pages.patients.at-healthpoint'); });

    Route::group(['prefix' => 'newsletters'], function() {
        Route::get('/',['as'=>'media.patient-newsletters','uses'=>'MediaController@newsletters']);
        Route::get('/{slug}',['as'=>'media.patient-newsletters.single','uses'=>'MediaController@singleNewsletter']);
    });

});


Route::group(['prefix' => 'health-tips'], function() {
    Route::get('/',['as'=>'blog.index','uses'=>'BlogController@index']);
    Route::get('/{article_id}',['as'=>'blog.single','uses'=>'BlogController@single']);
});

Route::get('page/{slug}',['as'=>'pages.single','uses'=>'PageController@article']);

Route::group(['prefix' => 'work-with-us'], function() {

    Route::get('/', function () { return view('pages.work-with-us.working-for-healthpoint'); });
    Route::get('/emiratization', function () { return view('pages.work-with-us.emiratization'); });
    Route::get('/careers', function () { return view('pages.work-with-us.opportunities'); });
});


Route::group(['prefix' => 'media'], function() {

    Route::get('/news',['as'=>'media.news','uses'=>'MediaController@news']);
    Route::get('/news/{slug}',['as'=>'media.news.single','uses'=>'MediaController@single']);

    Route::get('/hospital-campaigns',['as'=>'media.campaigns','uses'=>'MediaController@campaigns']);
    Route::get('/hospital-campaigns/{slug}',['as'=>'media.campaigns.single','uses'=>'MediaController@single']);

    Route::get('/seminars-and-workshops',['as'=>'media.seminars','uses'=>'MediaController@seminars']);
    Route::get('/seminars-and-workshops/{slug}',['as'=>'media.seminars.single','uses'=>'MediaController@singleEvent']);

    Route::group(['prefix' => 'galleries'], function() {
        Route::get('/', ['as' => 'pages.media.album', 'uses' => 'MediaController@galleries']);
        Route::get('/{slug}', ['as' => 'pages.media.album.single', 'uses' => 'MediaController@album']);
    });
});

Route::group(['prefix' => 'events'], function() {
    Route::get('/',['as'=>'events.index','uses'=>'EventController@index']);
    Route::get('/upcoming',['as'=>'events.upcoming','uses'=>'EventController@index']);
    Route::get('/past',['as'=>'events.past','uses'=>'EventController@past']);
    Route::get('/{slug}',['as'=>'events.single','uses'=>'EventController@show']);
});

Route::group(['prefix' => 'surveys'], function() {
    Route::get('/',['as'=>'surveys.index','uses'=>'SurveyController@index']);
    Route::get('/{slug}',['as'=>'surveys.single','uses'=>'SurveyController@show']);
    Route::post('/store',['as'=>'surveys.store','uses'=>'SurveyController@store']);
});

Route::get('/page/{slug}',['as'=>'page.single','uses'=>'MediaController@singlePage']);

Route::group(['prefix' => 'cp'], function() {

    Route::get('/',function(){ return redirect()->route('admin.login'); });
    Route::post('/login-post',['as'=>'admin.login.validate','uses'=>'Auth\AuthController@login']);
    Route::get('/login',['as'=>'admin.login','uses'=>'AdminController@login','middleware' => 'guest']);
    Route::get('/logout',['as'=>'admin.logout','uses'=>'Auth\AuthController@logout']);

    Route::group(['prefix' => 'articles','middleware' => 'auth'], function() {
        Route::get('/',                     ['as'=>'admin.articles.index','uses'=>'AdminController@articles']);
        Route::get('/create',               ['as'=>'admin.articles.create','uses'=>'Admin\ArticleController@create']);
        Route::post('/store',               ['as'=>'admin.articles.store','uses'=>'Admin\ArticleController@store']);
        Route::get('/edit/{id}',            ['as'=>'admin.articles.edit','uses'=>'Admin\ArticleController@edit']);
        Route::post('/update',              ['as'=>'admin.articles.update','uses'=>'Admin\ArticleController@update']);
        Route::get('/delete/{id}',          ['as'=>'admin.articles.delete','uses'=>'Admin\ArticleController@delete']);
        Route::get('/category/{category}',  ['as'=>'admin.articles.get','uses'=>'Admin\ArticleController@index']);
    });

    Route::group(['prefix' => 'physicians','middleware' => 'auth'], function() {
        Route::get('/',                     ['as'=>'admin.physicians.index','uses'=>'AdminController@physicians']);
        Route::get('/create',               ['as'=>'admin.physicians.create','uses'=>'Admin\PhysicianController@create']);
        Route::post('/store',               ['as'=>'admin.physicians.store','uses'=>'Admin\PhysicianController@store']);
        Route::get('/edit/{id}',            ['as'=>'admin.physicians.edit','uses'=>'Admin\PhysicianController@edit']);
        Route::post('/update',              ['as'=>'admin.physicians.update','uses'=>'Admin\PhysicianController@update']);
        Route::get('/delete/{id}',          ['as'=>'admin.physicians.delete','uses'=>'Admin\PhysicianController@delete']);
        Route::get('/feature/{id}',          ['as'=>'admin.physicians.feature','uses'=>'Admin\PhysicianController@feature']);
        Route::get('/unfeature/{id}',          ['as'=>'admin.physicians.unfeature','uses'=>'Admin\PhysicianController@unfeature']);
    });

    Route::group(['prefix' => 'departments','middleware' => 'auth'], function() {
        Route::get('/',                     ['as'=>'admin.departments.index','uses'=>'AdminController@departments']);
        Route::get('/create',               ['as'=>'admin.departments.create','uses'=>'Admin\DepartmentController@create']);
        Route::post('/store',               ['as'=>'admin.departments.store','uses'=>'Admin\DepartmentController@store']);
        Route::get('/edit/{id}',            ['as'=>'admin.departments.edit','uses'=>'Admin\DepartmentController@edit']);
        Route::post('/update',              ['as'=>'admin.departments.update','uses'=>'Admin\DepartmentController@update']);
        Route::get('/delete/{id}',          ['as'=>'admin.departments.delete','uses'=>'Admin\DepartmentController@delete']);
    });

    Route::group(['prefix' => 'insurances','middleware' => 'auth'], function() {
        Route::get('/',                     ['as'=>'admin.insurances.index','uses'=>'AdminController@insurances']);
        Route::get('/create',               ['as'=>'admin.insurances.create','uses'=>'Admin\InsuranceController@create']);
        Route::post('/store',               ['as'=>'admin.insurances.store','uses'=>'Admin\InsuranceController@store']);
        Route::get('/edit/{id}',            ['as'=>'admin.insurances.edit','uses'=>'Admin\InsuranceController@edit']);
        Route::post('/update',              ['as'=>'admin.insurances.update','uses'=>'Admin\InsuranceController@update']);
        Route::get('/delete/{id}',          ['as'=>'admin.insurances.delete','uses'=>'Admin\InsuranceController@delete']);
    });

    Route::group(['prefix' => 'careers','middleware' => 'auth'], function() {
        Route::get('/',                     ['as'=>'admin.careers.index','uses'=>'AdminController@careers']);
        Route::get('/create',               ['as'=>'admin.careers.create','uses'=>'Admin\CareerController@create']);
        Route::post('/store',               ['as'=>'admin.careers.store','uses'=>'Admin\CareerController@store']);
        Route::get('/edit/{id}',            ['as'=>'admin.careers.edit','uses'=>'Admin\CareerController@edit']);
        Route::post('/update',              ['as'=>'admin.careers.update','uses'=>'Admin\CareerController@update']);
        Route::get('/delete/{id}',          ['as'=>'admin.careers.delete','uses'=>'Admin\CareerController@delete']);
    });

    Route::group(['prefix' => 'tweets','middleware' => 'auth'], function() {
        Route::get('/',                     ['as'=>'admin.tweets.index','uses'=>'AdminController@tweets']);
        Route::get('/create',               ['as'=>'admin.tweets.create','uses'=>'Admin\TweetController@create']);
        Route::post('/store',               ['as'=>'admin.tweets.store','uses'=>'Admin\TweetController@store']);
        Route::get('/edit/{id}',            ['as'=>'admin.tweets.edit','uses'=>'Admin\TweetController@edit']);
        Route::post('/update',              ['as'=>'admin.tweets.update','uses'=>'Admin\TweetController@update']);
        Route::get('/delete/{id}',          ['as'=>'admin.tweets.delete','uses'=>'Admin\TweetController@delete']);
    });

    Route::group(['prefix' => 'galleries','middleware' => 'auth'], function() {
        Route::get('/',                     ['as'=>'admin.galleries.index','uses'=>'AdminController@galleries']);
        Route::get('/create',               ['as'=>'admin.galleries.create','uses'=>'Admin\GalleryController@create']);
        Route::post('/store',               ['as'=>'admin.galleries.store','uses'=>'Admin\GalleryController@store']);
        Route::get('/edit/{id}',            ['as'=>'admin.galleries.edit','uses'=>'Admin\GalleryController@edit']);
        Route::post('/update',              ['as'=>'admin.galleries.update','uses'=>'Admin\GalleryController@update']);
        Route::get('/delete/{id}',          ['as'=>'admin.galleries.delete','uses'=>'Admin\GalleryController@delete']);
        Route::get('/delete-item/{id}',     ['as'=>'admin.galleries.delete-item','uses'=>'Admin\GalleryController@deleteItem']);
        Route::get('/{cat}',                ['as'=>'admin.galleries.filter','uses'=>'Admin\GalleryController@filter']);
    });

    Route::group(['prefix' => 'management','middleware' => 'auth'], function() {
        Route::get('/',                     ['as'=>'admin.management.index','uses'=>'AdminController@management']);
        Route::get('/create',               ['as'=>'admin.management.create','uses'=>'Admin\ManagementController@create']);
        Route::post('/store',               ['as'=>'admin.management.store','uses'=>'Admin\ManagementController@store']);
        Route::get('/edit/{id}',            ['as'=>'admin.management.edit','uses'=>'Admin\ManagementController@edit']);
        Route::post('/update',              ['as'=>'admin.management.update','uses'=>'Admin\ManagementController@update']);
        Route::get('/delete/{id}',          ['as'=>'admin.management.delete','uses'=>'Admin\ManagementController@delete']);
    });

    Route::group(['prefix' => 'events','middleware' => 'auth'], function() {
        Route::get('/',                     ['as'=>'admin.events.index','uses'=>'AdminController@events']);
        Route::get('/create',               ['as'=>'admin.events.create','uses'=>'Admin\EventController@create']);
        Route::post('/store',               ['as'=>'admin.events.store','uses'=>'Admin\EventController@store']);
        Route::get('/edit/{id}',            ['as'=>'admin.events.edit','uses'=>'Admin\EventController@edit']);
        Route::post('/update',              ['as'=>'admin.events.update','uses'=>'Admin\EventController@update']);
        Route::get('/delete/{id}',          ['as'=>'admin.events.delete','uses'=>'Admin\EventController@delete']);
    });

    Route::group(['prefix' => 'videos','middleware' => 'auth'], function() {
        Route::get('/',                     ['as'=>'admin.videos.index','uses'=>'AdminController@videos']);
        Route::get('/create',               ['as'=>'admin.videos.create','uses'=>'Admin\VideoController@create']);
        Route::post('/store',               ['as'=>'admin.videos.store','uses'=>'Admin\VideoController@store']);
        Route::get('/edit/{id}',            ['as'=>'admin.videos.edit','uses'=>'Admin\VideoController@edit']);
        Route::post('/update',              ['as'=>'admin.videos.update','uses'=>'Admin\VideoController@update']);
        Route::get('/delete/{id}',          ['as'=>'admin.videos.delete','uses'=>'Admin\VideoController@delete']);
    });

    Route::group(['prefix' => 'appointments','middleware' => 'auth'], function() {
        Route::get('/',                     ['as'=>'admin.appointments.index','uses'=>'AdminController@appointments']);
        Route::get('/create',               ['as'=>'admin.appointments.create','uses'=>'Admin\AppointmentController@create']);
        Route::post('/store',               ['as'=>'admin.appointments.store','uses'=>'Admin\AppointmentController@store']);
        Route::get('/report',               ['as'=>'admin.appointments.report','uses'=>'Admin\AppointmentController@report']);
        Route::get('/edit/{id}',            ['as'=>'admin.appointments.edit','uses'=>'Admin\AppointmentController@edit']);
        Route::post('/update',              ['as'=>'admin.appointments.update','uses'=>'Admin\AppointmentController@update']);
        Route::get('/delete/{id}',          ['as'=>'admin.appointments.delete','uses'=>'Admin\AppointmentController@delete']);
        Route::get('/{id}',                 ['as'=>'admin.appointments.view','uses'=>'Admin\AppointmentController@view']);
    });

    Route::group(['prefix' => 'uploads','middleware' => 'auth'], function() {
        Route::get('/',                     ['as'=>'admin.uploads.index','uses'=>'Admin\UploadController@index']);
        Route::post('/store',               ['as'=>'admin.uploads.store','uses'=>'Admin\UploadController@store']);
        Route::get('/delete/{id}',          ['as'=>'admin.uploads.delete','uses'=>'Admin\UploadController@delete']);
        Route::get('/{id}',                 ['as'=>'admin.uploads.show','uses'=>'Admin\UploadController@show']);
    });

    Route::group(['prefix' => 'surveys','middleware' => 'auth'], function() {
        Route::get('/',                     ['as'=>'admin.surveys.index','uses'=>'Admin\SurveyController@index']);
        Route::get('/create',               ['as'=>'admin.surveys.create','uses'=>'Admin\SurveyController@create']);
        Route::post('/store',               ['as'=>'admin.surveys.store','uses'=>'Admin\SurveyController@store']);
        Route::post('/update',              ['as'=>'admin.surveys.update','uses'=>'Admin\SurveyController@update']);
        Route::get('/delete/{id}',          ['as'=>'admin.surveys.delete','uses'=>'Admin\SurveyController@delete']);
        Route::get('/{survey_id}',          ['as'=>'admin.surveys.show','uses'=>'Admin\SurveyController@show']);
        Route::get('/toggle-push/{survey_id}',['as'=>'admin.surveys.toggle-publish','uses'=>'Admin\SurveyController@togglePublish']);
        Route::get('/preview/{survey_id}',  ['as'=>'admin.surveys.preview','uses'=>'Admin\SurveyController@preview']);
        Route::get('/{survey_id}/entries',  ['as'=>'admin.surveys.entries','uses'=>'Admin\SurveyController@entries']);
        Route::get('/{survey_id}/excel',    ['as'=>'admin.surveys.excel','uses'=>'Admin\SurveyController@excel']);
        Route::get('/{survey_id}/entries/{answer_id}',['as'=>'admin.surveys.entries.show','uses'=>'Admin\SurveyController@showEntry']);
        Route::get('entries/{answer_id}/delete',['as'=>'admin.surveys.entries.delete','uses'=>'Admin\SurveyController@deleteEntry']);
    });

    Route::group(['prefix' => 'options','middleware' => 'auth'], function() {

        Route::get('/general',              ['as'=>'admin.options.index','uses'=>'AdminController@options']);
        Route::get('/about-us',             ['as'=>'admin.options.about',function(){ return view('admin.settings.about-us'); }]);
        Route::get('/patients-and-visitors',['as'=>'admin.options.patients',function(){ return view('admin.settings.patients-and-visitors'); }]);
        Route::get('/working',       ['as'=>'admin.options.working',function(){ return view('admin.settings.working-for-us'); }]);


        Route::get('/create',               ['as'=>'admin.options.create','uses'=>'Admin\OptionController@create']);
        Route::post('/store',               ['as'=>'admin.options.store','uses'=>'Admin\OptionController@store']);
        Route::get('/edit/{id}',            ['as'=>'admin.options.edit','uses'=>'Admin\OptionController@edit']);
        Route::post('/update',              ['as'=>'admin.options.update','uses'=>'Admin\OptionController@update']);
        Route::get('/delete/{id}',          ['as'=>'admin.options.delete','uses'=>'Admin\OptionController@delete']);
        Route::get('/{id}',                 ['as'=>'admin.options.view','uses'=>'Admin\OptionController@view']);
    });
});



