<?php

namespace App\Http\Middleware;

use Closure;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Redirect;

class ForceW
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @param  string|null  $guard
     * @return mixed
     */
    public function handle($request, Closure $next, $guard = null)
    {

        //Add the 'www.' to all requests
//        $request=app('request');
//        $host=$request->header('host');
//        if (substr($host, 0, 4) != 'www.') {
//            $request->headers->set('host', 'www.'.$host);
//            return Redirect::to($request->path());
//        }

        return $next($request);
    }
}
