<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class AppointmentRequestInfo extends Model
{
    protected $fillable = ['key','value','appointment_request_id'];
}
