<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Management extends Model
{

    protected $fillable = ['name','position','description'];


    /**
     * A user has a profile.
     *
     * @return \Illuminate\Database\Eloquent\Relations\HasOne
     */
    public function uploads()
    {
        return $this->morphOne('App\Models\Upload', 'uploadable');
    }

    /**
     * get Thumbnail Attrib
     *
     * @return \Illuminate\Database\Eloquent\Relations\HasOne
     */
    public function getThumbnailAttribute()
    {
        $thumb = $this->uploads()->where('template', 'thumb')->first();

        return $thumb ? asset('public/').$thumb->url : 'http://placehold.it/240x280';
    }
}
