<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Nicolaslopezj\Searchable\SearchableTrait;

class Department extends Model
{
    use SearchableTrait;

    /**
     * Searchable rules.
     *
     * @var array
     */
    protected $searchable = [
        'columns' => [
            'name' => 10,
            'description' => 10,
        ]
    ];

    protected $fillable = ['name','slug','description'];

    public function physicians(){
        return $this->belongsToMany('App\Models\Physician','physician_departments');
    }

    public function headPhysician(){
        return $this->belongsToMany('App\Models\Physician','department_heads');
    }

    public function getHeadAttribute(){
        return count($this->headPhysician) ? $this->headPhysician[0] : null;
    }

    public function getSubHeadAttribute(){
        return count($this->headPhysician) > 1 ? $this->headPhysician[1] : null;
    }
}
