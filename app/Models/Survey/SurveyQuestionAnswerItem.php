<?php

namespace App\Models\Survey;

use Illuminate\Database\Eloquent\Model;

class SurveyQuestionAnswerItem extends Model
{
    protected $fillable = ['value','survey_question_answer_id','survey_question_id'];

    public function question(){
        return $this->hasOne('App\Models\Survey\SurveyQuestion','id','survey_question_id');
    }

    public function answer(){
        return $this->hasOne('App\Models\Survey\SurveyQuestionAnswer');
    }

    public function getAnswerAttribute(){
        return $this->answer()->first();
    }
}
