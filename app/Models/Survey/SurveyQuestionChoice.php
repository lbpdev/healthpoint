<?php

namespace App\Models\Survey;

use Illuminate\Database\Eloquent\Model;

class SurveyQuestionChoice extends Model
{
    protected $fillable = ['survey_question_id','value','ratability'];

    public function question(){
        return $this->hasOne('App\Models\Survey\SurveyQuestion');
    }

    public function rating(){
        return $this->hasOne('App\Models\Survey\SurveyQuestionChoiceRating');
    }
}
