<?php

namespace App\Models\Survey;

use Illuminate\Database\Eloquent\Model;

class SurveyQuestionChoiceRating extends Model
{
    protected $fillable = ['survey_question_choice_id','survey_question_answer_id','value'];

    public function choice(){
        return $this->hasOne('App\Models\Survey\SurveyQuestionChoice');
    }

    public function answer(){
        return $this->hasOne('App\Models\Survey\SurveyQuestionAnswer');
    }
}
