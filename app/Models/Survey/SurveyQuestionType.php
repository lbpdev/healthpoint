<?php

namespace App\Models\Survey;

use Illuminate\Database\Eloquent\Model;

class SurveyQuestionType extends Model
{
    protected $fillable = ['name','slug'];
}
