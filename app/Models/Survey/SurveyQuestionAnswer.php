<?php

namespace App\Models\Survey;

use Illuminate\Database\Eloquent\Model;

class SurveyQuestionAnswer extends Model
{
    protected $fillable = ['survey_id','user_ip'];

    public function survey(){
        return $this->hasOne('App\Models\Survey\Survey','id','survey_id');
    }

    public function data(){
        return $this->hasMany('App\Models\Survey\SurveyQuestionAnswerItem');
    }

    public function getSurveyAttribute(){
        return $this->survey()->first();
    }
}
