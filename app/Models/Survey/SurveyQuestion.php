<?php

namespace App\Models\Survey;

use Illuminate\Database\Eloquent\Model;

class SurveyQuestion extends Model
{
    protected $fillable = ['survey_id','survey_question_type_id','question','order','is_required','is_float'];

    public function type(){
        return $this->hasOne('App\Models\Survey\SurveyQuestionType','id','survey_question_type_id');
    }

    public function survey(){
        return $this->hasOne('App\Models\Survey\Survey','id','survey_id');
    }

    public function getTypeAttribute(){
        return $this->type()->first();
    }

    public function choices(){
        return $this->hasMany('App\Models\Survey\SurveyQuestionChoice');
    }

    public function getChoicesAttribute(){
        return $this->choices()->get();
    }

    public function getEntriesAttribute(){
        return $this->entries()->get();
    }
}
