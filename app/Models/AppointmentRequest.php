<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class AppointmentRequest extends Model
{
    protected $fillable = ['name'];

    public function info(){
        return $this->hasMany('App\Models\AppointmentRequestInfo');
    }
}
