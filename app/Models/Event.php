<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Event extends Model
{

    protected $fillable = ['title', 'slug', 'subtitle', 'date', 'content', 'category','location','time'];

    public $dates = ['date'];

    /**
     * An article has uploads.
     *
     * @return \Illuminate\Database\Eloquent\Relations\HasOne
     */
    public function uploads()
    {
        return $this->morphOne('App\Models\Upload', 'uploadable');
    }


    /**
     * get Thumbnail Attrib
     *
     * @return \Illuminate\Database\Eloquent\Relations\HasOne
     */
    public function getThumbnailSquareAttribute()
    {
        $thumb = $this->uploads()->where('template', 'thumb-square')->first();

        return $thumb ? $thumb->url : 'http://placehold.it/240x240';
    }


    /**
     * get Thumbnail Attrib
     *
     * @return \Illuminate\Database\Eloquent\Relations\HasOne
     */
    public function getThumbnailAttribute()
    {
        $thumb = $this->uploads()->where('template', 'thumb')->first();

        return $thumb ? asset('public'.$thumb->url ) : 'http://placehold.it/320x240';
    }

    /**
     * get Thumbnail Attrib
     *
     * @return \Illuminate\Database\Eloquent\Relations\HasOne
     */
    public function getThumbnailLongAttribute()
    {
        $thumb = $this->uploads()->where('template', 'long')->first();

        return $thumb ? asset('public'.$thumb->url ) : 'http://placehold.it/240x200';
    }


}
