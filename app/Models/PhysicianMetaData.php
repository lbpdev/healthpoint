<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class PhysicianMetaData extends Model
{
    protected $table = 'physician_meta_data';

    protected $fillable = ['physician_id','meta_type_id','value'];
}
