<?php
namespace App\Services;

use App\Models\Article;
use App\Repositories\Traits\Fetcher;

class ArticleFetcher {
    use Fetcher;

    public function __construct(Article $model)
    {
        $this->model = $model;
    }

    public function getByCategory($category_id,$limit)
    {
        return $this->model->where('category_id',$category_id)->orderBy('created_at','DESC')->paginate($limit);
    }

    public function getByCategoryPopular($category_id,$limit)
    {
        return $this->model->where('category_id',$category_id)->orderBy('views','DESC')->paginate($limit);
    }

    public function getLatestNews()
    {
        return $this->model->where('category_id',5)->orderBy('created_at','DESC')->first();
    }
}