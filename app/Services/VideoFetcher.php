<?php
namespace App\Services;

use App\Models\Video;
use App\Repositories\Traits\Fetcher;

class VideoFetcher {
    use Fetcher;

    public function __construct(Video $model)
    {
        $this->model = $model;
    }

    public function getByCategory($category,$limit)
    {
        return $this->model->where('category',$category)->orderBy('created_at','DESC')->limit($limit)->get();
    }
}