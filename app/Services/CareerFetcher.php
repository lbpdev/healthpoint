<?php
namespace App\Services;

use App\Models\Career;
use App\Repositories\Traits\Fetcher;

class CareerFetcher {

    use Fetcher;

    public function __construct(Career $model)
    {
        $this->model = $model;
    }

    public function getList()
    {
        return $this->model->lists('name','id');
    }
}