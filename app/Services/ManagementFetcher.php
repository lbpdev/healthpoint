<?php
namespace App\Services;

use App\Models\Management;
use App\Repositories\Traits\Fetcher;

class ManagementFetcher {

    public function __construct(Management $model)
    {
        $this->model = $model;
    }

    public function getAll(){
    	return $this->model->orderBy('created_at','ASC')->get();
    }
}
