<?php
namespace App\Services;

use App\Models\Insurance;
use App\Repositories\Traits\Fetcher;

class InsuranceFetcher {

    use Fetcher;

    public function __construct(Insurance $model)
    {
        $this->model = $model;
    }

    public function getList()
    {
        return $this->model->lists('name','id');
    }
}