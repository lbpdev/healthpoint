<?php
namespace App\Services;

use App\Models\ArticleCategory;
use App\Repositories\Traits\Fetcher;

class CategoryFetcher {
    use Fetcher;

    public function __construct(ArticleCategory $model)
    {
        $this->model = $model;
    }

    public function getList()
    {
        return $this->model->lists('name','id');
    }
}