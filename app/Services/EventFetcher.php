<?php
namespace App\Services;

use App\Models\Event;
use App\Repositories\Traits\Fetcher;
use Carbon\Carbon;

class EventFetcher {
    use Fetcher;

    public function __construct(Event $model)
    {
        $this->model = $model;
    }

    public function getByCategory($category,$limit)
    {
        return $this->model->where('category',$category)->orderBy('date','DESC')->paginate($limit);
    }

    public function upcoming($category,$limit)
    {
        return $this->model->whereDate('date','>=',Carbon::now()->format('Y-m-d'))->where('category',$category)->orderBy('date','DESC')->paginate($limit);;
    }

    public function past($category,$limit)
    {
        return $this->model->whereDate('date','<',Carbon::now()->format('Y-m-d'))->where('category',$category)->orderBy('date','ASC')->paginate($limit);;
    }
}