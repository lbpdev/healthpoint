<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateSurveyQuestionChoiceRatingsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {

        Schema::create('survey_question_choice_ratings', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('survey_question_choice_id')->unsigned()->index();
            $table->foreign('survey_question_choice_id')->references('id')->on('survey_question_choices')->onDelete('cascade');
            $table->integer('survey_question_answer_id')->unsigned()->index();
            $table->foreign('survey_question_answer_id')->references('id')->on('survey_question_answers')->onDelete('cascade');
            $table->string('value');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('survey_question_choice_ratings');
    }
}
