<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateSurveyQuestionAnswerItemsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('survey_question_answer_items', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('survey_question_answer_id')->unsigned()->index();
            $table->foreign('survey_question_answer_id')->references('id')->on('survey_question_answers')->onDelete('cascade');
            $table->integer('survey_question_id')->unsigned()->index();
            $table->foreign('survey_question_id')->references('id')->on('survey_questions')->onDelete('cascade');
            $table->string('value');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('survey_question_answer_items');
    }
}
