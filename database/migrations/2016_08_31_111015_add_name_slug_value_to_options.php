<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddNameSlugValueToOptions extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('options',function($table){
            $table->string('name');
            $table->string('slug');
            $table->longText('value');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('options',function($table){
            $table->dropColumn('name');
            $table->dropColumn('slug');
            $table->dropColumn('value');
        });
    }
}
