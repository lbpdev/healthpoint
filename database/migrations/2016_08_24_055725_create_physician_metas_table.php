<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePhysicianMetasTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('physician_meta_data', function (Blueprint $table) {
            //'name','slug','value','physician_id'
            $table->increments('id');
            $table->longText('value');
            $table->integer('physician_id')->unsigned()->index();
            $table->foreign('physician_id')->references('id')->on('physicians')->onDelete('cascade');
            $table->integer('meta_type_id')->unsigned()->index();
            $table->foreign('meta_type_id')->references('id')->on('physician_meta_types')->onDelete('cascade');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('physician_meta_data');
    }
}
