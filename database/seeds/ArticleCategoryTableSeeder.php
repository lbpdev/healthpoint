<?php

use Illuminate\Database\Seeder;
use App\Models\ArticleCategory;

use App\Repositories\CanCreateSlug;

class ArticleCategoryTableSeeder extends Seeder
{
    use CanCreateSlug;

    public function __construct(ArticleCategory $model)
    {
        $this->model = $model;
    }

    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $data = [
            ['name' => 'Mubadala Healthcare Assets'],
            ['name' => 'Services'],
            ['name' => 'At Healthpoint'],
            ['name' => 'Blog'],
            ['name' => 'News'],
            ['name' => 'Hospital Campaigns'],
            ['name' => 'Partners']
        ];

        foreach ($data as $item){
            $item['slug'] = $this->generateSlug($item['name']);

            $this->model->create($item);
        }

    }
}

