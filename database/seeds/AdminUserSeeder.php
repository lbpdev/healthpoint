<?php

use Illuminate\Database\Seeder;
use App\User;

class AdminUserSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
//        User::create( [
//            'email' => 'f.kyriakidis@healthpoint.ae' ,
//            'password' => \Illuminate\Support\Facades\Hash::make( 'HPadm2016!!' ) ,
//            'name' => 'Fadi Kyriakidis' ,
//        ] );
        User::create( [
            'email' => 'i.alsaabri@healthpoint.ae' ,
            'password' => \Illuminate\Support\Facades\Hash::make( 'HPadm2016!!' ) ,
            'name' => 'Isra Alsaabri' ,
        ] );
        User::create( [
            'email' => 'r.morales@healthpoint.ae' ,
            'password' => \Illuminate\Support\Facades\Hash::make( 'HPadm2016!!' ) ,
            'name' => 'Rochelle Morales' ,
        ] );
    }
}
