<?php

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
         $this->call(DepartmentTableSeeder::class);
         $this->call(ArticleCategoryTableSeeder::class);
         $this->call(ArticleTableSeeder::class);
         $this->call(ArticlesAtHealthpoint::class);
         $this->call(ArticlesOurServices::class);
         $this->call(InsuranceTableSeeder::class);
         $this->call(PhysicianMetaTypeTableSeeder::class);
         $this->call(PhysicianTableSeeder::class);
         $this->call(EventTableSeeder::class);
         $this->call(VideoTableSeeder::class);
         $this->call(ManagementTableSeeder::class);
    }
}
