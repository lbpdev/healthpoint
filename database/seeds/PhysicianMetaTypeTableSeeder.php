<?php

use Illuminate\Database\Seeder;
use App\Models\PhysicianMetaType;

class PhysicianMetaTypeTableSeeder extends Seeder
{
    use \App\Repositories\CanCreateSlug;

    public function __construct(PhysicianMetaType $model)
    {
        $this->model = $model;
    }
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $data = [
            ['name' => 'Work Experience'],
            ['name' => 'Medical Training'],
            ['name' => 'Awards And Membership'],
            ['name' => 'Contact Number'],
        ];

        foreach ($data as $item){
            $item['slug'] = $this->generateSlug($item['name']);

            $this->model->create($item);
        }
    }
}
