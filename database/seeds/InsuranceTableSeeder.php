<?php

use Illuminate\Database\Seeder;
use App\Models\Insurance;

use App\Repositories\CanCreateSlug;
use App\Services\Uploaders\InsuranceThumbnailUploader;

class InsuranceTableSeeder extends Seeder
{
    use CanCreateSlug;

    public function __construct(Insurance $model)
    {
        $this->model = $model;
    }

    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $data = [
            ['name' => 'ADNIC'],
            ['name' => 'Almadallah'],
            ['name' => 'Daman'],
            ['name' => 'Thiqa'],
            ['name' => 'NAS'],
            ['name' => 'WAPMED'],
            ['name' => 'Oman Insurance'],
            ['name' => 'Saico Health']
        ];

        foreach ($data as $item){
            $item['slug'] = $this->generateSlug($item['name']);

            $this->model->create($item);
        }

    }
}

