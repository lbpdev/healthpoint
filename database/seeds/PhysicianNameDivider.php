<?php

use Illuminate\Database\Seeder;

class PhysicianNameDivider extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $data = [
            ['id' => 1, 'name' =>'Dr. Maurice Khoury'],
            ['id' => 2, 'name' =>'Dr. Mohammed B Al Hadad'],
            ['id' => 3, 'name' =>' Dr. Janaranjan Jalli'],
            ['id' => 4, 'name' =>'Dr. Abeer Sawwaf Kraishan'],
            ['id' => 5, 'name' =>'Dr. Ahed Bisharat'],
            ['id' => 6, 'name' =>'Dr. Ahmad Muhammad Taha'],
            ['id' => 7, 'name' =>'Dr. Ahmed Fouad Ibrahim Ahmed'],
            ['id' => 8, 'name' =>'Dr. Ameera Al-Diwani'],
            ['id' => 9, 'name' =>'Azzam Kamal'],
            ['id' => 10, 'name' =>'Dr. Chanshik Shim'],
            ['id' => 11, 'name' =>'Dr. Christopher Butcher'],
            ['id' => 12, 'name' =>'Dr. Cornelia Prevot Loudiere'],
            ['id' => 13, 'name' =>'Dr. Diana Awni Albuhaisi'],
            ['id' => 14, 'name' =>'Dr. Efthimios Simon'],
            ['id' => 15, 'name' =>'Dr. Eleanor Luna'],
            ['id' => 16, 'name' =>'Dr. Emad Rahmani'],
            ['id' => 17, 'name' =>'Dr. Fadhel Al Ateeqi'],
            ['id' => 18, 'name' =>'Dr. Fani Bakardjieva'],
            ['id' => 19, 'name' =>'Dr. Griseldis Schmoldt'],
            ['id' => 20, 'name' =>'Dr. Hanan Al-Issa'],
            ['id' => 21, 'name' =>'Dr. Harpreet Saini'],
            ['id' => 22, 'name' =>'Dr. Hassy Pulipparambil'],
            ['id' => 23, 'name' =>'Dr. Hatem Ramadan'],
            ['id' => 24, 'name' =>'Dr. Ilham Said Mohammed Abbas'],
            ['id' => 25, 'name' =>'Dr. Jeffrey Fairley'],
            ['id' => 26, 'name' =>'Joanne Fenton'],
            ['id' => 27, 'name' =>'Dr. Jongdae Park'],
            ['id' => 28, 'name' =>'Dr. Jorge Molina-Martinez'],
            ['id' => 29, 'name' =>'Dr. Kenneth Bramlett'],
            ['id' => 30, 'name' =>'Dr. Kiyoung Choi'],
            ['id' => 31, 'name' =>'Dr. Mai Ahmed Sultan Al Jaber'],
            ['id' => 32, 'name' =>'Dr. Maliha Fansur Nazir'],
            ['id' => 33, 'name' =>'Dr. Mohammed Ameenuddin'],
            ['id' => 34, 'name' =>'Dr. Muhammad Tahir'],
            ['id' => 35, 'name' =>'Dr. Nader Darwich'],
            ['id' => 36, 'name' =>'Dr. Nagib Yordi'],
            ['id' => 37, 'name' =>'Dr. Nairah Rasul-Syed'],
            ['id' => 38, 'name' =>'Dr. Neil Richard Fell'],
            ['id' => 39, 'name' =>'Dr. Ojo Chidinma Jumoke'],
            ['id' => 40, 'name' =>'Dr. Olexiy Ivanov'],
            ['id' => 41, 'name' =>'Professor, Dr. Philippe Neyret'],
            ['id' => 42, 'name' =>'Dr. Philipp Sagawe'],
            ['id' => 43, 'name' =>'Dr. Mohammad Raed Cheikhali'],
            ['id' => 44, 'name' =>'Rashid Buhari'],
            ['id' => 45, 'name' =>'Dr. Razan Hamideh'],
            ['id' => 46, 'name' =>'Dr. Rehab Abou El Seoud'],
            ['id' => 47, 'name' =>'Dr. Reinhard Stoewe'],
            ['id' => 48, 'name' =>'Dr. Roland Assmann'],
            ['id' => 49, 'name' =>'Dr. Sadaf Brown'],
            ['id' => 50, 'name' =>'Dr. Samer Nuhaily'],
            ['id' => 51, 'name' =>'Dr. Shafaath Husain Syed Mohammed'],
            ['id' => 52, 'name' =>'Dr. Staffan Holbeck'],
            ['id' => 53, 'name' =>'Prof. Dr. Stefan Schumacher'],
            ['id' => 54, 'name' =>'Steven Mosedale'],
            ['id' => 55, 'name' =>'Dr. Susan S. Glew'],
            ['id' => 56, 'name' =>'Dr. Timm Wolter'],
            ['id' => 57, 'name' =>'Dr. Waleed Farouk Mohamed'],
            ['id' => 58, 'name' =>'Dr. Uma Devi'],
            ['id' => 59, 'name' =>'Dr. Bernard Hoffmann'],
            ['id' => 60, 'name' =>'Dr. Tamer Gamal Eldin Ahmed Omar'],
            ['id' => 61, 'name' =>'Dr. Mohamad Hani Dalati'],
            ['id' => 62, 'name' =>'Dr. Tabarak Salah Mohamed Aly Elyan'],
            ['id' => 63, 'name' =>'Dr. Arohi Khairnar'],
            ['id' => 64, 'name' =>'Dr. Manjusha Mohanan'],
            ['id' => 65, 'name' =>'Dr Khalid F. Al Ameri'],
            ['id' => 66, 'name' =>'Dr. Abu Bakr Awad Osman'],
            ['id' => 67, 'name' =>'Dr. Ghada Izzat Qawasmeh'],
            ['id' => 68, 'name' =>'Dr. Osman Mustafa Osman Ortashi']
        ];

        foreach($data as $item){
            $physician = \App\Models\Physician::where('id',$item['id'])->first();

            $string = $item['name'];
            $pieces = explode(' ', $string);
            $lastName = array_pop($pieces);
            $firstName = implode(' ',$pieces);
            $physician->fname = $firstName;
            $physician->lname = $lastName;

            $physician->save();
        }
    }
}
