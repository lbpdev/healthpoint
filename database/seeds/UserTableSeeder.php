<?php

use Illuminate\Database\Seeder;
use App\User;

class UserTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        User::create( [
            'email' => 'gene@leadingbrands.me' ,
            'password' => \Illuminate\Support\Facades\Hash::make( 'secret' ) ,
            'name' => 'admin' ,
        ] );
    }
}
