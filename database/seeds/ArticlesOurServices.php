<?php

use Illuminate\Database\Seeder;
use App\Models\Article;

use App\Repositories\CanCreateSlug;

class ArticlesOurServices extends Seeder
{
    use CanCreateSlug;

    public function __construct(Article $model)
    {
        $this->model = $model;
    }

    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $data = [
            [
                'title' => "Five-Star Service",
                'content' => "<p>Our facility was designed to make you feel as though you are visiting a five-star hotel. With Starbucks coffee and gourmet dining on-site, valet parking, free Wi-Fi and marble fixtures throughout the hospital, your experience at Healthpoint will be enjoyable.&nbsp;</p>",
            ],
            [
                'title' => "Language Services",
                'content' => "<p>Our physicians and reception staff are bilingual, meeting your needs for English and Arabic translation. You may request an English-Arabic interpreter to accompany you during your appointment.</p>",
            ],
            [
                'title' => "Patient Rooms",
                'content' => "<p>Healthpoint has different types of in-patient suites for maximum patient comfort. All rooms feature flat-screen televisions, Nespresso machines and other five-star amenities.</p>",
            ],
            [
                'title' => "Pharmacy",
                'content' => "<p>We have a pharmacy on the ground level of the Clinic reception to add to the convenience of your post-appointment care.</p>",
            ],
            [
                'title' => "Complimentary Wi-Fi",
                'content' => "​​​<p>All patients and guests may enjoy complimentary Wi-Fi throughout the Clinic and Hospital during their visit.</p>",
            ],
            [
                'title' => "Dining",
                'content' => "<p>The Hummingbird Café is an executive-style dining lounge on the ground level of the Hospital which features international cuisine. You may enjoy Starbucks coffee and a variety of baked goods at Deli Marche in the Clinic reception next to the Pharmacy.</p>",
            ],
            [
                'title' => "Prayer Rooms",
                'content' => "<p>Prayer rooms are located throughout Healthpoint for the convenience of our patients.</p>",
            ],
            [
                'title' => "Parking And Valet",
                'content' => "<p>We offer complimentary valet to all patients and visitors, as well as self-park options around the hospital and in the underground parking garage at no cost.</p>",
            ],
            [
                'title' => "Reception Assistance & Services For Disabled Patients",
                'content' => "<p>Our guest services officers at the Clinic and Hospital reception desks will guide you to your appointment and escort our guests with disabilities and special needs.</p>",
            ]
        ];

        foreach ($data as $item){
            $item['slug'] = $this->generateSlug($item['title']);
            $item['category_id'] = 2;

            $this->model->create($item);
        }

    }
}



