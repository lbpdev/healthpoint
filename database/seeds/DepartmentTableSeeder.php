<?php

use Illuminate\Database\Seeder;
use App\Models\Department;

use App\Repositories\CanCreateSlug;

class DepartmentTableSeeder extends Seeder
{
    use CanCreateSlug;

    public function __construct(Department $department)
    {
        $this->model = $department;
    }

    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $data = [
            ['name' => 'Anesthesia'],
            ['name' => 'Bariatric & Metabolic Surgery'],
            ['name' => 'Cardiology'],
            ['name' => 'Dentistry'],
            ['name' => 'Dermatology'],
            ['name' => 'Diagnostic Imaging'],
            ['name' => 'Digestive diseases'],
            ['name' => 'Ear, Nose and Throat'],
            ['name' => 'Family Medicine'],
            ['name' => 'General Practice'],
            ['name' => 'General Surgery'],
            ['name' => 'Gynecology'],
            ['name' => 'Internal Medicine'],
            ['name' => 'Non-invasive Cosmetics'],
            ['name' => 'Orthopedics & Sports Medicine'],
            ['name' => 'Pediatrics'],
            ['name' => 'Physiotherapy & Rehabilitation'],
            ['name' => 'Plastic & Cosmetic Surgery'],
            ['name' => 'Podiatry'],
            ['name' => 'Respiratory & Sleep Medicine'],
            ['name' => 'Rheumatology'],
            ['name' => 'Spine Care'],
            ['name' => 'Urology & Endourology'],
            ['name' => 'Vascular Surgery'],
            ['name' => 'Wound Care'],
        ];

        foreach ($data as $item){
            $item['description'] = "<p>The physicians of Healthpoint Hospital’s Department of Anesthesia work closely with our orthopedic, bariatric and general surgeons to provide in-patients with world-class care.</p><h1>Pre-Operative</h1><p>Prior to surgery, patients will meet with one of Healthpoint’s anesthesiologists for a pre-surgical consultation to review of any medical issues that might impact anesthesia. Your anesthesiologist will also review the difference between types of anesthesia and discuss ways to manage your pain post-operation.​</p><h1>Types of Anesthesia:</h1><ul><li>General</li><li>Nerve Block/Plexus Block</li><li>Neuroaxial (Epidural/Spinal/Combined Spinal-Epidural)</li><li>Local</li><li>Sedation</li></ul><h1>Post-Operative</h1><p>Your anesthesiologist will help you and your surgeon to manage your pain and will recommend a stay in either our recovery room (PACU) or the intensive care unit (ICU) depending on what you require.</p><h1>Post-surgical pain will be treated by one or more of the following:​</h1><p>Round the clock and/or as needed analgesics (either orally or ​intravenously) Patient-controlled analgesia, in which the patient gives himself the analgesics whenever demanded just by pressing a button Continuous local anesthesia infiltration via catheter directly into the surgical site or in the epidural site﻿</p>";

            $item['slug'] = $this->generateSlug($item['name']);

            $this->model->create($item);
        }

    }
}
