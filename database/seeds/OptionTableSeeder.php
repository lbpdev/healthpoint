<?php

use Illuminate\Database\Seeder;
use App\Models\Option;

use App\Repositories\CanCreateSlug;

class OptionTableSeeder extends Seeder
{
    use CanCreateSlug;

    public function __construct(Option $model)
    {
        $this->model = $model;
    }

    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $data = [
            [
                'name' => 'Location',
                'value' => '<p>Zayed Sports City,Between<br />Gate 1 and 6 entrances - Abu<br />Dhabi - United Arab Emirates&nbsp;<br /><a href=\"https://www.google.com/maps?saddr=My+Location&amp;daddr=Healthpoint+-+Zayed+Sports+City,Between+Gate+1+and+6+entrances+-+Abu+Dhabi\" target=\"_blank\"><strong>GET DRIVING DIRECTIONS</strong></a>&nbsp;</p>',
            ]
        ];

        foreach ($data as $item){
            $item['description'] = "<p>The physicians of Healthpoint Hospital’s Department of Anesthesia work closely with our orthopedic, bariatric and general surgeons to provide in-patients with world-class care.</p><h1>Pre-Operative</h1><p>Prior to surgery, patients will meet with one of Healthpoint’s anesthesiologists for a pre-surgical consultation to review of any medical issues that might impact anesthesia. Your anesthesiologist will also review the difference between types of anesthesia and discuss ways to manage your pain post-operation.​</p><h1>Types of Anesthesia:</h1><ul><li>General</li><li>Nerve Block/Plexus Block</li><li>Neuroaxial (Epidural/Spinal/Combined Spinal-Epidural)</li><li>Local</li><li>Sedation</li></ul><h1>Post-Operative</h1><p>Your anesthesiologist will help you and your surgeon to manage your pain and will recommend a stay in either our recovery room (PACU) or the intensive care unit (ICU) depending on what you require.</p><h1>Post-surgical pain will be treated by one or more of the following:​</h1><p>Round the clock and/or as needed analgesics (either orally or ​intravenously) Patient-controlled analgesia, in which the patient gives himself the analgesics whenever demanded just by pressing a button Continuous local anesthesia infiltration via catheter directly into the surgical site or in the epidural site﻿</p>";

            $item['slug'] = $this->generateSlug($item['name']);

            $this->model->create($item);
        }

    }
}
